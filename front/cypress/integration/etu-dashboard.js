describe("Test student dashboard seen by student", function() {
  beforeEach(function() {
    const api_url = Cypress.env("API_URL");
    cy.request("POST", `${api_url}/login`, {
      username: "studenttest",
      password: "studenttest"
    }).then(res => {
      cy.setCookie("token", res.body.token);
      cy.setCookie("user", JSON.stringify(res.body));
    });

    const ui_url = Cypress.env("UI_URL");
    cy.visit(`${ui_url}/etu-dashboard`);
  });

  it("Check tableau des critères", function() {
    cy.get(":nth-child(1) > .ant-card-grid").contains("critTest");
    cy.get(":nth-child(1) > .ant-card-grid").contains("Non validé");

    cy.get(":nth-child(2) > .ant-card-grid").contains("critWeek");
    cy.get(":nth-child(2) > .ant-card-grid").contains(
      "0 / 5 semaines validées"
    );

    cy.get(
      ":nth-child(1) > .ant-card-grid > :nth-child(1) > p > .ant-btn"
    ).should("not.be.visible");
    cy.get(
      ":nth-child(2) > .ant-card-grid > :nth-child(1) > p > .ant-btn"
    ).should("not.be.visible");
  });

  it("Adding form to a student", function() {
    cy.get(
      ".ant-layout-content > div:nth-child(4) > button:nth-child(1)"
    ).click();
    cy.get(
      "label.ant-checkbox-wrapper:nth-child(1) > span:nth-child(1) > input:nth-child(1)"
    ).click();
    cy.get("#form_cmi_description").type("descriptionTest");
    const fileName = "image.jpg";
    cy.fixture(fileName).then(fileContent => {
      cy.get("span.ant-upload").upload(
        { fileContent, fileName, mimeType: "image/png" },
        { subjectType: "drag-n-drop" }
      );
    });
    cy.get("button.ant-btn-primary:nth-child(2)").click();
    cy.get(
      ".ant-list-items > div:nth-child(2) > li:nth-child(1) > div:nth-child(1) > div:nth-child(1) > h4:nth-child(1) > a:nth-child(1) > h3:nth-child(1)"
    ).click();
    cy.get("#form_cmi_description")
      .invoke("val")
      .then(text => expect(text).to.equals("descriptionTest"));
  });

  it("Adding stage form to a student", function() {
    cy.get(
      ".ant-layout-content > div:nth-child(4) > button:nth-child(1)"
    ).click();
    cy.get(
      "#form_cmi_criterion > label:nth-child(2) > span:nth-child(2)"
    ).click();
    cy.get("#form_cmi_job_title").type("Titre du stage");
    cy.get(".ant-calendar-picker-input").click();
    cy.get('[title="March 1, 2020"] > .ant-calendar-date').click();
    cy.get('[title="March 15, 2020"] > .ant-calendar-date').click();
    cy.get("#form_cmi_company").type("Etablissement");
    cy.get("#form_cmi_address").type("Adresse");
    cy.get("#form_cmi_address_c").type("Adresse_c");
    cy.get("#form_cmi_postal_code").type("33000");
    cy.get("#form_cmi_city").type("Ville");
    cy.get("#form_cmi_country").type("Pays");
    cy.get("#form_cmi_description").type("Description");
    const fileName = "image.jpg";
    cy.fixture(fileName).then(fileContent => {
      cy.get("span.ant-upload").upload(
        { fileContent, fileName, mimeType: "image/png" },
        { subjectType: "drag-n-drop" }
      );
    });
    cy.get("button.ant-btn-primary:nth-child(2)").click();
    cy.wait(1500);
    cy.get(":nth-child(3) > .ant-list-item").contains("critWeek Etablissement");
    cy.get(
      ".ant-list-items > div:nth-child(3) > li:nth-child(1) > div:nth-child(1)"
    ).click();
    cy.get("#form_cmi_job_title").should("have.value", "Titre du stage");
    cy.get('[placeholder="Date de début"]').should("have.value", "01/03/2020");
    cy.get('[placeholder="Date de fin"]').should("have.value", "15/03/2020");
    cy.get("#form_cmi_company").should("have.value", "Etablissement");
    cy.get("#form_cmi_address").should("have.value", "Adresse");
    cy.get("#form_cmi_address_c").should("have.value", "Adresse_c");
    cy.get("#form_cmi_postal_code").should("have.value", "33000");
    cy.get("#form_cmi_city").should("have.value", "Ville");
    cy.get("#form_cmi_country").should("have.value", "Pays");
    cy.get("#form_cmi_description").should("have.value", "Description");
    cy.get(".ant-col-xs-1 > .ant-btn");
  });

  it("MEP validate and invalidate forms", function() {
    cy.get(".ant-col-xs-1 > .ant-btn");
    const api_url = Cypress.env("API_URL");
    const ui_url = Cypress.env("UI_URL");
    cy.request("POST", `${api_url}/login`, {
      username: "meptest",
      password: "meptest"
    }).then(res => {
      cy.setCookie("token", res.body.token);
      cy.setCookie("user", JSON.stringify(res.body));
      const token = res.body.token;
      cy.request({
        method: "GET",
        url: `${api_url}/course/all`,
        headers: {
          Authorization: token
        }
      }).then(res => {
        const courses = res.body;
        const course = courses.find(elem => elem.short_name === "c1");
        cy.request({
          method: "GET",
          url: `${api_url}/user/type/student/course/${course._id}`,
          headers: {
            Authorization: token
          }
        }).then(resUser => {
          const user = resUser.body[0];
          cy.visit(`${ui_url}/etu-dashboard-pedagogic?studentId=${user._id}`);
          cy.get(":nth-child(2) > .ant-list-item").click();
          cy.get("#invalidate").click();
          cy.get(".ant-modal-body > .ant-input").type("Refus de la fiche");
          cy.get(
            "body > div:nth-child(14) > div > div.ant-modal-wrap > div > div.ant-modal-content > div.ant-modal-footer > button.ant-btn.ant-btn-primary"
          ).click();
          cy.get(
            ":nth-child(6) > .ant-list > .ant-spin-nested-loading > .ant-spin-container > .ant-list-items > :nth-child(2) > .ant-list-item > .ant-list-item-meta > .ant-list-item-meta-content"
          ).click();
          cy.get("#form_cmi_description").type("UPDATED");
          cy.get("#validate").click();

          cy.get(":nth-child(2) > .ant-card-grid").contains(
            "2 / 5 semaines validées"
          );
          cy.get(
            ":nth-child(5) > .ant-list > .ant-spin-nested-loading > .ant-spin-container > .ant-list-items > :nth-child(1) > .ant-list-item > .ant-list-item-meta > .ant-list-item-meta-content"
          ).contains("critTest");
          cy.get(
            ":nth-child(7) > .ant-list > .ant-spin-nested-loading > .ant-spin-container > .ant-list-items > :nth-child(1) > .ant-list-item > .ant-list-item-meta > .ant-list-item-meta-content > .ant-list-item-meta-title > a > h3"
          )
            .contains("critWeek Etablissement")
            .click();
          cy.get("#form_cmi_description").should(
            "have.value",
            "DescriptionUPDATED"
          );
        });
      });
    });
  });

  it("Week criterion updated in student", function() {
    cy.get(":nth-child(2) > .ant-card-grid").contains(
      "2 / 5 semaines validées"
    );
  });

  it("Modify refused form", function() {
    cy.get(
      ":nth-child(5) > .ant-list > .ant-spin-nested-loading > .ant-spin-container > .ant-list-items > :nth-child(1) > .ant-list-item > .ant-list-item-meta > .ant-list-item-meta-content > .ant-list-item-meta-title > a > h3"
    ).click();
    cy.get("#form_cmi_description").type("UPDATED");
    cy.get("#modify").click();
    cy.get(
      ".ant-layout-content > div:nth-child(6) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > ul:nth-child(1) > div:nth-child(2) > li:nth-child(1) > div:nth-child(1)"
    ).click();
    cy.get("#form_cmi_description").should(
      "have.value",
      "descriptionTestUPDATED"
    );
  });

  it("Student cannot modify validated forms", function() {
    cy.get(
      ":nth-child(7) > .ant-list > .ant-spin-nested-loading > .ant-spin-container > .ant-list-items > :nth-child(1) > .ant-list-item > .ant-list-item-meta > .ant-list-item-meta-content > .ant-list-item-meta-title > a > h3"
    ).click();
    cy.get("#modify").should("not.be.visible");
  });

  it("MEP validates criteria", function() {
    cy.get(".ant-col-xs-1 > .ant-btn");
    const api_url = Cypress.env("API_URL");
    const ui_url = Cypress.env("UI_URL");
    cy.request("POST", `${api_url}/login`, {
      username: "meptest",
      password: "meptest"
    }).then(res => {
      cy.setCookie("token", res.body.token);
      cy.setCookie("user", JSON.stringify(res.body));
      const token = res.body.token;
      cy.request({
        method: "GET",
        url: `${api_url}/course/all`,
        headers: {
          Authorization: token
        }
      }).then(res => {
        const courses = res.body;
        const course = courses.find(elem => elem.short_name === "c1");
        cy.request({
          method: "GET",
          url: `${api_url}/user/type/student/course/${course._id}`,
          headers: {
            Authorization: token
          }
        }).then(resUser => {
          const user = resUser.body[0];
          cy.visit(`${ui_url}/etu-dashboard-pedagogic?studentId=${user._id}`);
          cy.get(
            ":nth-child(1) > .ant-card-grid > :nth-child(1) > p > .ant-btn"
          ).click();
          cy.get(":nth-child(1) > .ant-card-grid").contains("Validé");
          cy.get(":nth-child(1) > .ant-card-grid").contains(
            "Invalider Critère"
          );

          cy.get(
            ":nth-child(2) > .ant-card-grid > :nth-child(1) > p > .ant-btn"
          ).click();
          cy.get(":nth-child(2) > .ant-card-grid").contains(
            "Invalider Critère"
          );
        });
      });
    });
  });

  it("Student has its criteria validated", function() {
    cy.get(":nth-child(1) > .ant-card-grid").contains("Validé");
  });

  it("MEP invalidates criteria", function() {
    cy.get(".ant-col-xs-1 > .ant-btn");
    const api_url = Cypress.env("API_URL");
    const ui_url = Cypress.env("UI_URL");
    cy.request("POST", `${api_url}/login`, {
      username: "meptest",
      password: "meptest"
    }).then(res => {
      cy.setCookie("token", res.body.token);
      cy.setCookie("user", JSON.stringify(res.body));
      const token = res.body.token;
      cy.request({
        method: "GET",
        url: `${api_url}/course/all`,
        headers: {
          Authorization: token
        }
      }).then(res => {
        const courses = res.body;
        const course = courses.find(elem => elem.short_name === "c1");
        cy.request({
          method: "GET",
          url: `${api_url}/user/type/student/course/${course._id}`,
          headers: {
            Authorization: token
          }
        }).then(resUser => {
          const user = resUser.body[0];
          cy.visit(`${ui_url}/etu-dashboard-pedagogic?studentId=${user._id}`);
          cy.get(
            ":nth-child(1) > .ant-card-grid > :nth-child(1) > p > .ant-btn"
          ).click();
          cy.get(
            ":nth-child(2) > .ant-card-grid > :nth-child(1) > p > .ant-btn"
          ).click();

          cy.get(":nth-child(1) > .ant-card-grid").contains("Non validé");
          cy.get(":nth-child(1) > .ant-card-grid").contains("Valider Critère");
          cy.get(":nth-child(2) > .ant-card-grid").contains("Valider Critère");
        });
      });
    });
  });

  it("Student has its criteria invalidated", function() {
    cy.get(":nth-child(1) > .ant-card-grid").contains("Non validé");
  });

  it("Adds form to a student from MEP page and check mep buttons", function() {
    cy.get(".ant-col-xs-1 > .ant-btn");
    const api_url = Cypress.env("API_URL");
    const ui_url = Cypress.env("UI_URL");
    cy.request("POST", `${api_url}/login`, {
      username: "meptest",
      password: "meptest"
    }).then(res => {
      cy.setCookie("token", res.body.token);
      cy.setCookie("user", JSON.stringify(res.body));
      const token = res.body.token;
      cy.request({
        method: "GET",
        url: `${api_url}/course/all`,
        headers: {
          Authorization: token
        }
      }).then(res => {
        const courses = res.body;
        const course = courses.find(elem => elem.short_name === "c1");
        cy.request({
          method: "GET",
          url: `${api_url}/user/type/student/course/${course._id}`,
          headers: {
            Authorization: token
          }
        }).then(resUser => {
          const user = resUser.body[0];
          cy.visit(`${ui_url}/etu-dashboard-pedagogic?studentId=${user._id}`);
          cy.get(
            ".ant-layout-content > div:nth-child(4) > button:nth-child(1)"
          ).click();
          cy.get(
            "label.ant-checkbox-wrapper:nth-child(1) > span:nth-child(1) > input:nth-child(1)"
          ).click();
          cy.get("#form_cmi_description").type("Deuxième fiche");
          const fileName = "image.jpg";
          cy.fixture(fileName).then(fileContent => {
            cy.get("span.ant-upload").upload(
              { fileContent, fileName, mimeType: "image/png" },
              { subjectType: "drag-n-drop" }
            );
          });
          cy.get("button.ant-btn-primary:nth-child(2)").click();
          cy.get(
            ":nth-child(2) > .ant-list-item > .ant-list-item-meta > .ant-list-item-meta-content > .ant-list-item-meta-title > a > h3"
          ).click();
          cy.reload();
          cy.get(
            ".ant-layout-content > div:nth-child(6) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > ul:nth-child(1) > div:nth-child(3) > li:nth-child(1) > div:nth-child(1)"
          ).click();
          cy.get("#validate").should("be.visible");
          cy.get("#invalidate").should("be.visible");
          cy.get("#form_cmi_description")
            .invoke("val")
            .then(text => expect(text).to.equals("Deuxième fiche"));
        });
      });
    });
  });
});
