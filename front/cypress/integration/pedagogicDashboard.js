describe('Test pedagogic dashboard page', function() {
  beforeEach(function() {
    const api_url = Cypress.env('API_URL');
    cy.request('POST', `${api_url}/login`, { username: 'admintest', password: 'admintest' })
      .then(res => {
        cy.setCookie('token', res.body.token);
        cy.setCookie('user', JSON.stringify(res.body));
    });
  });

  it('Shows students by course and their forms to validate', function() {
    const api_url = Cypress.env('API_URL');
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/pedagogic-dashboard`);
    cy.getCookie('token')
      .then(cookie => {
        const token = cookie.value;
        cy.request({
          method: 'GET',
          url: `${api_url}/course/all`,
          headers: {
            "Authorization": token,
          }
        }).then(res => {
          const course = res.body[0];
          cy.get('div.ant-select:nth-child(2) > div:nth-child(1) > div:nth-child(1)').contains('div', course.short_name);

          cy.request({
            method: 'GET',
            url: `${api_url}/user/type/student/course/${course._id}`,
            headers: {
              "Authorization": token,
            }
          }).then(resStudents => {
            const students = resStudents.body;
            cy.request({
              method: 'GET',
              url: `${api_url}/criterion/all`,
              headers: {
                "Authorization": token,
              }
            }).then(resCrit => {
              const criterion = resCrit.body;
              let cptStudent = 1;
              let cptForm = 1;
              students.forEach(student => {
                cy.get(`.student-table > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(${cptStudent})`)
                  .contains('td', student.first_name).should('be.visible');
                cy.get(`.student-table > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(${cptStudent})`)
                  .contains('td', student.last_name).should('be.visible');
  
                student.forms_list.forEach(form => {
                  if (form.criteria.length === 1 && form.state === 'toValidate') {
                    const formCrit = criterion.find(crit => crit._id === form.criteria[0]);
                    cy.get(`div.ant-col-xs-24:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(${cptForm})`)
                      .contains('td', formCrit.name).should('be.visible');
                    cy.get(`div.ant-col-xs-24:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(${cptForm})`)
                      .contains('td', student.last_name).should('be.visible');
                    cy.get(`div.ant-col-xs-24:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(${cptForm})`)
                      .contains('td', student.first_name).should('be.visible');
                    cptForm = cptForm + 1;
                  }
                });
                cptStudent = cptStudent + 1;
              });
            });
          });
        });
      });
  });

  it('Reloads students and forms when course change', function() {
    const api_url = Cypress.env('API_URL');
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/pedagogic-dashboard`);
    cy.getCookie('token')
      .then(cookie => {
        const token = cookie.value;
        cy.request({
          method: 'GET',
          url: `${api_url}/course/all`,
          headers: {
            "Authorization": token,
          }
        }).then(res => {
          cy.get('div.ant-select:nth-child(2)').click();
          cy.get('li.ant-select-dropdown-menu-item:nth-child(2)').click();
          const course = res.body.find(elem => elem.short_name === 'c2');
          cy.get('div.ant-select:nth-child(2) > div:nth-child(1) > div:nth-child(1)').contains('div', course.short_name);

          cy.request({
            method: 'GET',
            url: `${api_url}/user/type/student/course/${course._id}`,
            headers: {
              "Authorization": token,
            }
          }).then(resStudents => {
            const students = resStudents.body;
            cy.request({
              method: 'GET',
              url: `${api_url}/criterion/all`,
              headers: {
                "Authorization": token,
              }
            }).then(resCrit => {
              const criterion = resCrit.body;
              let cptStudent = 1;
              let cptForm = 1;
              students.forEach(student => {
                cy.get(`.student-table > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(${cptStudent})`)
                  .contains('td', student.first_name).should('be.visible');
                cy.get(`.student-table > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(${cptStudent})`)
                  .contains('td', student.last_name).should('be.visible');
  
                student.forms_list.forEach(form => {
                  if (form.criteria.length === 1 && form.state === 'toValidate') {
                    const formCrit = criterion.find(crit => crit._id === form.criteria[0]);
                    cy.get(`div.ant-col-xs-24:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(${cptForm})`)
                      .contains('td', formCrit.name).should('be.visible');
                    cy.get(`div.ant-col-xs-24:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(${cptForm})`)
                      .contains('td', student.last_name).should('be.visible');
                    cy.get(`div.ant-col-xs-24:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(${cptForm})`)
                      .contains('td', student.first_name).should('be.visible');
                    cptForm = cptForm + 1;
                  }
                });
                cptStudent = cptStudent + 1;
              });
            });
          });
        });
      });
  });

  it('Opens form modal when form is clicked and validate it', function() {
    const api_url = Cypress.env('API_URL');
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/pedagogic-dashboard`);
    cy.getCookie('token')
      .then(cookie => {
        const token = cookie.value;
        cy.request({
          method: 'GET',
          url: `${api_url}/course/all`,
          headers: {
            "Authorization": token,
          }
        }).then(res => {
          const course = res.body[0];
          cy.get('div.ant-select:nth-child(2) > div:nth-child(1) > div:nth-child(1)').contains('div', course.short_name);
          
          cy.get('div.ant-col-xs-24:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1)')
            .click();

          cy.request({
            method: 'GET',
            url: `${api_url}/user/type/student`,
            headers: {
              "Authorization": token,
            }
          }).then(allStudents => {
            const student = allStudents.body.find(elem => elem.first_name === 'student');
            cy.request({
              method: 'GET',
              url: `${api_url}/form/student/${student._id}`,
              headers: {
                "Authorization": token,
              }
            }).then(resForms => {
              let descriptionValue;
              cy.get('#form_cmi_description')
                .invoke('val')
                .then(text => {
                  descriptionValue = text;
                  const form = resForms.body.find(elem => elem.description === descriptionValue);
    
                  cy.get('#validate').should('be.visible');
                  cy.get('#invalidate').should('be.visible');
                  cy.get('#validate')
                    .click();
    
                  cy.request({
                    method: 'GET',
                    url: `${api_url}/form/${form._id}`,
                    headers: {
                      "Authorization": token,
                    }
                  }).then(resForm => {
                    const formValues = resForm.body;
                    expect(formValues.description).to.equals(descriptionValue);
                    expect(formValues.state).to.equals('validated');
                  });
                });
            });
          });
        });
      });
  });

  it('Click on a student to visit its dashboard', function() {
    const api_url = Cypress.env('API_URL');
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/pedagogic-dashboard`);
    cy.getCookie('token')
      .then(cookie => {
        const token = cookie.value;
        cy.request({
          method: 'GET',
          url: `${api_url}/course/all`,
          headers: {
            "Authorization": token,
          }
        }).then(res => {
          const course = res.body[0];
          cy.get('div.ant-select:nth-child(2) > div:nth-child(1) > div:nth-child(1)').contains('div', course.short_name);
          
          cy.get('.student-table > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1)')
            .click();
          
          cy.location('href').should('include', 'etu-dashboard-pedagogic?studentId=');
        });
      });
  });
});
    