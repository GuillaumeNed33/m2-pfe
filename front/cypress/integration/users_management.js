/* eslint-disable no-undef */
describe('Test coursesManagement page', () => {
  beforeEach(function() {
    const api_url = Cypress.env('API_URL');
    cy.request('POST', `${api_url}/login`, { username: 'admintest', password: 'admintest' })
      .then(res => {
        cy.setCookie('token', res.body.token);
        cy.setCookie('user', JSON.stringify(res.body));
    });
  });

  it('Loads all users', () => {
    const api_url = Cypress.env('API_URL');
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/users-management`);

    cy.getCookie('token')
      .then(cookie => {  
        const token = cookie.value;      
        cy.request({
          method: 'GET',
          url: `${api_url}/user/all`,
          headers: {
            "Authorization": token,
          }
        }).then(res => {
          expect(res.status).to.equal(200);
          res.body.forEach((user) => {
            cy.get('.ant-table-tbody').contains('td', user.first_name).should('be.visible');
            cy.get('.ant-table-tbody').contains('td', user.last_name).should('be.visible');
          });
        });
      });
  });

  it('Adds a user', () => {
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/users-management`);

    cy.get('#UserForm_first_name')
      .type('cypress_first')
      .should('have.value', 'cypress_first');
    cy.get('#UserForm_last_name')
      .type('cypress_last')
      .should('have.value', 'cypress_last');
    cy.get('#UserForm_login')
      .type('cypress')
      .should('have.value', 'cypress');
    cy.get('#UserForm_password')
      .type('cypress')
      .should('have.value', 'cypress');
    cy.get('#UserForm_student_number')
      .type('1')
      .should('have.value', '1');
    cy.get('#UserForm_study_year')
      .click();
    cy.get('li.ant-select-dropdown-menu-item:nth-child(3)')
      .click();
    cy.get('#UserForm_courses > label:nth-child(2) > span:nth-child(1) > input:nth-child(1)')
      .click();
  
    cy.get('#createUser')
      .click();
  
    cy.get('.ant-table-tbody').contains('td', 'cypress_first').should('be.visible');
    cy.get('.ant-table-tbody').contains('td', 'cypress_last').should('be.visible');
  });

  it('Updates a user', () => {
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/users-management`);

    cy.get('tr.ant-table-row:nth-child(5) > td:nth-child(6) > span:nth-child(1) > button:nth-child(1)')
      .click();
    cy.get('#UpdateUserForm_first_name')
      .clear()
      .type('UPDATED')
      .should('have.value', 'UPDATED');
    cy.get('#modifyUser')
      .click();
    cy.get('.ant-table-tbody').contains('td', 'UPDATED').should('be.visible');
  });

  it('Deletes a user', () => {
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/users-management`);

    cy.get('.ant-table-tbody').find('tr').then((trs) => {
      const numberOfElems = trs.length;
      cy.get('tr.ant-table-row:nth-child(5) > td:nth-child(6) > span:nth-child(1) > button:nth-child(3)')
        .click();
      cy.get('.ant-modal-footer > button:nth-child(1)')
        .click();
      cy.get('.ant-table-tbody').find('tr').should('have.length', numberOfElems - 1);
    });
  });
});
