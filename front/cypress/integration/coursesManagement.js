describe('Test coursesManagement page', function() {
  beforeEach(function() {
    const api_url = Cypress.env('API_URL');
    cy.request('POST', `${api_url}/login`, { username: 'admintest', password: 'admintest' })
      .then(res => {
        cy.setCookie('token', res.body.token);
        cy.setCookie('user', JSON.stringify(res.body));
    });
  });

  it('Creates a course', function() {
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/courses-management`);

    cy.get('#CourseForm_short_name')
      .type('short')
      .should('have.value', 'short');

    cy.get('#CourseForm_long_name')
      .type('long')
      .should('have.value', 'long');

    cy.get('#createCourse')
      .click();

    cy.get('.ant-table-tbody').contains('td', 'short').should('be.visible');
    cy.get('.ant-table-tbody').contains('td', 'long').should('be.visible');
  });

  it('Modifies a course', function() {
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/courses-management`);
    
    cy.get('tr.ant-table-row:nth-child(2) > td:nth-child(4) > span:nth-child(1) > button:nth-child(1)')
      .click();
    
    cy.get('.ant-table-expanded-row > td:nth-child(2) > form:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(1) > span:nth-child(1) > input:nth-child(2)')
      .type('_modif');
    cy.get('.ant-table-expanded-row > td:nth-child(2) > form:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > span:nth-child(1) > span:nth-child(1) > input:nth-child(2)')
      .type('_modif');

    cy.get('#modifyCourse')
      .click();
      
    cy.get('.ant-table-tbody').contains('td', 'short_modif').should('be.visible');
    cy.get('.ant-table-tbody').contains('td', 'long_modif').should('be.visible');
  });

  it('Deletes a course', function() {
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/courses-management`);
    
    cy.get('tr.ant-table-row:nth-child(2) > td:nth-child(4) > span:nth-child(1) > button:nth-child(3)')
      .click();
    
    cy.get('.ant-modal-footer > button:nth-child(1)')
      .click();
    
    cy.get('tr.ant-table-row:nth-child(2)').should('not.exist');
  });

  it('Loads all courses', function() {
    const api_url = Cypress.env('API_URL');
    const ui_url = Cypress.env('UI_URL');
    cy.visit(`${ui_url}/courses-management`);

    cy.getCookie('token')
      .then(cookie => {  
        const token = cookie.value;      
        cy.request({
          method: 'GET',
          url: `${api_url}/course/all`,
          headers: {
            "Authorization": token,
          }
        }).then(res => {
          expect(res.status).to.equal(200);
          res.body.forEach(course => {
            cy.get('.ant-table-tbody').contains('td', course.short_name).should('be.visible');
            cy.get('.ant-table-tbody').contains('td', course.long_name).should('be.visible');
          });
        });
      });
  });
});
  