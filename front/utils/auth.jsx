import { useEffect } from 'react';
import Router from 'next/router';
import nextCookie from 'next-cookies';
import cookie from 'js-cookie';
import axios from 'axios'

const REDIRECT_URL = '/unauthorized';
const LOGOUT_URL = '/documentation';

export const login = ({ user, token }) => {
  cookie.set('token', token);
  cookie.set('user', user);
};

export const getAuthToken = () => {
  return cookie.get('token');
}

export const getAuthTokenSSR = (ctx) => {
  const { token } = nextCookie(ctx);
  return token;
}

export const getApiURL = (ctx) => {
  return ctx.req && ctx.res ? process.env.API_SSR_URL : process.env.API_URL;
}

export const getAuthUser =() => {
  return cookie.get('user');
}

export const verifyToken = (API_URL, token, restriction = []) => {
  const data = (restriction.length === 0) ? {} : { roles: restriction }
  return axios.post(`${API_URL}/auth`, data, { headers: {"Authorization" : token} })
}

export const auth = async (ctx, restriction) => {
  const { token } = nextCookie(ctx);
  const API_URL = getApiURL(ctx)
  // If there's no token, it means the user is not logged in.
  if (!token) {
    redirectionUnauthorized(ctx);
  } else {
    try {
      await verifyToken(API_URL, token, restriction)
    } catch (e) {
      console.error(e, "AUTH VERIFICATION")
      redirectionUnauthorized(ctx);
    }
  }
  return {token}
};

const redirectionUnauthorized = (ctx) => {
  if (typeof window === 'undefined') {
    ctx.res.writeHead(302, { Location: REDIRECT_URL });
    ctx.res.end();
  } else {
    Router.push(REDIRECT_URL);
  }
}

export const logout = () => {
  cookie.remove('token');
  cookie.remove('user');
  // to support logging out from all windows
  window.localStorage.setItem('logout', Date.now());
  Router.push(LOGOUT_URL);
};

export const withAuthSync = (WrappedComponent, restrictedType = []) => {
  const Wrapper = (props) => {
    const syncLogout = (event) => {
      if (event.key === 'logout') {
        console.log('logged out from storage!');
        Router.push(LOGOUT_URL);
      }
    };
    useEffect(() => {
      window.addEventListener('storage', syncLogout);
      
      return () => {
        window.removeEventListener('storage', syncLogout);
        window.localStorage.removeItem('logout');
      };
    }, []);
    return <WrappedComponent {...props} />;
  };
  
  Wrapper.getInitialProps = async (ctx) => {
    const token = await auth(ctx, restrictedType);
    const componentProps = WrappedComponent.getInitialProps
      && (await WrappedComponent.getInitialProps(ctx));
    return { ...componentProps, ...token };
  };
  return Wrapper;
};
