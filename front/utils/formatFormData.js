export const formatData = (values, criteria) => {
  const criteriaIds = [];
  values.criterion.forEach(critTag => {
    const matchingCrit = criteria.find(elem => elem.tag === critTag);
    if (matchingCrit) {
      criteriaIds.push(matchingCrit._id);
    }
  });
  const status = 'toValidate'
  const description = (typeof values.description !== 'undefined') ? values.description : null
  const company = (typeof values.company !== 'undefined') ? values.company : null
  const address = (typeof values.address !== 'undefined') ? values.address : null
  const address_c = (typeof values.address_c !== 'undefined') ? values.address_c : null
  const postal_code = (typeof values.postal_code !== 'undefined') ? values.postal_code : null
  const city = (typeof values.city !== 'undefined') ? values.city : null
  const country = (typeof values.country !== 'undefined') ? values.country : null
  const job_title = (typeof values.job_title !== 'undefined') ? values.job_title : null
  const start_date = (typeof values.start_end_date !== 'undefined') ? (
    values.start_end_date[0].toDate()
  ) : null
  const end_date = (typeof values.start_end_date !== 'undefined') ? (
    values.start_end_date[1].toDate()
  ) : null

  const allData = {
    state: status,
    criteria: criteriaIds,
    submit_date: Date.now(),
    review_date: status === 'validated' ? new Date.now() : null,
    description: description,
    rejection_reason: null,
  };
  if (company) {
    allData.company = company;
  }
  if (address) {
    allData.address = address;
  }
  if (address_c) {
    allData.address_c = address_c;
  }
  if (postal_code) {
    allData.postal_code = postal_code;
  }
  if (city) {
    allData.city = city;
  }
  if (country) {
    allData.country = country;
  }
  if (job_title) {
    allData.job_title = job_title;
  }
  if (start_date) {
    allData.start_date = start_date;
  }
  if (end_date) {
    allData.end_date = end_date;
  }

  return allData;
}

export const setFormData = (data, files, oldData) => {
  const formData = new FormData();
  if (data.student) {formData.append("student", data.student);}
  else if (oldData){
    formData.append('student', oldData.student);
  }
  if (data.state) formData.append("state", data.state);
  if (data.criteria) {
    data.criteria.forEach(crit => {
      formData.append("criteria[]", crit);
    });
  }
  if (data.submit_date) formData.append("submit_date", data.submit_date);
  if (data.review_date) formData.append("review_date", data.review_date);
  if (data.description) formData.append("description", data.description);
  if (data.company) formData.append("company", data.company);
  if (data.address) formData.append("address", data.address);
  if (data.address_c) formData.append("address_c", data.address_c);
  if (data.postal_code) formData.append("postal_code", data.postal_code);
  if (data.city) formData.append("city", data.city);
  if (data.country) formData.append("country", data.country);
  if (data.job_title) formData.append("job_title", data.job_title);
  if (data.start_date) formData.append("start_date", data.start_date);
  if (data.end_date) formData.append("end_date", data.end_date);
  if (files) {
    files.fileList
      .map(file => file.originFileObj)
      .forEach(file => {
        formData.append("file", file);
      });
  }
  return formData;
};
