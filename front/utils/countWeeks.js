
import moment from 'moment';


function roundNumberUp(num) {
  return Math.floor(num);
}

function getNumberOfWeeks(endDate, startDate) {
  const startDateMoment = moment(startDate);
  const endDateMoment = moment(endDate);
  const days = Math.round(startDateMoment.diff(endDateMoment, "days"));
  return roundNumberUp(days / 7);
}

// eslint-disable-next-line import/prefer-default-export
export const getCriteriaNumberOfWeeks = function (validatedForms, criteria) {
  const criteriaNumberOfWeeksDict = {}; // To count total of days for all criteria
  const criteriaCountDaysDict = {};

  criteria.forEach((criterion) => {
    if (criterion.count_total_weeks) {
      criteriaCountDaysDict[criterion._id] = criterion.minimum_weeks_to_validate;
      criteriaNumberOfWeeksDict[criterion._id] = 0;
    }
  });

  validatedForms.forEach((form) => {
    form.criteria.forEach((criterionId) => {
      if (Object.keys(criteriaCountDaysDict).includes(criterionId)) {
        const startDate = moment(form.start_date);
        const endDate = moment(form.end_date);
        const nbWeeks = getNumberOfWeeks(startDate, endDate);
        criteriaNumberOfWeeksDict[criterionId] += nbWeeks;
      }
    });
  });
  return criteriaNumberOfWeeksDict;
};
