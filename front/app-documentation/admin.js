
const admin = `## Utilisateur Membre de l'équipe pédagogique

### Page d'accueil et Tableau de bord

En se connectant avec son nom d'utilisateur et son mot de passe, le membre de l'équipe pédagogique arrive sur son tableau de bord.

Le tableau de bord contient deux tableaux. L'un pour visualiser les étudiants et l'autre pour voir les fiches en attente de ces étudiants. En arrivant sur la page le tableau des étudiants et donc des fiches est filtré sur 1 cursus, ici IMSAT (c'est un exemple). En cliquant sur le selecteur de cursus, une liste déroulante apparaît permettant de changer le cursus selectionné, parmis les cursus où l'utilisateur est un membre de l'équipe pédagogique. Lorsque le cursus change, le tableau des étudiants et des fiches en attentes sont rechargés. Le fonctionnement est le même pour filtrer d'après le semestre des étudiants. Le tableau des étudiants affiche également l'avancement des critères pour les étudiants. Chaque critère est représenté par une bulle, en mettant la souris sur une bulle sur la ligne d'un étudiant le nom du critère est affiché. Un critère qui n'est pas validé est en gris, s'il est validé alors il apparaît en vert.

En cliquant sur l'une des fiches en attente d'un étudiant un pop-up s'ouvre avec le récapitulatif de la fiche.

Le membre de l'équipe pédagogique peut ensuite "Valider" la fiche ou la "Refuser", ce qui ouvre un nouveau pop-up demandant le motif du refus de la fiche. Ce motif sera affiché à l'étudiant sur son Tableau de bord.

### Tableau de bord d'un étudiant

En cliquant sur un étudiant (une ligne du tableau des étudiants) le membre de l'équipe pédagogique est redirigé sur le tableau de bord de l'étudiant.

Sur cette page, le membre de l'équipe pédagogique peut :

* Valider un critère pour l'étudiant en cliquant sur "Valider" au dessus d'un des critères.

* Créer une fiche pour un étudiant en y complétant les informations et le ou les justificatifs.

* Consulter les fiches refusées, en attente ou validées de l'étudiant, en cliquant dessus.

* Modifier et/ou Valider une fiche refusée.

* Modifier et/ou Valider une fiche en attente.

* Modifier et/ou Refuser une fiche en attente.

* Refuser une fiche validée (si elle a été validé par erreur par exemple).

### Gestion des utilisateurs

En cliquant sur l'onglet "Gestion des utilisateurs" l'utilisateur accède à cette page.

Sur la gauche de la page un formulaire permet de créer un utilisateur. L'utilisateur à créer est soit un étudiant soit un membre de l'équipe pédagogique. L'utilisateur créé ne peut être ajouté qu'à un cursus dont fait partie le membre de l'équipe pédagogique connecté.

Des étudiants peuvent être créés en grand nombre en cliquant sur "Import CSV" dans la formualire de création. Il faut ensuite glisser-déposer le fichier au format .csv dans la zone prévu à cet effet, ou cliquer sur cetet zone pour sélectionner le fichier à importer. Le format du fichier .csv (les colonnes à créer et remplir pour chaque étudiant) sont accessible en cliquant sur le lien "Informations sur le format du fichier" ouvrant un pop-up d'explication du format.

Sur la droite de la page un tableau récapitulatif des utilisateurs déjà créés, qui sont des le ou les cursus du membre de l'équipe pédagogique connecté. En cliquant sur "Modifier" il est possible de changer les informations d'un utilisateur. En cliquant sur "Supprimer" un pop-up s'ouvre demandant confirmation de suppression.

### Gestion de la documentation

En cliquant sur l'onglet "Gestion Documentation" l'utilisateur est redirigé sur cette page.

C'est ici que le membre de l'équipe pédagogique peut écrire la documentation de son ou ses cursus. Pour chacun de ses cursus un onglet est affiché, en cliquant dessus, l'utilisateur peut écrire la documentation de ce cursus grâce à un éditeur de texte. Pour valider la documentation, il après l'avoir rédigée cliquer sur "Publier". Cette documentation est visualisable par les étudiants du cursus concerné.

### Documentation

Sur cette page le membre de l'équipe pédagogique peut consulter les documentations de son ou ses cursus afin d'en vérifier le contenu. Mais également avoir un rappel du fonctionnement des pages auxquels l'utilisateur a accès (cette documentation) et la documentation globale du portail CMI, rédigée par un administrateur.

## Utilisateur Administrateur

### Pages similaires aux membres de l'équipe pédagogique

Un administrateur est membre de l'équipe pédagogique ayant accès à tous les cursus. De ce fait, il a accès aux mêmes pages (plus d'autres détaillées plus bas) avec l'accès à tous les cursus :

* Sur la page de gestion utilisateur il voit tous les utilisateurs et peut créer des administrateurs.

* Sur son tableau de bord il peut trier son tableau sur tous les cursus et donc accéder à tous les étudiants.

* Sur la page de gestion de documentation il peut rédiger en plus la documentation globale.

### Gestion des cursus

En cliquant sur "Gestion cursus" l'administrateur arrive sur cette page.

Depuis cette page, accessible qu'aux administrateurs, il est possible de créer un cursus en spécifiant son nom court (acronyme) et long. Il apparaît ensuite dans la liste des cursus et peut être modifier ou supprimer. L'administrateur pourra ensuite depuis la gestion utilisateur créer des étudiants / membre de l'quipe pédagogique pour ce cursus ou ajouter des membre de l'équipe pédagogique à ce cursus en modifiant les membres de l'équipe pédagogique, et leur ajoutant ce cursus.`;

export default admin;
