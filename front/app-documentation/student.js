const student = `## Utilisateur Étudiant

### Page d'accueil et Tableau de bord

En se connectant avec son nom d'utilisateur et son mot de passe, l'étudiant arrive sur son tableau de bord.

Sur son tableau de bord l'étudiant peut consulter ses critères, savoir s'ils sont validés ou non et voir l'avancement concernant ces critères (semaines validées) pour les critères de Stage ou de Mobilité. Les critères en cours de validation (et avancés) sont en bleu, la coloration du cercle avance en fonction de l'avancée du critère. Un critère validé apparaît en vert. Un récapitulatif est disponible en haut à drotie en cliquant sur l'icone "?".

En cliquant sur le bouton "Créer une fiche", un pop-up s'ouvre permettant à l'étudiant de compléter des informations pour soumettre une fiche pour avancer la validation d'un critère. Les champs à compléter dépendent du critère sélectionné. Pour une mobilité ou un stage des informations concernant la destination sont demandées alors que pour le niveau d'anglais seulement une description et un document prouvant cette validation (TOEIC ou TOEFL) sont demandées. Au moins une pièce justificative est demandée pour une fiche.

En validant sa soumission en cliquant sur "Confirmer", la fiche apparaît dans la section "Fiches en attente". Cette fiche pourra ensuite être validée ou refusée par un Membre de l'équipe pédagogique.

Sous le bouton de création de fiche, trois parties permettent de visualiser les fiches précédement créées. Pour chaque fiche, la date la plus à droite est la date de soumission de la fiche. La date en rouge est la date du refus (pour les fiches refusées) et celle en verte la date de validation (pour les fiches validées).

* Les "Fiches en attente de validation" sont les fiches créées par l'étudiant et en attente de révision par un membre de l'équipe pédagogique afin qu'il la valide ou la refuse (avec un motif écrit par ce dernier).

* Les "Fiches refusées" sont les fiches qui ont été revues par un membre de l'équipe pédagogique et qui a jugé non recevable la fiche pour la ou les critères demandés. La raison du refus est signalée en  passant la souris sur la fiche refusée.

* Les "Fiches validées" sont les fiches qui ont été acceptées par un membre de l'équipe pédagogique. De ce fait l'étudiant ne peut plus modifier ces fiches.

En cliquant sur une fiche un pop-up s'ouvre permettant de visualiser les informations complétées pour une fiche. C'est par ce moyen qu'une fiche refusée ou en attente de validation peut également être modifiée, avant revue par un membre de l'équipe pédagogique. Une fiche refusée qui est modifée repasse en "Attente de validation".

Lors de la modification d'une fiche, si une pièce jointe est ajoutée alors les anciennes seront écrasées. Pour ajouter une pièce-jointe et garder les anciennes il faut donc remettre les anciennes avec la nouvelle. Si aucune pièce-joitne n'est ajouté lors d'une modification, les anciennes sont gardées.

### Documentation

En cliquant sur l'onglet "Documentation" l'étudiant est redirigé sur la page des documentations. Il peut sur cette page consulter la documentation de l'application (cette documentation), la documentation globale (rédigée par un administrateur) et la documentation relative à son cursus (rédigée par les membre de l'équipe pédagogique de ce cursus).`;

export default student;