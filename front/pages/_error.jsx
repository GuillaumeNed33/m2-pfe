import React from 'react';
import { Result } from 'antd';
import { withAuthSync } from '../utils/auth'

function Error({ statusCode }) {
  const errorMessage = {
    403: "Vous n'êtes pas autorisé à accéder à cette page !",
    404: "Cette page est introuvable !",
    500: "Une erreur interne est survenue !",
    "serveur": `Une erreur ${statusCode} est survenue !`,
    "client": `Une erreur est survenue côté client !`
  }
  
  const message = statusCode ?
    (statusCode && statusCode !== 403 && statusCode !== 404 && statusCode !== 500) ? errorMessage["serveur"] : errorMessage[statusCode]
    : errorMessage["client"]
  
  const status = ( statusCode === null || (statusCode && statusCode !== 403 && statusCode !== 404 && statusCode !== 500) )?
    "warning" : `${statusCode}`
  
  const title = statusCode !== 403 ? statusCode : 401
  
  return (
    <Result
      status={"warning"}
      title={title}
      subTitle={message}
    />
  );
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
};

export default (Error);
