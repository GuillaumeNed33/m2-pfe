import React from "react";
import axios from 'axios';
import PropTypes from 'prop-types';
import { Tabs } from 'antd';
import { markdown } from 'markdown';
import { getApiURL, getAuthTokenSSR } from '../utils/auth';
const { TabPane } = Tabs;
import { getAuthUser } from '../utils/auth';
import admin from '../app-documentation/admin';
import pedagogic from '../app-documentation/pedagogic';
import student from '../app-documentation/student';

class DocumentationPage extends React.Component {
  static async getInitialProps(ctx) {
    let allProps = {};
    try {
      const API_URL = getApiURL(ctx)
      const token = getAuthTokenSSR(ctx);
      let res;
      if (token) {
        res = await axios.get(`${API_URL}/documentation/all`, {headers: { "Authorization": token }});
      } else {
        res = await axios.get(`${API_URL}/documentation/all`);
      }
      const { data } = res;
      allProps = { docs: data };
    } catch (err) {
      console.error(err);
      allProps = { docs: [] };
    }
    return allProps;
  }
  
  constructor(props) {
    super(props);
    this.state = {
      docs: this.props.docs,
    }
  }
  
  render() {
    const docs = [...this.state.docs];
    let docApp;
    let appPane;
    const userString = getAuthUser();
    const user = userString ? JSON.parse(userString) : null;
    if (user) {
      if (user.type === 'student') {
        docApp = markdown.toHTML(student);
      } else if (user.type === 'mep') {
        docApp = markdown.toHTML(pedagogic);
      } else if (user.type === 'admin') {
        docApp = markdown.toHTML(admin);
      }
      appPane =
        <TabPane tab={"Application"} key={"app"}>
          <div dangerouslySetInnerHTML={{__html: docApp.replace(/<script.*>.*<\/script>/ims, '')}} />
        </TabPane>;
    }

    docs.forEach(d => {
      d.title = d.type === "global" ? "Documentation Globale" : d.course.long_name
    })
    if(docs.length > 0) {
      return (
        <>
          <Tabs type="card">
            {appPane}
            {
              docs.map((d) => (
                <TabPane tab={d.title} key={d._id}>
                  <div dangerouslySetInnerHTML={{__html: d.content.replace(/<script.*>.*<\/script>/ims, '')}} />
                </TabPane>
              ))
            }
          </Tabs>
        </>
      );
    } else if (user && !(docs.length > 0)) {
      return (
        <>
          <Tabs type="card">
            {appPane}
          </Tabs>
        </>
      );
    } else {
      return (
        <>
          <div>Aucune documentation n'est disponible</div>
        </>
      );
    }
  }
}

DocumentationPage.propTypes = {
  docs: PropTypes.arrayOf(PropTypes.object)
};

DocumentationPage.defaultProps = {
  docs: []
};

export default DocumentationPage;
