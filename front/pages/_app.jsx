import React from 'react';
import App from 'next/app';
import nextCookie from 'next-cookies'
import PropTypes from 'prop-types'
import { getApiURL, verifyToken } from '../utils/auth'
import CustomLayout from '../components/layout';

class MyApp extends App {
  // Only uncomment this method if you have blocking data requirements for
  // every single page in your application. This disables the ability to
  // perform automatic static optimization, causing every page in your app to
  // be server-side rendered.
  //
  static async getInitialProps(appContext) {
    // calls page's `getInitialProps` and fills `appProps.pageProps`
    const appProps = await App.getInitialProps(appContext);
    let connected = true;
    if(typeof appProps.pageProps === "undefined" || !appProps.pageProps.token) {
      try {
        const { token } = nextCookie(appContext.ctx);
        const API_URL = getApiURL(appContext.ctx)
        if (token) {
          await verifyToken(API_URL, token)
        } else {
          connected = false;
        }
      } catch (e) {
        connected = false;
      }
    }
    const allProps = {
      ...appProps,
      isConnected: connected
    }
    return { ...allProps }
  }
  
  render() {
    const { Component, pageProps } = this.props;
    return <CustomLayout connected={this.props.isConnected}><Component {...pageProps} /></CustomLayout>;
  }
}

MyApp.propTypes = {
  isConnected: PropTypes.bool.isRequired,
};

MyApp.defaultProps = {
  isConnected: false,
};

export default MyApp;
