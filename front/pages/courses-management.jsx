import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types'
import { withAuthSync, getAuthTokenSSR, getApiURL } from '../utils/auth'
import CourseForm from '../components/courses/courseForm';
import CoursesTable from '../components/courses/coursesTable';
import { Card } from 'antd';

class CourseManagementPage extends React.Component {
  static async getInitialProps(ctx) {
    let allProps = {};
    const API_URL = getApiURL(ctx)
    const token = getAuthTokenSSR(ctx);
    try {
      const res = await axios.get(`${API_URL}/course/all`, {headers: { "Authorization": token }})
      const { data } = res;
      allProps = { courses: data };
    } catch (err) {
      console.error(err);
      allProps = { courses: [] };
    }
    return allProps;
  }
  
  constructor(props) {
    super(props);
    this.state = {
      courses: this.props.courses
    };
    this.updateCourse = this.updateCourse.bind(this);
    this.addCourse = this.addCourse.bind(this);
    this.deleteCourse = this.deleteCourse.bind(this);
  }
  
  addCourse(course) {
    const { courses } = this.state;
    let copyCourses = [];
    if (courses) {
      copyCourses = [...courses];
    }
    copyCourses.push(course);
    this.setState({
      courses: copyCourses,
    });
  }
  
  updateCourse(course) {
    const { courses } = this.state;
    let copyCourses = [];
    if (courses) {
      copyCourses = [...courses];
    }
    const modifiedCourses = copyCourses.map((elem) => {
      if(elem._id === course._id) {
        return course;
      } else {
        return elem;
      }
    });
    this.setState({
      courses: modifiedCourses,
    });
  }
  
  deleteCourse(course) {
    const { courses } = this.state;
    let copyCourses = [];
    if (courses) {
      copyCourses = [...courses];
    }
    const indexCourse = copyCourses.indexOf(course);
    copyCourses.splice(indexCourse, 1);
    this.setState({
      courses: copyCourses,
    });
  }
  
  render() {
    const { courses } = this.state;
    return (
      <div className={"page-content"}>
        <Card title="Ajouter un cursus" className={"card-to-add"}>
          <CourseForm addCourse={this.addCourse} />
        </Card>
        <Card title="Tableau des cursus" className={"card-to-display"}>
          <CoursesTable
            courses={courses}
            onUpdate={this.updateCourse}
            onDelete={this.deleteCourse}
          />
        </Card>
      </div>
    );
  }
}

CourseManagementPage.propTypes = {
  courses: PropTypes.arrayOf(PropTypes.object).isRequired,
};

CourseManagementPage.defaultProps = {
  courses: [],
};

export default withAuthSync(CourseManagementPage, ["admin"]);
