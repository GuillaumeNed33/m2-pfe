import React from 'react';
import Error from './_error'

const UnauthorizedPage = () => {
  return (<Error statusCode={403}/>);
};

export default UnauthorizedPage;
