import React from "react";
import PropTypes from 'prop-types'
import { getApiURL, getAuthTokenSSR, withAuthSync } from '../utils/auth'
import DashboardStudent from '../components/dashboard/dashboardStudent';
import axios from 'axios'

class EtuDashboardPage extends React.Component {
  static async getInitialProps(ctx) {
    let allProps = {};
    const API_URL = getApiURL(ctx)
    const token = getAuthTokenSSR(ctx);
    try {
      const student = (
        await axios.get(`${API_URL}/user`, {
          headers: {Authorization: token}
        })
      ).data
      let criteria = (
        await axios.get(`${API_URL}/criterion/all`, {
          headers: {Authorization: token}
        })
      ).data;
      allProps = {
        student: student,
        criteria: criteria,
      };
    } catch (err) {
      console.error(err);
      allProps = {
        student: {},
        criteria: []
      };
    }
    return allProps;
  }

  constructor(props) {
    super(props);
    this.state = {
      student: this.props.student,
      criteria: this.props.criteria
    }
  }
  
  render() {
    return (<DashboardStudent student={this.state.student} criteria={this.state.criteria} />);
  }
}

EtuDashboardPage.propTypes = {
  student: PropTypes.object.isRequired,
  criteria: PropTypes.arrayOf(PropTypes.object).isRequired
};
EtuDashboardPage.defaultProps = {
  student: null,
  criteria: []
};
export default withAuthSync(EtuDashboardPage, ["student"]);
