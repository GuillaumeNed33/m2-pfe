/* eslint-disable react/prop-types */
/* eslint-disable no-underscore-dangle */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import axios from 'axios';
import { getAuthToken, withAuthSync, getAuthTokenSSR, getApiURL } from '../utils/auth'
import UserForm from '../components/users/userForm';
import UsersTable from '../components/users/usersTable';
import { Card, message } from 'antd';
import PropTypes from 'prop-types'

class UsersManagementPage extends React.Component {
  static async getInitialProps(ctx) {
    let ret = {
      users: [],
      courses: [],
    };
    try {
      const token = getAuthTokenSSR(ctx);
      const API_URL = getApiURL(ctx)
      ret.courses = (await axios.get(`${API_URL}/course/all`, { headers: { "Authorization": token } })).data;
      ret.users = (await axios.get(`${API_URL}/user/all`, { headers: { "Authorization": token } })).data;
      return {...ret};
    } catch (e) {
      console.error(e)
      return {...ret}
    }
  }
  
  constructor(props) {
    super(props);
    this.addUser = this.addUser.bind(this);
    this.addMultipleUsers = this.addMultipleUsers.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.incrementStudyYear = this.incrementStudyYear.bind(this);
    this.deleteMultipleStudents = this.deleteMultipleStudents.bind(this)
    this.state = {
      users: this.props.users,
      courses: this.props.courses,
    };
  }
  
  addMultipleUsers(users) {
    this.setState({
      users: [...this.state.users].concat(users)
    })
  }
  
  addUser(user) {
    const cpUsers = [...this.state.users];
    this.setState({
      users: cpUsers.concat(user)
    })
  }
  
  updateUser(user) {
    const cpUsers = [...this.state.users];
    this.setState({
      users: cpUsers.map((u) => (u._id === user._id ? user : u))
    })
  }

  async incrementStudyYear(studentIds){
    const token = getAuthToken();
    axios.put(`${process.env.API_URL}/user/incrementStudyYear`, {studentIds: studentIds}, { headers: { "Authorization": token } })
      .then((response) => {
        if (response.status === 200) {
          message.success('Les étudiants séléctionnés ont changé d\'année')
          let users = this.state.users;
          users.forEach(user => {
            if(studentIds.includes(user._id)){
              if(user.study_year < 4)
                user.study_year++;
            }
          })
        this.setState({
          users: users
        })
        }
      }).catch((e) => {
      console.error(e)
      message.error('Une erreur est survenue lors du changement d\'année des étudiants.');
    });
  }

  async deleteMultipleStudents(studentIds){
    const token = getAuthToken();
    axios.put(`${process.env.API_URL}/user/deleteMultipleStudents`, {studentIds: studentIds}, { headers: { "Authorization": token } })
      .then((response) => {
        if (response.status === 200) {
          message.success('Les étudiants séléctionnés ont été supprimés')
          let users = this.state.users;
          this.setState({
            users: users.filter((user) => (!studentIds.includes(user._id)))
          })
        }
      }).catch((e) => {
      console.error(e)
      message.error('Une erreur est survenue lors de la suppression des étudiants.');
    });
    
  }
  
  deleteUser(userToDelete) {
    const token = getAuthToken();
    axios.delete(`${process.env.API_URL}/user/${userToDelete._id}`, { headers: { "Authorization": token } })
      .then((response) => {
        if (response.status === 200) {
          const cpUsers = [...this.state.users];
          this.setState({
            users: cpUsers.filter((user) => (user._id !== userToDelete._id))
          });
          message.success('Utilisateur supprimé avec succès !');
        }
      }).catch((e) => {
      console.error(e)
      message.error('Erreur à la suppression de l\'utilisateur.');
    });
  }
  
  render() {
    const { courses, users } = this.state;
    return (
      <div className={"page-content"}>
        <Card title="Ajouter un utilisateur" className={"card-to-add"}>
          <UserForm
            courses={courses}
            addUser={this.addUser}
            addMultipleUsers={this.addMultipleUsers}
          />
        </Card>
        <Card title="Tableau des utilisateurs" className={"card-to-display"}>
          <UsersTable
              courses={courses}
              users={users}
              deleteUser={this.deleteUser}
              updateUser={this.updateUser}
              incrementStudyYear={this.incrementStudyYear}
              deleteMultipleStudents={this.deleteMultipleStudents}
          />
        </Card>
      </div>
    );
  }
}

UsersManagementPage.propTypes = {
  courses: PropTypes.arrayOf(PropTypes.object).isRequired,
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
};

UsersManagementPage.defaultProps = {
  courses: [],
  users: [],
};

export default withAuthSync(UsersManagementPage, ["mep", "admin"]);
