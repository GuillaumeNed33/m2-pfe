import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types'
import { withAuthSync, getAuthTokenSSR, getApiURL } from '../utils/auth'
import DocEditor from '../components/docs/docEditor';

class DocManagementPage extends React.Component {
  static async getInitialProps(ctx) {
    const allProps = {
      courses: [],
      docs: []
    }
    const token = getAuthTokenSSR(ctx);
    const API_URL = getApiURL(ctx)
    try {
      const courses = await axios.get(`${API_URL}/course/all`, {
        params: { forDocumentation: true },
        headers: { "Authorization": token }
      })
      allProps.courses = courses.data;
      const docs = await axios.get(`${API_URL}/documentation/all`,{
        params: { withoutPopulateCourse: true },
        headers: { "Authorization": token }
      })
      allProps.docs = docs.data;
      return {...allProps}
    } catch (e) {
      console.error(e)
      return {...allProps}
    }
  }
  
  constructor(props) {
    super(props);
    this.state = {
      courses: this.props.courses,
      docs: this.props.docs,
    }
    this.addDocumentation = this.addDocumentation.bind(this);
    this.updateDocumentation = this.updateDocumentation.bind(this);
  }
  
  addDocumentation(doc) {
    let copyDocs = [...this.state.docs];
    copyDocs.push(doc);
    this.setState({
      docs: copyDocs,
    });
  }
  
  updateDocumentation(doc) {
    let copyDocs = [...this.state.docs];
    let indexDoc = copyDocs.findIndex(d => d._id === doc._id);
    if(indexDoc !== -1) {
      copyDocs[indexDoc] = doc;
      this.setState({
        docs: copyDocs,
      });
    } else {
      console.error(doc, "Doc not found.")
    }
  }
  
  render() {
    const { courses, docs } = this.state;
    return (
      <>
        <DocEditor
          addDocumentation={this.addDocumentation}
          updateDocumentation={this.updateDocumentation}
          courses={courses}
          docs={docs}
        />
      </>
    );
  }
}

DocManagementPage.propTypes = {
  courses: PropTypes.arrayOf(PropTypes.object).isRequired,
  docs: PropTypes.arrayOf(PropTypes.object).isRequired,
};

DocManagementPage.defaultProps = {
  courses: [],
  docs: [],
};

export default withAuthSync(DocManagementPage, ["mep", "admin"]);
