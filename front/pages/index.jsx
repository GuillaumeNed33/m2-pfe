import React from 'react';
import Router from 'next/router'

const IndexPage = () => {
  return (<></>);
};

IndexPage.getInitialProps= async ({res}) => {
  if (res) {
    res.writeHead(301, {
      Location: '/documentation'
    });
    res.end();
  } else {
    await Router.push('/documentation')
  }
}

export default IndexPage;
