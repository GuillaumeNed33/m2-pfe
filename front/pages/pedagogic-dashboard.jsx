import React from 'react';
import axios from 'axios';
import moment from 'moment';
import PropTypes from 'prop-types';
import { getAuthToken, withAuthSync, getAuthTokenSSR, getApiURL } from '../utils/auth'
import FormsTable from '../components/pedagogicDashboard/formsTable';
import StudentsTable from '../components/pedagogicDashboard/studentsTable';
import PDFDownloadButton from '../components/pedagogicDashboard/pdfDownloadButton';
import { Card, Select } from 'antd';
const { Option } = Select;

class PedagogicDashboardPage extends React.Component {
  static async getInitialProps(ctx) {
    const allProps = {
      courses: [],
      students: [],
      forms: [],
      criteria: [],
    };
    try {
      const API_URL = getApiURL(ctx)
      const token = getAuthTokenSSR(ctx);
      const criteriaRes = await axios.get(`${API_URL}/criterion/all`, {headers: { "Authorization": token }});
      const criteria = criteriaRes.data;
      allProps.criteria = criteria;
      const coursesRes = await axios.get(`${API_URL}/course/all`, {headers: { "Authorization": token }});
      const courses = coursesRes.data;
      allProps.courses = courses;
      if (courses.length > 0) {
        const firstCourse = courses[0];
        const studentsRes = await axios.get(`${API_URL}/user/type/student/course/${firstCourse._id}`, {headers: { "Authorization": token }});
        const students = studentsRes.data;
        allProps.students = students;
        if (students.length > 0) {
          students.forEach((student) => {
            student.forms_list.forEach((form) => {
              if (form.state === 'toValidate') {
                form.first_name = student.first_name;
                form.last_name = student.last_name;
                form.formated_submit_date = moment(form.submit_date).format('DD-MM-YYYY');
                form.study_year = student.study_year;
                let criteriaName = '';
                form.criteria.forEach((critId) => {
                  const criteriaInfo = criteria.find((crit) => crit._id === critId);
                  criteriaName += `${criteriaInfo.name} `;
                });
                form.criteriaNames = criteriaName;
                form.criteriaTag = form.criteria.map(((crit) => criteria.find((critToFind) => crit === critToFind._id).tag));
                allProps.forms.push(form);
              }
            })
          });
        }
      }
    } catch (error) {
      console.error(error);
    }
    return allProps;
  }
  
  constructor(props) {
    super(props);
    this.state = {
      courses: this.props.courses,
      criteria: this.props.criteria,
      students: this.props.students,
      filteredStudents: this.props.students,
      forms: this.props.forms,
      filteredForms: this.props.forms,
      selectedCourse: this.props.courses[0] ? this.props.courses[0] : null,
      selectValue: null,
    };
    this.onCourseChange = this.onCourseChange.bind(this);
    this.onYearChange = this.onYearChange.bind(this);
    this.deleteForm = this.deleteForm.bind(this);
  }
  
  async onCourseChange(value) {
    const allForms = [];
    const { criteria } = this.state;
    try {
      const token = getAuthToken();
      const studentsRes = await axios.get(`${process.env.API_URL}/user/type/student/course/${value}`, { headers: { Authorization: token } });
      const students = studentsRes.data;
      if (students.length > 0) {
        students.forEach((student) => {
          student.forms_list.forEach((form) => {
            if (form.state === 'toValidate') {
              form.first_name = student.first_name;
              form.last_name = student.last_name;
              form.formated_submit_date = moment(form.submit_date).format('DD-MM-YYYY');
              form.study_year = student.study_year;
              let criteriaName = '';
              form.criteria.forEach((critId) => {
                const criteriaInfo = criteria.find((crit) => crit._id === critId);
                criteriaName += `${criteriaInfo.name} `;
              });
              form.criteriaNames = criteriaName;
              form.criteriaTag = form.criteria.map(((crit) => criteria.find((critToFind) => crit === critToFind._id).tag));
              allForms.push(form);
            }
          })
        });
        this.setState({
          students: [...students],
          forms: [...allForms],
          filteredForms: [...allForms],
          filteredStudents: [...students],
          selectedCourse: this.state.courses.find((elem) => elem._id === value),
          selectValue: null,
        });
      } else {
        this.setState({
          students: [],
          filteredStudents: [],
          forms: [],
          filteredForms: [],
          selectedCourse: this.state.courses.find((elem) => elem._id === value),
          selectValue: null,
        });
      }
    } catch (error) {
      this.setState({
        students: [],
        filteredStudents: [],
        forms: [],
        filteredForms: [],
        selectedCourse: this.state.courses.find((elem) => elem._id === value),
        selectValue: null,
      });
      console.error(error);
    }
  }

  onYearChange(value) {
    const { students, forms } = this.state;
    if (value && students && forms) {
      const cpStudents = [...students];
      const cpForms = [...forms];
      const studentsFiltered = cpStudents.filter((student) => student.study_year === value - 1);
      const formsFiltered = cpForms.filter((form) => form.study_year === value - 1);
      this.setState({
        filteredStudents: studentsFiltered,
        filteredForms: formsFiltered,
        selectValue: value,
      });
    } else {
      this.setState({
        filteredStudents: [...students],
        filteredForms: [...forms],
        selectValue: null,
      });
    }
  }
  
  deleteForm(form) {
    const { forms, filteredForms } = this.state;
    const cpForms = [...forms];
    const cpFilteredForms = [...filteredForms];
    const cpFormsFilter = cpForms.filter((formToDel) => formToDel._id !== form._id);
    const cpFormFilteredFilter = cpFilteredForms.filter((formToDel) => formToDel._id !== form._id);
    this.setState({
      forms: cpFormsFilter,
      filteredForms: cpFormFilteredFilter,
    });
  }
  
  render() {
    const {
      courses, filteredStudents, filteredForms, criteria, selectedCourse, selectValue
    } = this.state;
    const defaultSelect = selectedCourse ? selectedCourse.short_name : null;
    const allOptions = courses.map((course) => (
      <Option key={course._id} value={course._id}>
        {' '}
        {course.short_name}
        {' '}
      </Option>
    ));
    const studyYear = [...Array(5).keys()].map((key) => key += 1);
    const studyOptions = studyYear.map((year) => (
      <Option key={year} value={year}>
        {year <= 3 ? 'L' + year: 'M' + (year-3)}
      </Option>
    ));
    
    return (
      <div className={"page-content"}>
        <Card title="Étudiants" className={"card-to-add"}>
          <div className={"inline"} style={{minWidth: 500, marginBottom: "2%"}}>
            <span>Cursus : </span>
            <Select
              style={{ width: 120 }}
              defaultValue={defaultSelect}
              onChange={this.onCourseChange}
            >
              {allOptions}
            </Select>
            <span style={{ marginLeft: 20 }}>Année : </span>
            <Select
              style={{ width: 120 }}
              onChange={this.onYearChange}
              placeholder="Année"
              value={selectValue}
            >
              <Option key="empty" value={null}> Tous </Option>
              {studyOptions}
            </Select>
            <PDFDownloadButton
              students={filteredStudents}
              course={selectedCourse}
              criteria={criteria}
              year={selectValue}
            />
          </div>
          <StudentsTable
            criteria={criteria}
            students={filteredStudents}
          />
        </Card>
        <Card title="Fiches en attente" className={"card-to-display"}>
          <FormsTable
            criteria={criteria}
            forms={filteredForms}
            deleteForm={this.deleteForm}
          />
        </Card>
      </div>
    );
  }
}

PedagogicDashboardPage.propTypes = {
  courses: PropTypes.arrayOf(PropTypes.object).isRequired,
  criteria: PropTypes.arrayOf(PropTypes.object).isRequired,
  students: PropTypes.arrayOf(PropTypes.object).isRequired,
  forms: PropTypes.arrayOf(PropTypes.object).isRequired,
};

PedagogicDashboardPage.defaultProps = {
  courses: [],
  criteria: [],
  students: [],
  forms: [],
};

export default withAuthSync(PedagogicDashboardPage, ["mep", "admin"]);
