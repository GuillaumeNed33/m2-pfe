import React from "react";
import PropTypes from 'prop-types'
import { getApiURL, getAuthTokenSSR, withAuthSync } from '../utils/auth'
import DashboardStudent from '../components/dashboard/dashboardStudent';
import axios from 'axios'
import Router from 'next/router'

class EtuDashboardPedagogicPage extends React.Component {
  static async getInitialProps(ctx) {
    let allProps = {};
    const API_URL = getApiURL(ctx)
    const token = getAuthTokenSSR(ctx);
    try {
      const student = (
        await axios.get(`${API_URL}/user/${ctx.query.studentId}`, {
          headers: {Authorization: token}
        })
      ).data;
      let criteria = (
        await axios.get(`${API_URL}/criterion/all`, {
          headers: {Authorization: token}
        })
      ).data;
      allProps = {
        student: student,
        criteria: criteria,
      };
    } catch (err) {
      allProps = {
        student: null,
        criteria: []
      };
    }
    if(allProps.student) {
      return allProps;
    } else {
      if (ctx.res) {
        ctx.res.writeHead(301, {
          Location: '/unauthorized'
        });
        ctx.res.end();
      } else {
        await Router.push('/unauthorized')
      }
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      student: this.props.student,
      criteria: this.props.criteria
    }
  }

  render() {
    return (<DashboardStudent student={this.state.student} criteria={this.state.criteria} showMepPage />);
  }
}

EtuDashboardPedagogicPage.propTypes = {
  student: PropTypes.object.isRequired,
  criteria: PropTypes.arrayOf(PropTypes.object).isRequired
};
EtuDashboardPedagogicPage.defaultProps = {
  student: null,
  criteria: []
};

export default withAuthSync(EtuDashboardPedagogicPage, ["mep", "admin"]);
