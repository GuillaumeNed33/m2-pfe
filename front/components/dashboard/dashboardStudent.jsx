import React from "react";
import PropTypes from "prop-types";
import { getCriteriaNumberOfWeeks } from "../../utils/countWeeks";
import FormModal from "../forms/form-modal";
import CriterionBoard from "./criterionBoard";
import UserForms from "./userForms";
import DashboardDocumentation from "./dashboardDocumentation";
import { Button, Popover } from "antd";

class DashboardStudent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      criteria: [],
      validatedForms: [],
      toValidateForms: [],
      refusedForms: [],
      student: {}
    };
    this.updateStudentCriteria = this.updateStudentCriteria.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.formCreated = this.formCreated.bind(this);
  }
  
  componentDidMount() {
    let validatedForms = [];
    let toValidateForms = [];
    let refusedForms = [];
    if(this.props.student && typeof this.props.student.forms_list !== "undefined") {
      this.props.student.forms_list.forEach(form => {
        switch (form.state) {
          case "validated":
            validatedForms.push(form);
            break;
          case "toValidate":
            toValidateForms.push(form);
            break;
          case "refused":
            refusedForms.push(form);
            break;
          default:
        }
      });
    }
    this.props.student.criteriaNumberOfWeeksDict = getCriteriaNumberOfWeeks(
      validatedForms,
      this.props.criteria
    );
    
    this.setState({
      validatedForms: validatedForms,
      toValidateForms: toValidateForms,
      refusedForms: refusedForms,
      student: {...this.props.student},
      criteria: [...this.props.criteria]
    });
  }
  
  updateStudentCriteria(student) {
    student.criteriaNumberOfWeeksDict = getCriteriaNumberOfWeeks(
      this.state.validatedForms,
      this.state.criteria
    );
    this.setState({
      student: {...student}
    });
  }
  
  formCreated(form) {
    const { student } = this.state;
    const { showMepPage } = this.props;
    
    if (showMepPage) {
      const { validatedForms } = this.state;
      const validatedFormsCp = [...validatedForms];
      validatedFormsCp.push(form);
      
      student.criteriaNumberOfWeeksDict = getCriteriaNumberOfWeeks(
        validatedFormsCp,
        this.state.criteria
      );
      
      this.setState({
        validatedForms: validatedFormsCp,
        student: student,
      });
    } else {
      const { toValidateForms } = this.state;
      const toValidateCp = [...toValidateForms];
      toValidateCp.push(form);
      this.setState({
        toValidateForms: toValidateCp
      });
    }
  }
  
  handleUpdate(formId, data, oldState) {
    const { refusedForms, toValidateForms, validatedForms } = this.state;
    let filteredRefused = [...refusedForms];
    let filteredToValidate = [...toValidateForms];
    let filteredValidated = [...validatedForms];
    
    if (oldState === "refused") {
      filteredRefused = filteredRefused.filter(elem => elem._id !== formId);
    } else if (oldState === "toValidate") {
      filteredToValidate = filteredToValidate.filter(elem => elem._id !== formId);
    } else {
      filteredValidated = filteredValidated.filter(elem => elem._id !== formId);
    }
    
    if (data.state === "refused") {
      filteredRefused.push(data);
    } else if (data.state === "toValidate") {
      filteredToValidate.push(data);
    } else {
      filteredValidated.push(data);
    }
    this.setState({
      refusedForms: filteredRefused,
      toValidateForms: filteredToValidate,
      validatedForms: filteredValidated
    });
  }
  
  render() {
    let showMepPage = this.props.showMepPage;
    return (
      <>
        <p>
          <DashboardDocumentation />
          {this.state.student.description && (
            <Popover
              placement="bottomLeft"
              title={"Description"}
              content={this.state.student.description}
            >
              <Button>
                {this.state.student.first_name +
                " " +
                this.state.student.last_name +
                " - n° : " +
                this.state.student.student_number}
              </Button>
            </Popover>
          )}
          {!this.state.student.description && (
            <Button>
              {this.state.student.first_name +
              " " +
              this.state.student.last_name +
              " - n° : " +
              this.state.student.student_number}
            </Button>
          )}
        </p>
        
        <hr style={{ marginBottom: "2%" }} />
        
        <CriterionBoard
          criteria={this.state.criteria}
          student={this.state.student}
          showMepPage={showMepPage}
          reloadData={this.updateStudentCriteria}
        />
        
        <FormModal
          studentId={this.state.student._id}
          criteria={this.state.criteria}
          addForm={this.formCreated}
          showMepPage={showMepPage}
        />
        
        <UserForms
          title="Fiches refusées"
          showMepPage={showMepPage}
          forms={this.state.refusedForms}
          criteria={this.state.criteria}
          iconType="close-circle"
          iconColor="#A52A2A"
          onUpdate={this.handleUpdate}
        />
        
        <UserForms
          title="Fiches en attente de validation"
          showMepPage={showMepPage}
          forms={this.state.toValidateForms}
          criteria={this.state.criteria}
          iconType="warning"
          iconColor="#daa520"
          onUpdate={this.handleUpdate}
        />
        
        <UserForms
          title="Fiches validées"
          showMepPage={showMepPage}
          forms={this.state.validatedForms}
          criteria={this.state.criteria}
          iconType="check-circle"
          iconColor="#52c41a"
          onUpdate={this.handleUpdate}
        />
      </>
    );
  }
}

DashboardStudent.propTypes = {
  student: PropTypes.object.isRequired,
  criteria: PropTypes.array,
  showMepPage: PropTypes.bool
};

DashboardStudent.defaultProps = {
  student: {},
  criteria: [],
  showMepPage: false
};

export default DashboardStudent;
