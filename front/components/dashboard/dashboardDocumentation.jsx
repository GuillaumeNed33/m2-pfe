import React from "react";
import { Button, Popover, Progress } from "antd";
import PropTypes from 'prop-types';

class DashboardDocumentation extends React.Component {
  render() {
    return (
        <Popover
          title={"Aide"}
          content={
            <div>
              <div>
                Un critère validé apparaît comme ceci : {"  "}
                <Progress
                  type="circle"
                  percent={100}
                  width={50}
                  format={() => {
                    return "Critère";
                  }}
                />
              </div>
              <div>
                Un critère non validé apparaît comme ceci : {"  "}
                <Progress
                  type="circle"
                  percent={99}
                  width={50}
                  format={() => {
                    return "Critère";
                  }}
                />{" "}
                ou{" "}
                <Progress
                  type="circle"
                  percent={0}
                  width={50}
                  format={() => {
                    return "Critère";
                  }}
                />
              </div>
              <div>
                Dans une fiche nous avons{" "}
                <Button
                  style={{
                    borderColor: "#ff4d4f",
                    color: "#ff4d4f"
                  }}
                >
                  Date de refus
                </Button>{" "}
                /{" "}
                <Button
                  style={{
                    borderColor: "#73d13d",
                    color: "#73d13d"
                  }}
                >
                  Date de validation
                </Button>{" "}
                <Button>Date de soumission</Button>
              </div>
            </div>
          }
        >
          <Button style={{ float: "right" }}>?</Button>
        </Popover>
    );
  }
}

DashboardDocumentation.propTypes = {};

DashboardDocumentation.defaultProps = {};

export default DashboardDocumentation;
