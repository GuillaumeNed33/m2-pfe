import React from "react";
import moment from "moment";
import axios from "axios";
import WrappedForm from "../forms/form-cmi";
import { getAuthToken } from "../../utils/auth";
import { formatData, setFormData } from "../../utils/formatFormData";
import { Button, Icon, Input, List, message, Modal, Popover, Tag } from "antd";
import PropTypes from "prop-types";

const { TextArea } = Input;

class UserForms extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      isModalOpen: false,
      isRefusedModalOpen: false,
      refusingReason: null,
      displayedSection: [],
      selectedForm: null
    };
    this.closeModal = this.closeModal.bind(this);
    this.acceptForm = this.acceptForm.bind(this);
    this.openRefusingModal = this.openRefusingModal.bind(this);
    this.closeRefuseModal = this.closeRefuseModal.bind(this);
    this.refuseForm = this.refuseForm.bind(this);
    this.updateReason = this.updateReason.bind(this);
    this.updateForm = this.updateForm.bind(this);
  }
  
  componentDidMount() {
    this.addCriterionTagsToForms();
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.forms !== prevProps.forms) {
      this.addCriterionTagsToForms();
    }
  }
  
  addCriterionTagsToForms() {
    this.props.forms.forEach(form => {
      form.criteriaTag = form.criteria.map(
        crit =>
          this.props.criteria.find(critToFind => crit === critToFind._id).tag
      );
    });
  }
  
  saveFormRef = formRef => {
    this.formRef = formRef;
  };
  
  closeModal() {
    this.setState({
      isModalOpen: false,
      displayedSection: [],
      selectedForm: null
    });
  }
  
  acceptForm() {
    try {
      const { form } = this.formRef.props;
      form.validateFields(async (err, values) => {
        if (!err) {
          const review_date = moment().format();
          const token = getAuthToken();
          const oldState = this.state.selectedForm.state;
          const data = formatData(values, this.props.criteria);
          const resUpdate = await axios.put(
            `${process.env.API_URL}/form/${this.state.selectedForm._id}`,
            data,
            { headers: { Authorization: token } }
          );
          const resValidate = await axios.put(
            `${process.env.API_URL}/form/${this.state.selectedForm._id}`,
            {
              state: "validated",
              review_date: review_date
            },
            { headers: { Authorization: token } }
          );
          if (resUpdate.status === 200 && resValidate.status === 200) {
            this.props.onUpdate(this.state.selectedForm._id, resValidate.data, oldState);
            this.setState({
              selectedForm: null,
              displayedSection: [],
              isModalOpen: false
            });
          }
        }
      });
    } catch (err) {
      console.error(err);
      message.error("Une erreur s'est produite.");
    }
  }
  
  openRefusingModal() {
    this.setState({
      isRefusedModalOpen: true
    });
  }
  
  closeRefuseModal() {
    this.setState({
      isRefusedModalOpen: false,
      refusingReason: null
    });
  }
  
  async refuseForm() {
    if (this.state.refusingReason) {
      try {
        const token = getAuthToken();
        let oldState = this.state.selectedForm.state;
        const review_date = moment().format();
        const resRefuse = await axios.put(
          `${process.env.API_URL}/form/${this.state.selectedForm._id}`,
          {
            state: "refused",
            rejection_reason: this.state.refusingReason,
            review_date: review_date
          },
          { headers: { Authorization: token } }
        );
        if (resRefuse.status === 200) {
          this.props.onUpdate(this.state.selectedForm._id, resRefuse.data, oldState);
          message.success("Fiche refusée !");
          this.closeRefuseModal();
          this.closeModal();
        }
      } catch (err) {
        message.error("Une erreur s'est produite.");
        console.error(err);
      }
    } else {
      message.error("La raison de refus doit être renseignée.", 5);
    }
  }
  
  updateForm() {
    try {
      const { form } = this.formRef.props;
      form.validateFields(async (err, values) => {
        if (!err) {
          const token = getAuthToken();
          const oldState = this.state.selectedForm.state;
          const data = formatData(values, this.props.criteria);
          data.state = oldState === "validated" ? "validated" : data.state;
          const formData = setFormData(data, values.files,this.state.selectedForm);
          const resUpdate = await axios.put(
            `${process.env.API_URL}/form/${this.state.selectedForm._id}`,
            formData, { headers : { "Authorization": token ,
            "Content-Type":
              "multipart/form-data; boundary=---01100001011100000110100"} }
          );
          this.props.onUpdate(this.state.selectedForm._id, resUpdate.data, oldState);
          message.success("Fiche modifiée !");
          this.closeModal();
          form.resetFields();
        }
      });
    } catch (err) {
      message.error("Une erreur s'est produite.");
      console.error(err);
    }
  }
  
  updateReason(event) {
    this.setState({
      refusingReason: event.target.value
    });
  }
  
  render() {
    const {
      isModalOpen,
      displayedSection,
      isRefusedModalOpen,
      refusingReason
    } = this.state;
    let criteriaDict = {};
    this.props.criteria.forEach(criterion => {
      criteriaDict[criterion._id] = criterion.name;
    });
    
    return (
      <div style={{ marginBottom: "2%", marginTop: "2%" }}>
        <h2>{this.props.title}</h2>
        <hr style={{ marginBottom: "10px" }} />
        <List
          bordered
          id='formList'
          itemLayout="horizontal"
          dataSource={this.props.forms}
          renderItem={form => (
            <div>
              <List.Item
                className='listItem'
                onClick={() => {
                  this.setState({
                    selectedForm: form,
                    displayedSection: form.criteriaTag,
                    isModalOpen: true
                  });
                }}
              >
                <Popover
                  placement="bottom"
                  title={
                    form.state === "refused" && form.rejection_reason ? (
                      <p style={{ color: "red" }}>Raison du rejet</p>
                    ) : (
                      "Description"
                    )
                  }
                  content={
                    form.state === "refused" && form.rejection_reason ? (
                      <p style={{ color: "red" }}>{form.rejection_reason}</p>
                    ) : (
                      form.description
                    )
                  }
                >
                  <List.Item.Meta
                    title={
                      <a>
                        <h3>
                          <Icon
                            type={this.props.iconType}
                            theme="twoTone"
                            twoToneColor={this.props.iconColor}
                            style={{ marginRight: "10px" }}
                          />
                          {form.criteria.map(criterionId => {
                            return criteriaDict[criterionId] + " ";
                          })}
                          {typeof form.job_title !== 'undefined' &&
                          form.job_title !== null &&
                          "(" + form.company + ")"}
                          {(typeof form.job_title === 'undefined' ||
                            form.job_title === null) &&
                          typeof form.country !== 'undefined' &&
                          form.country !== null &&
                          "(" + form.country + ")"}
                          
                          <Button style={{ float: "right" }}>
                            {moment(form.submit_date).format("DD/MM/YYYY")}
                          </Button>
                          {form.state !== "toValidate" && form.review_date && (
                            <Button
                              style={
                                form.state === "validated"
                                  ? {
                                    float: "right",
                                    borderColor: "#73d13d",
                                    color: "#73d13d",
                                    marginRight: "1%"
                                  }
                                  : {
                                    float: "right",
                                    borderColor: "#ff4d4f",
                                    color: "#ff4d4f",
                                    marginRight: "1%"
                                  }
                              }
                            >
                              {moment(form.review_date).format("DD/MM/YYYY")}
                            </Button>
                          )}
                        </h3>
                      </a>
                    }
                  />
                </Popover>
              </List.Item>
            </div>
          )}
        >
          <WrappedForm
            visible={isModalOpen}
            displayedSection={displayedSection}
            wrappedComponentRef={this.saveFormRef}
            criteria={this.props.criteria}
            formCMI={this.state.selectedForm}
            handleSelectedCriterion={value =>
              this.setState({ displayedSection: value })
            }
            onCancel={this.closeModal}
            disableFields={
              !this.props.showMepPage &&
              this.state.selectedForm &&
              this.state.selectedForm.state === "validated"
            }
            showMepPage={this.props.showMepPage}
            onAccept={this.acceptForm}
            onRefuse={this.openRefusingModal}
            onUpdate={this.updateForm}
          />
          <Modal
            title="Refuser la fiche"
            visible={isRefusedModalOpen}
            onCancel={this.closeRefuseModal}
            footer={[
              <Button
                key="delete"
                type="danger"
                onClick={this.closeRefuseModal}
              >
                Annuler
              </Button>,
              <Button key="close" type="primary" onClick={this.refuseForm}>
                Refuser la fiche
              </Button>
            ]}
          >
            <TextArea
              value={refusingReason}
              onChange={this.updateReason}
              placeholder="Raison du refus"
            />
          </Modal>
        </List>
      </div>
    );
  }
}

UserForms.propTypes = {
  title: PropTypes.string,
  showMepPage: PropTypes.bool,
  forms: PropTypes.array,
  criteria: PropTypes.array,
  onUpdate: PropTypes.func,
};

UserForms.defaultProps = {
  title: 'Default title',
  showMepPage: false,
  forms: [],
  criteria: [],
  onUpdate: () => { console.error('Default onUpdate called') },
};

export default UserForms;
