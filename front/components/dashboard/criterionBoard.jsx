import React from "react";
import axios from "axios";
import PropTypes from 'prop-types';
import { getAuthToken } from "../../utils/auth";
import { Card, Progress, Popover, Button, message } from "antd";

class CriterionBoard extends React.Component {
  constructor(props) {
    super(props);
    this.validateCriterion = this.validateCriterion.bind(this);
  }
  
  validateCriterion(criterion) {
    let student = this.props.student;
    let newCriteria = student.criteria;
    newCriteria.push(criterion);
    newCriteria = newCriteria.filter((a, b) => newCriteria.indexOf(a) === b);
    const token = getAuthToken();
    axios
      .put(
        process.env.API_URL + "/user/" + student._id,
        {
          criteria: newCriteria
        },
        { headers: { Authorization: token } }
      )
      .then(res => {
        message.success("Critère validé !");
        this.props.reloadData(res.data);
      })
      .catch(err => {
        console.error(err);
        message.error("Une erreur s'est produite.");
      });
  }
  
  invalidateCriterion(criterion) {
    let student = this.props.student;
    let newCriteria = student.criteria;
    const index = newCriteria.indexOf(criterion);
    if (index !== -1) newCriteria.splice(index, 1);
    const token = getAuthToken();
    axios
      .put(
        process.env.API_URL + "/user/" + student._id,
        {
          criteria: newCriteria
        },
        { headers: { Authorization: token } }
      )
      .then(res => {
        message.success("Critère invalidé !");
        this.props.reloadData(res.data);
      })
      .catch(err => {
        console.error(err);
        message.error("Une erreur s'est produite.");
      });
  }
  
  render() {
    const { student, showMepPage, criteria } = this.props;
    return (
      <div>
        <Card style={{ marginBottom: "10px" }} bordered={false}>
          {criteria.map(criterion => {
            let validatedCriterion = [];
            let showWeeks =
              typeof student !== "undefined" &&
              student &&
              Object.keys(student.criteriaNumberOfWeeksDict).includes(criterion._id);
            let numberWeeksForPercent =
              student.criteriaNumberOfWeeksDict[criterion._id] >
              criterion.minimum_weeks_to_validate
                ? criterion.minimum_weeks_to_validate
                : student.criteriaNumberOfWeeksDict[criterion._id];
            let percent =
              (showWeeks
                ? typeof student !== "undefined" &&
                student &&
                Object.keys(student.criteriaNumberOfWeeksDict).includes(criterion._id) ?
                  numberWeeksForPercent / criterion.minimum_weeks_to_validate
                  : 0
                : 0) * 99;
            if (student.criteria) {
              validatedCriterion = student.criteria.includes(criterion._id);
            }
            return (
              <div key={criterion._id}>
                <Card.Grid
                  key={criterion._id}
                  style={{
                    width: "25%",
                    textAlign: "center"
                  }}
                >
                  <div>
                    {showMepPage && (
                      <p
                        key={criterion._id}
                        style={{ textAlign: "center", marginTop: "2%" }}
                      >
                        {!validatedCriterion && (
                          <Button
                            className={"ant-btn-validate-criterion"}
                            key="validate"
                            onClick={() =>
                              this.validateCriterion(criterion._id)
                            }
                          >
                            Valider Critère
                          </Button>
                        )}
                        {validatedCriterion && (
                          <Button
                            className={"ant-btn-invalidate-criterion"}
                            key="invalidate"
                            onClick={() =>
                              this.invalidateCriterion(criterion._id)
                            }
                          >
                            Invalider Critère
                          </Button>
                        )}
                      </p>
                    )}
                  </div>
                  <Progress
                    type="circle"
                    hoverable={showMepPage.toString()}
                    percent={validatedCriterion ? 100 : showWeeks ? percent : 0}
                    format={() => {
                      return criterion.name;
                    }}
                  />
                  <div style={{ marginTop: "5%", textAlign: "center" }}>
                    <Button
                      className={"ant-btn-criterion-validation"}
                      type="dashed"
                      style={
                        showWeeks
                          ? validatedCriterion
                          ? {
                            visibility: "visible",
                            backgroundColor: "white",
                            color: "#73d13d",
                            borderColor: "#73d13d"
                          }
                          : {
                            visibility: "visible",
                            backgroundColor: "white",
                            color: "#595959",
                            borderColor: "#d9d9d9"
                          }
                          : { display: "None" }
                      }
                    >
                      {showWeeks &&
                      student.criteriaNumberOfWeeksDict[criterion._id]}
                      {(typeof criterion.minimum_weeks_to_validate !== 'undefined' &&
                        criterion.minimum_weeks_to_validate) ? 
                        (" / " + criterion.minimum_weeks_to_validate) : ""}
                      {" semaines validées"}
                    </Button>
                    {(!showWeeks || (showWeeks && validatedCriterion)) && (
                      <Button
                        className={"ant-btn-criterion-validation"}
                        type="dashed"
                        style={
                          !showWeeks
                            ? validatedCriterion
                            ? {
                              visibility: "visible",
                              backgroundColor: "white",
                              color: "#73d13d",
                              borderColor: "#73d13d"
                            }
                            : {
                              visibility: "visible",
                              backgroundColor: "white",
                              color: "#595959",
                              borderColor: "#d9d9d9"
                            }
                            : { display: "None" }
                        }
                      >
                        {validatedCriterion ? "Validé" : "Non validé"}
                      </Button>
                    )}
                  </div>
                </Card.Grid>
              </div>
            );
          })}
        </Card>
      </div>
    );
  }
}

CriterionBoard.propTypes = {
  criteria: PropTypes.array,
  student: PropTypes.object,
  showMepPage: PropTypes.bool,
  reloadData: PropTypes.func,
};

CriterionBoard.defaultProps = {
  criteria: [],
  student: {},
  showMepPage: false,
  reloadData: () => { console.error('Default reload data called') },
};

export default CriterionBoard;
