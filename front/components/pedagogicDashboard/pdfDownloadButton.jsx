import React from 'react';
import { Button, Icon } from 'antd';
import PropTypes from 'prop-types';
import {
  // eslint-disable-next-line no-unused-vars
  PDFDownloadLink, BlobProvider,
} from '@react-pdf/renderer';
import PDFrecap from './pdfRecap';

class PDFDownloadButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      PDFDocument: null,
      PDFready: false,
    };
    this.generatePDF = this.generatePDF.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { course, year } = this.props;
    if (course.short_name !== prevProps.course.short_name || year !== prevProps.year) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState(
        { PDFready: false, PDFDocument: null },
      );
    }
  }

  generatePDF() {
    const {
      students, course, criteria,
    } = this.props;
    const PDFDocument = () => (
      <PDFrecap students={students} criteria={criteria} course={course} />
    );
    this.setState({
      PDFDocument,
      PDFready: true,
    });
  }

  render() {
    const { PDFready, PDFDocument } = this.state;
    const { course } = this.props;
    let { year } = this.props;
    year = year ? `-${year}` : '';

    if (!PDFready) {
      return (
        <>
          <Button
            key="exportPDF"
            onClick={this.generatePDF}
            type="primary"
            style={{ marginLeft: 5 }}
          >
            <Icon type="file-pdf" />
          </Button>
        </>
      );
    }
    return (
      <>
        <PDFDownloadLink document={<PDFDocument />} fileName={`${new Date().toISOString().slice(0, 10)}_${course.short_name}-recap${year}.pdf`}>
          {({ loading }) => (
            loading ? (
              <Button type="primary" style={{ marginLeft: 5 }} loading />
            )
              : (
                <Button type="primary" style={{ marginLeft: 5 }}>
                  <Icon type="download" />
                </Button>
              )
          )}
        </PDFDownloadLink>
      </>
    );
  }
}

PDFDownloadButton.propTypes = {
  students: PropTypes.arrayOf(PropTypes.object),
  // eslint-disable-next-line react/forbid-prop-types
  course: PropTypes.object,
  criteria: PropTypes.arrayOf(PropTypes.object),
  year: PropTypes.number,
};

PDFDownloadButton.defaultProps = {
  students: [],
  course: null,
  criteria: [],
  year: null,
};
export default PDFDownloadButton;
