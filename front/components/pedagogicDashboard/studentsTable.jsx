import React from 'react';
import Router from 'next/router'
import PropTypes from 'prop-types';
import { Table, Avatar, Tooltip } from 'antd';

class StudentsTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableCol: [
        {
          title: 'Nom',
          dataIndex: 'last_name',
          key: 'last_name',
        },
        {
          title: 'Prénom',
          dataIndex: 'first_name',
          key: 'first_name',
        },
        {
          title: 'Critères',
          dataIndex: 'criteria',
          key: 'criteria',
          render: (studentCriteria) => (
            <span>
              {this.props.criteria.map(
                (criterion) => studentCriteria.includes(criterion._id) ?
                  <Tooltip key={criterion._id} title={criterion.name}>
                    <Avatar style={{backgroundColor: '#7cb305'}}>{criterion.name.charAt(0)}</Avatar>
                  </Tooltip> :
                  <Tooltip key={criterion._id} title={criterion.name}>
                    <Avatar style={{backgroundColor: '#bfbfbf'}}>{criterion.name.charAt(0)}</Avatar>
                  </Tooltip>
              )}
            </span>
          )
        },
      ],
    };
  }
  
  render() {
    const { tableCol } = this.state;
    const { students } = this.props;
    
    return (
      <Table
        rowKey="_id"
        dataSource={students}
        columns={tableCol}
        className={"student-table"}
        onRow={(record, index) => ({
          onClick: async (event) => {
            await Router.push(`/etu-dashboard-pedagogic?studentId=${record._id}`);
          },
        })}
      />
    );
  }
}

StudentsTable.propTypes = {
  criteria: PropTypes.arrayOf(PropTypes.object),
  students: PropTypes.arrayOf(PropTypes.object),
};

StudentsTable.defaultProps = {
  criteria: [],
  students: [],
};

export default StudentsTable;
