/* eslint-disable no-nested-ternary */

import React from 'react';
import {
  Page, Text, View, Document, StyleSheet, Image,
} from '@react-pdf/renderer';
import PropTypes from 'prop-types';
import { getCriteriaNumberOfWeeks } from '../../utils/countWeeks';

class PDFrecap extends React.Component {
  constructor(props) {
    super(props);
    const { criteria } = this.props;

    this.state = {
      styles: StyleSheet.create({
        page: {
          flexDirection: 'row',
          padding: 10,
        },
        section: {
          margin: 10,
          padding: 10,
          fontSize: 14,
          borderBottomColor: '#bbb',
          borderBottomWidth: StyleSheet.hairlineWidth,
        },
        logo: {
          width: `${100 / criteria.length}%`,
          margin: 5,
        },
        title: {
          marginRight: 'auto',
          marginLeft: 'auto',
          padding: 10,
          fontSize: 18,
          borderRadius: 4,
          borderWidth: 1,
          borderColor: '#0050b3',
        },
        date: {
          marginRight: 20,
          alignSelf: 'center',
          fontSize: 12,
        },
        row: {
          flexDirection: 'row',
          justifyContent: 'space-between',
        },
        table: {
          display: 'table',
          width: 'auto',
          borderStyle: 'solid',
          borderColor: '#bfbfbf',
          borderWidth: 1,
          borderRightWidth: 0,
          borderBottomWidth: 0,
          margin: 20,
        },
        tableRow: {
          margin: 'auto',
          flexDirection: 'row',
        },
        tableColHeader: {
          width: '20%',
          borderStyle: 'solid',
          borderColor: '#bfbfbf',
          borderBottomColor: '#000',
          borderWidth: 1,
          borderLeftWidth: 0,
          borderTopWidth: 0,
        },
        tableCol: {
          width: '20%',
          borderStyle: 'solid',
          borderColor: '#bfbfbf',
          borderWidth: 1,
          borderLeftWidth: 0,
          borderTopWidth: 0,
        },
        tableCellHeader: {
          margin: 5,
          fontSize: 12,
          fontWeight: 500,
        },
        tableCell: {
          margin: 5,
          fontSize: 10,
        },
        valid: {
          color: '#237804',
        },
        invalid: {
          color: '#f5222d',
        },
      }),
    };
  }

  render() {
    const { students, course, criteria } = this.props;
    const { styles } = this.state;
    return (
      <>
        <Document name="recap">
          <Page style={styles.body}>
            <View style={styles.row}>
              <Image style={styles.logo} src="https://upload.wikimedia.org/wikipedia/fr/b/b7/Universit%C3%A9_Bordeaux_%28Logo_2013%29.jpg" />
              <Text style={styles.date}>
                {new Date().toUTCString().slice(5, 16)}
              </Text>
            </View>
            <Text style={styles.title}>Portail CMI</Text>
            <Text style={styles.section}>
              Avancement des critères des étudiants du CMI :
              {' '}
              {`${course.long_name}`}
            </Text>
            <View style={styles.table}>
              <View style={styles.tableRow}>
                <View style={styles.tableColHeader}>
                  <Text style={styles.tableCellHeader}>Étudiant</Text>
                </View>
                {criteria.map((criterion) => (
                  <View key={criterion._id} style={styles.tableColHeader}>
                    <Text style={styles.tableCellHeader}>{criterion.name}</Text>
                  </View>
                ))}
              </View>
              {students.map((student) => (
                <View key={student._id} style={styles.tableRow}>
                  <View style={styles.tableCol}>
                    <Text style={styles.tableCell}>
                      {student.first_name}
                      {' '}
                      {student.last_name}
                    </Text>
                  </View>
                  {criteria.map((criterion) => (
                    <View key={criterion._id} style={styles.tableCol}>
                      {
                        student.criteria.includes(criterion._id)
                          ? (
                            <Text style={[styles.tableCell, styles.valid]}>VALIDÉ</Text>
                          )
                          : criterion.count_total_weeks === true
                            ? (
                              <Text style={[styles.tableCell, styles.invalid]}>
                                NON [
                                {getCriteriaNumberOfWeeks(student.forms_list.filter((form) => (form.state === 'validated')), criteria)[criterion._id]}
                                {' '}
                                semaine(s)]
                              </Text>
                            )
                            : (
                              <Text style={[styles.tableCell, styles.invalid]}>NON</Text>
                            )
                      }
                    </View>
                  ))}
                </View>
              ))}
            </View>
          </Page>
        </Document>
      </>
    );
  }
}

PDFrecap.propTypes = {
  students: PropTypes.arrayOf(PropTypes.object),
  // eslint-disable-next-line react/forbid-prop-types
  course: PropTypes.object,
  criteria: PropTypes.arrayOf(PropTypes.object),
};

PDFrecap.defaultProps = {
  students: [],
  course: null,
  criteria: [],
};
export default PDFrecap;
