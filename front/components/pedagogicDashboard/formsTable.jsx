import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import moment from 'moment';
import { getAuthToken } from '../../utils/auth';
import WrappedForm from '../forms/form-cmi';
import {
  Card, Table, Modal, Button, message, Input
} from 'antd';
const { TextArea } = Input;

class FormsTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      isRefusedModalOpen: false,
      refusingReason: null,
      displayedSection: [],
      selectedForm: null,
      tableCol: [
        {
          title: 'Critères',
          dataIndex: 'criteriaNames',
          key: 'criteriaNames',
        },
        {
          title: 'Nom',
          dataIndex: 'last_name',
          key: 'last_name',
        },
        {
          title: 'Prénom',
          dataIndex: 'first_name',
          key: 'first_name',
        },
        {
          title: 'Date soumission',
          dataIndex: 'formated_submit_date',
          key: 'formated_submit_date',
        },
      ],
    };
    this.closeModal = this.closeModal.bind(this);
    this.acceptForm = this.acceptForm.bind(this);
    this.openRefusingModal = this.openRefusingModal.bind(this);
    this.closeRefuseModal = this.closeRefuseModal.bind(this);
    this.refuseForm = this.refuseForm.bind(this);
    this.updateReason = this.updateReason.bind(this);
  }
  
  closeModal() {
    this.setState({
      isModalOpen: false,
      displayedSection: [],
      selectedForm: null,
    });
  }
  
  async acceptForm() {
    try {
      const token = getAuthToken();
      const resValidate = await axios.put(`${process.env.API_URL}/form/${this.state.selectedForm._id}`,
        {
          state: 'validated',
          review_date: moment().format(),
        }, { headers : { "Authorization": token } });
      if (resValidate.status === 200) {
        this.props.deleteForm(this.state.selectedForm);
        this.setState({
          selectedForm: null,
          displayedSection: [],
          isModalOpen: false,
        });
      }
    } catch (err) {
      console.error(err);
    }
  }
  
  openRefusingModal() {
    this.setState({
      isRefusedModalOpen: true,
    });
  }
  
  closeRefuseModal() {
    this.setState({
      isRefusedModalOpen: false,
      refusingReason: null,
    });
  }
  
  async refuseForm() {
    if (this.state.refusingReason) {
      try {
        const token = getAuthToken();
        const resRefuse = await axios.put(`${process.env.API_URL}/form/${this.state.selectedForm._id}`,
          {
            state: 'refused',
            rejection_reason: this.state.refusingReason,
            review_date: moment().format(),
          }, { headers : { "Authorization": token } });
        if (resRefuse.status === 200) {
          this.props.deleteForm(this.state.selectedForm);
          this.closeRefuseModal();
          this.closeModal();
        }
      } catch (err) {
        console.error(err);
      }
    } else {
      message.error('La raison de refus doit être renseignée.', 5);
    }
  }
  
  updateReason(event) {
    this.setState({
      refusingReason: event.target.value,
    });
  }
  
  render() {
    const {
      tableCol, isModalOpen, displayedSection, isRefusedModalOpen, refusingReason,
    } = this.state;
    const { forms } = this.props;
    
    return (
      <>
        <Table
          rowKey="_id"
          className={"forms-table"}
          dataSource={forms}
          columns={tableCol}
          onRow={(record, index) => ({
            onClick: (event) => {
              this.setState({
                selectedForm: record,
                displayedSection: record.criteriaTag,
                isModalOpen: true,
              });
            },
          })}
        />
        <WrappedForm
          visible={isModalOpen}
          displayedSection={displayedSection}
          criteria={this.props.criteria}
          formCMI={this.state.selectedForm}
          handleSelectedCriterion={(value) => this.setState({ displayedSection: value })}
          onCancel={this.closeModal}
          disableFields
          fromMepDashboard
          onAccept={this.acceptForm}
          onRefuse={this.openRefusingModal}
        />
        <Modal
          title="Refuser la fiche"
          visible={isRefusedModalOpen}
          onCancel={this.closeRefuseModal}
          footer={[
            <Button key="delete" type="danger" onClick={this.closeRefuseModal}>
              Annuler
            </Button>,
            <Button key="close" type="primary" onClick={this.refuseForm}>
              Refuser la fiche
            </Button>,
          ]}
        >
          <TextArea
            value={refusingReason}
            onChange={this.updateReason}
            placeholder="Raison du refus"
          />
        </Modal>
      </>
    );
  }
}

FormsTable.propTypes = {
  criteria: PropTypes.arrayOf(PropTypes.object),
  forms: PropTypes.arrayOf(PropTypes.object),
  deleteForm: PropTypes.func,
};

FormsTable.defaultProps = {
  criteria: [],
  forms: [],
  deleteForm: () => console.error('Delete form default called'),
};


export default FormsTable;
