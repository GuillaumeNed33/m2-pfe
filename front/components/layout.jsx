import React from 'react';
import Router from 'next/router'
import Link from 'next/link'
import { getAuthUser, logout } from '../utils/auth'
import WrappedLoginForm from './loginForm'
import '../assets/layout.less';
import { Button, Icon, Layout, Menu, Modal } from 'antd';
const { Header, Content, Sider } = Layout;

class CustomLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      visible: false,
      aProposVisible: false,
      current: ['etu-dashboard', 'pedagogic-dashboard'],
    };
  };
  
  componentDidMount() {
    const url = window.location.href;
    const urlParsed = url.split('/');
    this.setState({
      current: [urlParsed[3]],
    });
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevProps !== this.props || prevState !== this.state) {
      const url = window.location.href;
      const urlParsed = url.split('/');
      if (!this.state.current.includes(urlParsed[3])) {
        this.setState({
          current: [urlParsed[3]],
        });
      }
    }
  }
  
  onCollapse = collapsed => {
    this.setState({ collapsed });
  };
  
  changeModalVisible = (isVisible) => {
    this.setState({
      visible: isVisible,
    });
  };
  
  changeModalAProposVisible = (aProposVisible) => {
    this.setState({
      aProposVisible: aProposVisible,
    });
  };
  
  handleClick = e => {
    this.setState({
      current: e.key,
    });
  };
  
  render() {
    const {connected} = this.props;
    const userString = getAuthUser();
    const user = connected && userString ? JSON.parse(userString) : null;
    const btnLog = connected && user ?
      <Button key="logout" style={{verticalAlign: 'center'}} onClick={() => logout()}>Déconnexion</Button> :
      <Button key="login" type="primary" style={{verticalAlign: 'center' }} onClick={() => this.changeModalVisible(true)}>Connexion</Button>
    
    return (
      <Layout id="app-layout">
        <Header className="header">
          <div className="logo">
            Portail CMI
          </div>
          <div style={{float: "right"}}>
            {btnLog}
            <Button key="a-propos" icon="info-circle" type="primary" onClick={() => this.changeModalAProposVisible(true)} style={{marginLeft: 10}}/>
          </div>
        </Header>
        <Layout>
          {connected &&
          <Sider
            collapsible
            collapsed={this.state.collapsed}
            onCollapse={this.onCollapse}
            breakpoint={'lg'}
            onBreakpoint={(broken) => this.setState({
              collapse: broken,
            })}
            width={200}
            style={{ overflow: 'auto',
              height: '100vh',
              position: 'fixed',
              marginTop: 64,
              background: '#fff',
              zIndex: 1}}>
            <Menu
              mode="inline"
              selectedKeys={this.state.current}
              style={{ height: '100%', borderRight: 0 }}
              onClick={this.handleClick}
            >
              {connected && user && user.type === "student" &&
              <Menu.Item key="etu-dashboard">
                <Link href="/etu-dashboard">
                  <a>
                    <Icon type="profile"/>
                    <span>Tableau de Bord</span>
                  </a>
                </Link>
              </Menu.Item>
              } {connected && user && (user.type === "admin" || user.type === "mep") &&
            <Menu.Item key="pedagogic-dashboard">
              <Link href="/pedagogic-dashboard">
                <a>
                  <Icon type="profile"/>
                  <span>Tableau de Bord</span>
                </a>
              </Link>
            </Menu.Item>
            } {connected && user && user.type === "admin" &&
            <Menu.Item key="courses-management">
              <Link href="/courses-management">
                <a>
                  <Icon type="book"/>
                  <span>Gestion Cursus</span>
                </a>
              </Link>
            </Menu.Item>
            } {connected && user && (user.type === "admin" || user.type === "mep") &&
            <Menu.Item key="users-management">
              <Link href="/users-management">
                <a>
                  <Icon type="user"/>
                  <span>Gestion Utilisateur</span>
                </a>
              </Link>
            </Menu.Item>
            } {connected && user && (user.type === "admin" || user.type === "mep") &&
            <Menu.Item key="docs-management">
              <Link href="/docs-management">
                <a>
                  <Icon type="file-text"/>
                  <span>Gestion Documentation</span>
                </a>
              </Link>
            </Menu.Item>
            }
              {connected && user &&
              <Menu.Item key="documentation">
                <Link href="/documentation">
                  <a>
                    <Icon type="info-circle"/>
                    <span>Documentation</span>
                  </a>
                </Link>
              </Menu.Item>
              }
            </Menu>
          </Sider>
          }
          <Layout className={"layout-content"}>
            <Content
              style={{
                background: '#fff',
                padding: 24,
                margin: 0,
                minHeight: 280,
              }}
            >
              {this.props.children}
            </Content>
          </Layout>
        </Layout>
        <Modal
          title="Se connecter"
          visible={this.state.visible}
          onCancel={() => this.changeModalVisible(false)}
          footer={null}
        >
          <WrappedLoginForm
            onOk={() => this.changeModalVisible(false)}
            onCancel={() => this.changeModalVisible(false)} />
        </Modal>
        <Modal
          title="À propos"
          visible={this.state.aProposVisible}
          onCancel={() => this.changeModalAProposVisible(false)}
          footer={null}
        >
          <p>Projet de fin d'études de Master Informatique (Génie Logiciel)</p>
          <p>Réalisé du 3 février 2020 au 24 mars 2020</p>
          <div>
            <b>Etudiants :</b>
            <ul>
              <li>Cyril CAULONQUE</li>
              <li>Rayan Ryad KHELOUFI</li>
              <li>Florian MOREL</li>
              <li>Guillaume NEDELEC</li>
              <li>Florian ROULET</li>
              <li>Alfred Aboubacar SYLLA</li>
            </ul>
            <b>Encadrants :</b>
            <ul>
              <li>David AUBER</li>
              <li>Philippe NARBEL</li>
              <li>Bruno PINAUD (client)</li>
            </ul>
          </div>
        </Modal>
      </Layout>
    );
  }
  
}

export default CustomLayout;
