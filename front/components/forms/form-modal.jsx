import React from "react";
import axios from "axios";
import PropTypes from "prop-types";
import { formatData, setFormData } from "../../utils/formatFormData";
import { getAuthToken } from "../../utils/auth";
import WrappedForm from "./form-cmi";
import { Button, message } from "antd";

class FormModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      displayedSection: []
    };
  }

  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  setModalStatus = open => {
    this.setState({
      isOpen: open
    });
  };

  handleSubmit = () => {
    const { form } = this.formRef.props;
    form.validateFields(async (err, values) => {
      if (!err) {
        const data = formatData(values, this.props.criteria);
        data.student = this.props.studentId;
        if (this.props.showMepPage) {
          data.state = "validated";
          data.review_date = data.submit_date;
        }
        try {
          //New form request
          if (typeof this.props.formCMI === "undefined") {
            const resForm = await this.createFormRequest(
              setFormData(data, values.files)
            );
            message.success("Nouvelle fiche créée avec succès !");
            this.resetForm();
            this.props.addForm(resForm.data);
          }
          //Update existing form request
          else {
            await this.updateFormRequest(setFormData(data, values.files));
            this.resetForm();
            message.success("Fiche mise à jour avec succès !");
          }
        } catch (e) {
          console.error(e);
          message.error("Une erreur s'est produite.");
        }
      }
    });
  };

  resetForm = () => {
    const { form } = this.formRef.props;
    form.resetFields();
    this.setState({
      isOpen: false,
      displayedSection: []
    });
  };

  createFormRequest = data => {
    const token = getAuthToken();
    return axios.post(process.env.API_URL + "/form", data, {
      headers: {
        Authorization: token,
        "Content-Type":
          "multipart/form-data; boundary=---01100001011100000110100"
      }
    });
  };

  updateFormRequest = async data => {
    const token = getAuthToken();
    return axios.put(
      process.env.API_URL + "/form/" + this.props.formCMI._id,
      data,
      {
        headers: {
          Authorization: token,
          "Content-Type":
            "multipart/form-data; boundary=---01100001011100000110100"
        }
      }
    );
  };

  render() {
    return (
      <div>
        <Button type="primary" onClick={() => this.setModalStatus(true)}>
          Créer une fiche
        </Button>
        <WrappedForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.isOpen}
          displayedSection={this.state.displayedSection}
          criteria={this.props.criteria}
          handleSelectedCriterion={value =>
            this.setState({ displayedSection: value })
          }
          onCancel={this.resetForm}
          onSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}

FormModal.propTypes = {
  studentId: PropTypes.string,
  criteria: PropTypes.array,
  addForm: PropTypes.func,
  showMepPage: PropTypes.bool,
};
FormModal.defaultProps = {
  studentId: 'Default student id',
  criteria: [],
  addForm: () => { console.log('Default addForm called') },
  showMepPage: false,
};

export default FormModal;
