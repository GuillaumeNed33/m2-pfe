import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import {
  Form,
  Input,
  Button,
  Checkbox,
  Upload,
  Icon,
  message,
  DatePicker,
  Modal
} from 'antd';
import axios from 'axios';
import { getAuthToken } from "../../utils/auth";
import fileDownload from 'js-file-download';

const { RangePicker } = DatePicker;
const { TextArea } = Input;
const { Dragger } = Upload;

class FormCMI extends React.Component {

  constructor(props) {
    super(props);
  }

  onCriterionChange = e => {
    this.props.handleSelectedCriterion(e);
  }

  render() {
    const { visible, onCancel, onSubmit, onRefuse, onAccept, onUpdate, form, criteria, displayedSection } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };
    const disableFields = this.props.disableFields ? this.props.disableFields : false;
    const showMepPage = this.props.showMepPage ? this.props.showMepPage : false;
    const fromMepDashboard = this.props.fromMepDashboard ? this.props.fromMepDashboard : false;

    const downloadFile = (file ,form) => {
      const token = getAuthToken();
      axios.get(
        `${process.env.API_URL}/form/${form._id}/${form.student}/file/${file.uniqueName}`,
        {
          headers: {
            Authorization: token,
          },
          responseType: 'blob',
        },
      )
        .then((res) => {
          fileDownload(res.data, file.name);
        });
    };

    const draggerProps = {
      name: 'file',
      multiple: true,
      beforeUpload(file) {
        const isValidFormat = file.type === 'application/pdf' || file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isValidFormat) {
          message.error('Le fichier doit être dans un des formats suivants : PDF / JPG / PNG !');
        }
        const isValidSize = file.size / 1024 / 1024 < 5;
        if (!isValidSize) {
          message.error('Votre fichier dépasse les 5Mo !');
        }
        return isValidFormat && isValidSize;
      },
      onChange(info) {
        const { status } = info.file;
        if (status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (status === 'error') {
          message.error(`${info.file.name} : Erreur d'upload.`);
        }
      },
    };
    const defaultValues = {
      criteria: (this.props.formCMI !== null) ? this.props.formCMI.criteriaTag : null,
      description: (this.props.formCMI !== null) ? this.props.formCMI.description : null,
      company: (this.props.formCMI !== null) ? this.props.formCMI.company : null,
      address: (this.props.formCMI !== null) ? this.props.formCMI.address : null,
      address_c: (this.props.formCMI !== null) ? this.props.formCMI.address_c : null,
      postal_code: (this.props.formCMI !== null) ? this.props.formCMI.postal_code : null,
      city: (this.props.formCMI !== null) ? this.props.formCMI.city : null,
      files_links: (this.props.formCMI !== null) ? this.props.formCMI.files_links : [],
      country: (this.props.formCMI !== null) ? this.props.formCMI.country : null,
      job_title: (this.props.formCMI !== null) ? this.props.formCMI.job_title : null,
      start_end_date: (this.props.formCMI !== null) ? [moment(this.props.formCMI.start_date), moment(this.props.formCMI.end_date)] : null,
    }
    const title = (this.props.formCMI !== null) ? "Modification d'une fiche" : "Créer une fiche";
    let footer = [];
    let btnsToAddInFooter = []
    
    if (this.props.formCMI) {
      if (showMepPage || fromMepDashboard) {
        if (this.props.formCMI.state === "validated") {
          btnsToAddInFooter.push("update");
          btnsToAddInFooter.push("invalidate");
        }
        else if (this.props.formCMI.state === "refused") {
          btnsToAddInFooter.push("validate");
        }
        else {
          btnsToAddInFooter.push("invalidate");
          btnsToAddInFooter.push("validate");
        }
      }
      else {
        btnsToAddInFooter.push("cancel");
        if(this.props.formCMI.state !== "validated") {
          btnsToAddInFooter.push("update");
        }
      }
    } else {
      btnsToAddInFooter.push("cancel")
      btnsToAddInFooter.push("submit")
    }
    
    btnsToAddInFooter.forEach(key => {
        let btnString;
        let btnFunction;
        let btnId;
        let type;
        switch(key) {
          case "update":
            btnString = "Modifier";
            btnId = "modify";
            btnFunction = onUpdate;
            type="dashed";
            break;
          case "validate":
            btnString = "Valider Fiche";
            btnId = "validate";
            btnFunction = onAccept;
            type="primary"
            break;
          case "invalidate":
            btnString = "Refuser Fiche";
            btnId = "invalidate";
            btnFunction = onRefuse;
            type="danger"
            break;
          case "cancel":
            btnString = "Annuler";
            btnId = "cancel";
            btnFunction = onCancel;
            type=""
            break;
          case "submit":
            btnString = "Confirmer";
            btnId = "submit";
            btnFunction = onSubmit;
            type="primary"
            break;
        }
        footer.push(
          <Button key={key} type={type} onClick={btnFunction} id={btnId}>
            {btnString}
          </Button>
        )
      }
    )


    return (
      <Modal
        visible={visible}
        title={title}
        onCancel={onCancel}
        width={'75%'}
        footer={footer}
      >
        <Form {...formItemLayout} onSubmit={this.handleSubmit}>
          <Form.Item label="Critère(s) concerné(s)">
            {getFieldDecorator('criterion', {
              initialValue: defaultValues.criteria,
              rules: [{ required: true, message: 'Selectionnez un critère.' }],
            })(
              <Checkbox.Group className='formItem' disabled={disableFields} onChange={this.onCriterionChange}>
                {criteria.map(c => (
                  <Checkbox className='formItem' key={c._id} value={c.tag}>{c.name}</Checkbox>
                ))}
              </Checkbox.Group>,
            )}
          </Form.Item>
          {(displayedSection.includes('stage')) &&
          <Form.Item label="Titre du stage" hasFeedback>
            {getFieldDecorator('job_title', {
              initialValue: defaultValues.job_title,
              rules: [{ required: true, message: 'Renseignez le titre de votre poste.' }],
            })(
              <Input className='formItem' disabled={disableFields} placeholder="Titre du stage" />,
            )}
          </Form.Item>
          }
          {(displayedSection.includes('mobilite') || displayedSection.includes('stage')) &&
          <>
            <Form.Item label="Date de début et de fin" hasFeedback>
              {getFieldDecorator('start_end_date', {
                initialValue: defaultValues.start_end_date,
                rules: [{ required: true, message: 'Renseignez une date de début et de fin' }],
              })(
                <RangePicker className='formItem' placeholder={['Date de début', 'Date de fin']} separator="au" disabled={disableFields} format={'DD/MM/YYYY'}/>
              )}
            </Form.Item>
            <Form.Item label="Établissement d'accueil" hasFeedback>
              {getFieldDecorator('company', {
                initialValue: defaultValues.company,
                rules: [{ required: true, message: 'Renseignez le nom de l\'établissement d\'accueil.' }],
              })(
                <Input className='formItem' disabled={disableFields} placeholder="Établissement d'accueil"/>
              )}
            </Form.Item>
            <Form.Item label="Adresse" hasFeedback>
              {getFieldDecorator('address', {
                initialValue: defaultValues.address,
                rules: [{ required: true, message: 'Renseignez l\'adresse.' }],
              })(
                <Input className='formItem' disabled={disableFields} placeholder="Adresse"/>
              )}
            </Form.Item>
            <Form.Item label="Complément d'adresse">
              {getFieldDecorator('address_c', {
                initialValue: defaultValues.address_c,
                rules: [{ required: false, message: 'Renseignez le complément d\'adresse.' }],
              })(
                <Input className='formItem' disabled={disableFields} placeholder="Complément d'adresse"/>
              )}
            </Form.Item>
            <Form.Item label="Code postal" hasFeedback>
              {getFieldDecorator('postal_code', {
                initialValue: defaultValues.postal_code,
                rules: [{ required: true, message: 'Renseignez le code postal.' }],
              })(
                <Input className='formItem' disabled={disableFields} placeholder="Code postal"/>
              )}
            </Form.Item>
            <Form.Item label="Ville" hasFeedback>
              {getFieldDecorator('city', {
                initialValue: defaultValues.city,
                rules: [{ required: true, message: 'Renseignez la ville.' }],
              })(
                <Input className='formItem' disabled={disableFields} placeholder="Ville"/>
              )}
            </Form.Item>
            <Form.Item label="Pays" hasFeedback>
              {getFieldDecorator('country', {
                initialValue: defaultValues.country,
                rules: [{ required: true, message: 'Renseignez le pays d\'accueil.' }],
              })(
                <Input className='formItem' disabled={disableFields} placeholder="Pays"/>
              )}
            </Form.Item>
          </>
          }
          {displayedSection.length === 0 ? (<p style={{textAlign: 'center', fontWeight: 'bold'}}>Veuillez selectionner un critère.</p>) : (
            <Form.Item label="Description" hasFeedback>
              {getFieldDecorator('description', {
                initialValue: defaultValues.description,
                rules: [{ required: true, message: 'Renseignez une description.' }],
              })(
                <TextArea
                  className='formItem'
                  disabled={disableFields}
                  placeholder="Description"
                  autoSize={{ minRows: 3, maxRows: 10 }} />
              )}
            </Form.Item>
          )}
          {displayedSection.length > 0 && (!this.props.formCMI || (!showMepPage && !disableFields)) &&
          <Form.Item label="Justificatif(s)">
            {getFieldDecorator('files', {
              rules: [{ required: !this.props.formCMI, message: 'Renseignez au moins un justificatifs.' }],
            })(
              <Dragger {...draggerProps}>
                <p className="ant-upload-drag-icon">
                  <Icon type="inbox" />
                </p>
                <p className="ant-upload-text">Cliquez ou glissez vos fichier ici</p>
                <p className="ant-upload-hint">
                  Formats acceptés : PDF / JPG / PNG <br/>
                  Taille maximum : 5Mo
                </p>
              </Dragger>
            )}
          </Form.Item>
          }
          <div>
            {this.props.formCMI && this.props.formCMI.files_links && this.props.formCMI.files_links.map(fileset => {
              return <Button type="primary" key={fileset.uniqueName}  onClick={() => downloadFile(fileset,this.props.formCMI)} style={{ marginLeft: 5 }}>
                <Icon type="download" />
                {fileset.name}
              </Button>
            })}
          </div>

        </Form>
      </Modal>
    );
  }
}

FormCMI.propTypes = {
  formCMI: PropTypes.object,
  criteria: PropTypes.array.isRequired,
  visible: PropTypes.bool.isRequired,
  displayedSection: PropTypes.array.isRequired,
  handleSelectedCriterion: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onRefuse: PropTypes.func,
  onAccept: PropTypes.func,
  onUpdate: PropTypes.func,
  disableFields: PropTypes.bool,
  showMepPage: PropTypes.bool,
}

FormCMI.defaultProps = {
  formCMI: null,
  criteria: [],
  visible: false,
  displayedSection: [],
  disableFields: false,
  showMepPage: false,
  handleSelectedCriterion: () => {console.log("default handleFormSubmit function")},
  onCancel: () => {console.log("default onCancel function")},
  onSubmit: () => {console.log("default onSubmit function")},
  onRefuse: () => {console.log("default onRefuse function")},
  onAccept: () => {console.log("default onAccept function")},
  onUpdate: () => {console.log("default onUpdate function")},
}

const WrappedForm = Form.create({ name: 'form_cmi' })(FormCMI);

export default WrappedForm
