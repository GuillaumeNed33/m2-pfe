import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { getAuthToken } from '../../utils/auth';
import CourseForm from './courseForm';
import { Table, Modal, Button, Divider, message, Icon } from 'antd';

class CoursesTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      courseToDelete: null,
      expandedRows: [],
      tableCol: [
        {
          title: 'Nom court',
          dataIndex: 'short_name',
          key: 'short_name',
          width: 300,
        },
        {
          title: 'Nom long',
          dataIndex: 'long_name',
          key: 'long_name',
          width: 300,
        },
        {
          title: 'Actions',
          dataIndex: 'actions',
          key: 'actions',
          render: (text, record) => (
            <span>
              <Button
                name="modifyCourseBtn"
                style={{ background: 'rgb(252, 207, 3)', color: 'white' }}
                onClick={(e) => {
                  e.stopPropagation();
                  const { expandedRows } = this.state;
                  if (expandedRows.indexOf(record._id) < 0) {
                    this.setState({
                      expandedRows: expandedRows.concat(record._id),
                    });
                  } else {
                    const indexCourse = expandedRows.indexOf(record._id);
                    expandedRows.splice(indexCourse, 1);
                    this.setState({
                      expandedRows,
                    });
                  }
                }}
              >
                <Icon type="edit"/>
              </Button>
              <Divider type="vertical" />
              <Button
                name="deleteCourseBtn"
                type="danger"
                onClick={(e) => {
                  e.stopPropagation();
                  this.setState({
                    showModal: true,
                    courseToDelete: record,
                  });
                }}
              >
                <Icon type="delete"/>
              </Button>
            </span>
          ),
        },
      ],
    }
    this.updateCourse = this.updateCourse.bind(this);
    this.deleteCourse = this.deleteCourse.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  updateCourse(course) {
    this.handleRowExpanded(course);
    this.props.onUpdate(course);
  }

  deleteCourse() {
    const { courseToDelete } = this.state;
    const token = getAuthToken();
    axios.delete(`${process.env.API_URL}/course/${courseToDelete._id}`,
      { headers : { "Authorization": token }
      })
      .then((res) => {
        this.props.onDelete(courseToDelete);
        message.success('Cursus supprimé avec succès !')
        this.setState({
          courseToDelete: null,
        });
        this.closeModal();
      })
      .catch((err) => {
        if (err.message) {
          message.error('Impossible de supprimer un cursus avec des étudiants inscrits.');
        } else {
          message.error('Erreur à la suppression du cursus.');
        }
      });
  }

  closeModal() {
    this.setState({
      showModal: false,
      courseToDelete: null,
    });
  }

  handleRowExpanded(record) {
    const { expandedRows } = this.state;
    if (expandedRows.indexOf(record._id) < 0) {
      this.setState({
        expandedRows: expandedRows.concat(record._id),
      });
    } else {
      const indexCourse = expandedRows.indexOf(record._id);
      expandedRows.splice(indexCourse, 1);
      this.setState({
        expandedRows,
      });
    }
  }

  render() {
    const { tableCol, showModal, expandedRows } = this.state;
    const { courses } = this.props;
    return (
      <>
        <Table
          rowKey="_id"
          dataSource={courses}
          columns={tableCol}
          expandedRowRender={(record) => (
            <CourseForm
              id={record._id}
              short_name={record.short_name}
              long_name={record.long_name}
              isModify={true}
              updateCourse={this.updateCourse}
              closeExpandedRow={this.handleRowExpanded}
            />
          )}
          expandRowByClick
          onExpand={(expanded, record) => this.handleRowExpanded(record)}
          expandedRowKeys={expandedRows}
        />
        <Modal
          title="Supression de Cursus"
          visible={showModal}
          onCancel={this.closeModal}
          footer={[
            <Button key="delete" type="primary" onClick={this.deleteCourse}>
              Supprimer
            </Button>,
            <Button key="close" type="danger" onClick={this.closeModal}>
              Annuler
            </Button>,
          ]}
        >
          <p>Êtes-vous sûr de supprimer ce cursus ?</p>
        </Modal>
      </>
    );
  }
}

CoursesTable.propTypes = {
  courses: PropTypes.array,
  onUpdate: PropTypes.func,
  onDelete: PropTypes.func,
};

CoursesTable.defaultProps = {
  courses: null,
  onUpdate: null,
  onDelete: null,
};


export default CoursesTable;
