import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { getAuthToken } from '../../utils/auth';
import { Form, Icon, Input, Button, message } from 'antd';

class CourseFormFields extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const token = getAuthToken();
        if (!this.props.isModify) {
          axios.post(process.env.API_URL + '/course', {
            short_name: values.short_name,
            long_name: values.long_name
          },
          { headers : { "Authorization": token}
          }).then((res) => {
            this.props.addCourse(res.data);
            this.props.form.resetFields();
            message.success('Cursus ajouté avec succès !')
          }).catch(err => {
            console.error(err);
            message.error('Erreur à l\'ajout du cursus.')
          });
        } else {
          axios.put(process.env.API_URL + '/course/' + this.props.id, {
            short_name: values.short_name,
            long_name: values.long_name
          },
            { headers : { "Authorization": token}
          }).then((res) => {
            this.props.updateCourse(res.data);
            this.props.form.resetFields();
            message.success('Cursus modifié avec succès !')
          }).catch(err => {
            console.error(err);
            message.error('Erreur à la modification du cursus.')
          });
        }
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const button = this.props.isModify ?
      (<Button id="modifyCourse" type="primary" htmlType="submit" icon='edit'>Modifier</Button>) :
      (<Button id="createCourse" type="primary" htmlType="submit" icon='plus-circle'>Créer</Button>);

    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Form.Item>
          {getFieldDecorator('short_name', {
            rules: [{ required: true, message: 'Nom court obligatoire' }],
            initialValue: this.props.short_name,
          })(
            <Input
              prefix={<Icon type="shrink" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Nom court (Acronyme)"
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('long_name', {
            rules: [{ required: true, message: 'Nom long obligatoire' }],
            initialValue: this.props.long_name,
          })(
            <Input
              id='long_name'
              prefix={<Icon type="arrows-alt" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Nom long"
            />,
          )}
        </Form.Item>
        <div className={"inline"}>
          {button}
          <Button type="danger" htmlType="reset"
                  onClick={ () => {
                    this.props.form.resetFields();
                    this.props.closeExpandedRow({
                      _id: this.props.id,
                      short_name: this.props.short_name,
                      long_name:this.props.long_name
                    });
                  }}>
            Annuler
          </Button>
        </div>
      </Form>
    );
  }
}

CourseFormFields.propTypes = {
  id: PropTypes.string,
  short_name: PropTypes.string,
  long_name: PropTypes.string,
  isModify: PropTypes.bool,
  addCourse: PropTypes.func,
  updateCourse: PropTypes.func,
  closeExpandedRow: PropTypes.func
};

CourseFormFields.defaultProps = {
  id: null,
  short_name: null,
  long_name: null,
  isModify: false,
  addCourse: null,
  updateCourse: null,
  closeExpandedRow: null,
};

const CourseForm = Form.create({ name: 'CourseForm' })(CourseFormFields);

export default CourseForm;
