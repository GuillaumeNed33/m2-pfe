/* eslint-disable react/destructuring-assignment */
import React from 'react';
import axios from 'axios';
import { getAuthToken } from '../../utils/auth';
import PropTypes from 'prop-types';
import { Form, Icon, Button, Upload, message, Modal } from 'antd';

class CsvImportUsersFormFields extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.showFormatModal = this.showFormatModal.bind(this);
    this.customRequest = this.customRequest.bind(this);
    this.state = {
      file: null,
      loading: false,
    };
  }
  
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((errs, values) => {
      if (!errs) {
        this.setState({loading: true})
        const formData = new FormData();
        formData.append('file', values.CSVfile.file.originFileObj);
        const token = getAuthToken();
        axios.post(`${process.env.API_URL}/user/csv`,
          formData, { headers: { 'Content-Type': 'multipart/form-data; boundary=---01100001011100000110100', "Authorization": token } })
          .then((res) => {
            this.props.form.resetFields();
            this.props.addMultipleUsers(res.data)
            this.setState({loading: false})
            message.success('Fichier importé avec succès !');
          })
          .catch(() => {
            message.error('Erreur : Une erreur s\'est produite pendant l\'import.');
            this.setState({loading: false})
          });
      }
    });
  }
  
  showFormatModal = () => {
    Modal.info({
      className: 'modal-import-format',
      title: 'Chaque colonne DOIT être séparée des autres par une virgule ( "," )',
      content: (
        <div>
          <h2>Import de MEP</h2>
          <hr/>
          <h4>Colonnes :</h4>
          <ul>
            <li><b>courses : </b>Nom court des cursus liés (séparés par des POINTS-VIRGULES) <br/><i>Facultatif pour les administrateurs (type="admin")</i></li>
            <li><b>first_name : </b>Prénom</li>
            <li><b>last_name : </b>Nom</li>
            <li><b>login : </b>Identifiant de connexion</li>
            <li><b>password : </b>Mot de passe</li>
            <li><b>type : </b>Type d'utilisateur ("admin" ou "mep")</li>
          </ul>
          <h4>Exemple :</h4>
          <pre>
            courses,first_name,last_name,login,password,type
            ISI;IMSAT,David,Auber,dauber,mdp,mep <br/>
            ...
          </pre>
          <br/>
          
          <h2>Import d'étudiants</h2>
          <hr/>
          <h4>Colonnes :</h4>
          <ul>
            <li><b>courses : </b>Nom court du cursus lié</li>
            <li><b>first_name : </b>Prénom</li>
            <li><b>last_name : </b>Nom</li>
            <li><b>login : </b>Identifiant de connexion</li>
            <li><b>password : </b>Mot de passe</li>
            <li><b>student_number : </b>Numéro étudiant</li>
            <li><b>study_year : </b>Année d'études (L1, L2, L3, M1 ou M2)</li>
            <li><b>type : </b>Type d'utilisateur &rArr; "student"</li>
          </ul>
          <h4>Exemple :</h4>
          <pre>
            courses,first_name,last_name,login,password,student_number,study_year,type
            ISI,Florian,Roulet,froulet001,mdp,21516918,M2,student <br/>
            ...
          </pre>
        </div>
      ),
    })
  };
  
  customRequest({ file, onSuccess }) {
    setTimeout(() => {
      this.setState({
        file,
      });
      onSuccess(null, file);
    });
  }
  
  render() {
    const { getFieldDecorator } = this.props.form;
    
    return (
      <>
        <div style={{width: '100%', marginTop: 15}}>
          <p className="ant-upload-hint" style={{textAlign: 'center'}}>
            <a onClick={this.showFormatModal} style={{textAlign: 'center'}}>
              Informations sur le format du fichier
            </a>
          </p>
        </div>
        <Form onSubmit={this.handleSubmit} className="csv-form">
          <Form.Item>
            {getFieldDecorator('CSVfile', {
              valuePropName: 'csvfile',
              rules: [
                { required: true, message: 'fichier requis.' },
              ],
            })(
              <Upload.Dragger
                name="file"
                accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                customRequest={this.customRequest}
                multiple={false}
              >
                <p className="ant-upload-drag-icon">
                  <Icon type="inbox" />
                </p>
                <p className="ant-upload-text">Cliquez ou faites glisser un fichier</p>
              </Upload.Dragger>
            )}
          </Form.Item>
          <Form.Item style={{textAlign: "center"}}>
            <Button id="importUsers" type="primary" htmlType="submit" loading={this.state.loading}>
              Importer
            </Button>
          </Form.Item>
        </Form>
      </>
    );
  }
}

CsvImportUsersFormFields.propTypes = {
  addMultipleUsers: PropTypes.func.isRequired
};

CsvImportUsersFormFields.defaultProps = {
  addMultipleUsers: () => console.error('Default addMultipleUsers called'),
};

const CsvImportUsersForm = Form.create({ name: 'CsvImportUsersForm' })(CsvImportUsersFormFields);

export default CsvImportUsersForm;
