/* eslint-disable no-underscore-dangle */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { getAuthToken, getAuthUser } from '../../utils/auth';
import CsvImportUsersForm from './csvImportUsersForm';
import { Select, Radio, Form, Input, InputNumber, Checkbox, Icon, Button, message } from 'antd';

const { Option } = Select;
const { TextArea } = Input;

class UserFormFields extends React.Component {
  constructor(props) {
    super(props);
    this.handleImportChange = this.handleImportChange.bind(this);
    this.handleUserTypeChange = this.handleUserTypeChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleHistoryChange = this.handleHistoryChange.bind(this);
    this.state = {
      userType: 'student',
      creationType: 'manual',
      showDescription: 'false',
    };
  }
  
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((errs, values) => {
      if (!errs) {
        const token = getAuthToken();
        let postUser = {
          type: values.type,
          first_name: values.first_name,
          last_name: values.last_name,
          login: values.login,
          password: values.password,
        };
        if (values.type === 'student') {
          postUser = {
            ...postUser,
            student_number: values.student_number,
            study_year: values.study_year,
            courses: [values.courses.toString()],
            history: [values.history],
            description: values.description,
          };
        } else if (values.type === 'mep') {
          postUser = {
            ...postUser,
            courses: values.courses,
          };
        } else if (values.type === 'admin') {
          postUser = {
            ...postUser,
            courses: this.props.courses.map((course) => course._id),
          };
        }
        axios.post(`${process.env.API_URL}/user`, postUser, { headers : { "Authorization": token } }).then((response) => {
          if (response.status === 201) {
            const user = response.data;
            this.props.addUser(user);
            this.props.form.resetFields();
            message.success('Utilisateur créé avec succès !');
          }
        }).catch(() => {
          message.error('Erreur à la création de l\'utilisateur.');
        });
      } else {
        message.error('Erreur : formulaire invalide.');
      }
    });
  }
  
  handleImportChange(e) {
    this.setState({
      creationType: e.target.value,
    });
  }
  
  handleUserTypeChange(value) {
    this.setState({
      userType: value,
    });
  }
  
  handleHistoryChange(value) {
    if (value > 1) {
      this.setState({
        showDescription: true,
      });
    } else if (value === 1) {
      this.setState({
        showDescription: false,
      });
    }
  }
  
  render() {
    const userString = getAuthUser();
    const user = userString ? JSON.parse(userString) : null;
    const { getFieldDecorator } = this.props.form;
    const { userType, creationType, showDescription } = this.state;
    
    const radioStyle = {
      display: 'block',
      height: '30px',
      lineHeight: '30px',
    };
    
    let AdaptiveForm;
    let DescriptionField = (<></>);
    
    if (showDescription === true) {
      DescriptionField = (
        <Form.Item label="Description">
          {getFieldDecorator('description')(
            <TextArea />,
          )}
        </Form.Item>
      );
    }

    let adminOption;
    if (user && user.type === 'admin') {
      adminOption = <Option value="admin">Administrateur</Option>;
    }
    
    const creationMethod =  <div style={{width: '100%'}}>
      <Radio.Group value={creationType} onChange={this.handleImportChange} buttonStyle="solid" style={{ width: '100%' }}>
        <Radio.Button value="manual" style={{width: '50%', textAlign: 'center'}}>Manuel</Radio.Button>
        <Radio.Button value="import" style={{width: '50%', textAlign: 'center'}}>Import CSV</Radio.Button>
      </Radio.Group>
    </div>
    
    switch (userType) {
      default:
      case 'student':
        AdaptiveForm = (
          <>
            <Form.Item label="Numéro Étudiant">
              {getFieldDecorator('student_number', {
                rules: [
                  { pattern: /^[0-9]+$/, message: 'Numéro étudiant invalide.' },
                  { required: true, message: 'Numéro étudiant obligatoire' },
                ],
              })(
                <Input
                  id="student_number"
                  prefix={<Icon type="number" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Numéro étudiant"
                  style={{ width: '100%' }}
                />,
              )}
            </Form.Item>
            <Form.Item label="Année d'étude">
              {getFieldDecorator('study_year', {
                initialValue: 0,
                rules: [{ required: true, message: 'Année d\'études obligatoire' }],
              })(
                <Select>
                  <Option value={0}>L1</Option>
                  <Option value={1}>L2</Option>
                  <Option value={2}>L3</Option>
                  <Option value={3}>M1</Option>
                  <Option value={4}>M2</Option>
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="Semestre d'intégration">
              {getFieldDecorator('history', {
                rules: [
                  { required: true, message: 'Semestre d\'intégration obligatoire' },
                  { type: 'number', message: 'Un nombre est requis.' },
                  { pattern: /(^[1-9]$)|(^10$)/, message: 'Numéro de semestre invalide.' },
                ],
                initialValue: 1,
              })(
                <InputNumber
                  id="history"
                  prefix={<Icon type="number" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  onChange={this.handleHistoryChange}
                  placeholder="Numéro du semestre d'intégration du cursus"
                  style={{ width: '100%' }}
                />,
              )}
            </Form.Item>
            {DescriptionField}
            <Form.Item label="Cursus">
              {getFieldDecorator('courses', {
                rules: [
                  { required: true, message: 'Un cours obligatoire' },
                ],
              })(
                <Radio.Group>
                  {
                    this.props.courses.map((course) => (
                      // eslint-disable-next-line no-underscore-dangle
                      <Radio key={course._id} style={radioStyle} value={course._id}>
                        {course.long_name}
                      </Radio>
                    ))
                  }
                </Radio.Group>,
              )}
            </Form.Item>
          </>
        );
        break;
      case 'mep':
        AdaptiveForm = (
          <>
            <Form.Item label="Cursus">
              {getFieldDecorator('courses')(
                <Checkbox.Group>
                  {
                    this.props.courses.map((course) => (
                      // eslint-disable-next-line no-underscore-dangle
                      <Checkbox key={course._id} style={radioStyle} value={course._id}>
                        {course.long_name}
                      </Checkbox>
                    ))
                  }
                </Checkbox.Group>,
              )}
            </Form.Item>
          </>
        );
        break;
      case 'admin':
        AdaptiveForm = (<></>);
        break;
    }
    
    if (creationType === 'manual') {
      return (
        <>
          {creationMethod}
          <Form onSubmit={this.handleSubmit} className="user-form">
            <Form.Item label="Type">
              {getFieldDecorator('type', {
                rules: [{ required: true, message: 'Type obligatoire' }],
                initialValue: 'student',
              })(
                <Select onChange={this.handleUserTypeChange} style={{ width: '100%' }}>
                  <Option value="student">Étudiant</Option>
                  <Option value="mep">Membre de l&apos;équipe pédagogique</Option>
                  {adminOption}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="Prénom">
              {getFieldDecorator('first_name', {
                rules: [{ required: true, message: 'Prénom obligatoire' }],
              })(
                <Input
                  prefix={<Icon type="form" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Prénom"
                />,
              )}
            </Form.Item>
            <Form.Item label="Nom">
              {getFieldDecorator('last_name', {
                rules: [{ required: true, message: 'Nom de famille obligatoire' }],
              })(
                <Input
                  id="last_name"
                  prefix={<Icon type="form" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Nom de famille"
                />,
              )}
            </Form.Item>
            <Form.Item label="Login">
              {getFieldDecorator('login', {
                rules: [{ required: true, message: 'Login obligatoire' }],
              })(
                <Input
                  id="login"
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Login"
                />,
              )}
            </Form.Item>
            <Form.Item label="Mot de passe">
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Mot de passe obligatoire' }],
              })(
                <Input.Password
                  id="password"
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Mot de passe"
                />,
              )}
            </Form.Item>
            {AdaptiveForm}
            <div className={"inline"}>
              <Button id="createUser" type="primary" htmlType="submit">
                Créer
              </Button>
              <Button
                type="danger"
                htmlType="reset"
                onClick={() => {
                  this.props.form.resetFields();
                }}
              >
                Annuler
              </Button>
            </div>
          </Form>
        </>
      );
    }
    
    return (
      <>
        {creationMethod}
        <CsvImportUsersForm addMultipleUsers={this.props.addMultipleUsers}/>
      </>
    );
  }
}

UserFormFields.propTypes = {
  courses: PropTypes.arrayOf(PropTypes.object).isRequired,
  form: PropTypes.instanceOf(Object).isRequired,
  addUser: PropTypes.func.isRequired,
  addMultipleUsers: PropTypes.func.isRequired,
};

UserFormFields.defaultProps = {
  courses: [],
  addUser: () => console.error('Default addUser called'),
  addMultipleUsers: () => console.error('Default addMultipleUsers called'),
};

const UserForm = Form.create({ name: 'UserForm' })(UserFormFields);

export default UserForm;
