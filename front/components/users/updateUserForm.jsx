/* eslint-disable no-underscore-dangle */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import axios from 'axios';
import { getAuthToken } from '../../utils/auth';
import PropTypes from 'prop-types';
import { Form, Icon, Input, Button, Select, Radio, Checkbox, message } from 'antd';
const { Option } = Select;

class UpdateUserFormFields extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUserTypeChange = this.handleUserTypeChange.bind(this);
    this.state = {
      userType: this.props.user.type,
    };
  }
  
  handleUserTypeChange(value) {
    this.setState({
      userType: value,
    });
  }
  
  handleSubmit(e) {
    e.preventDefault();
    const { user } = this.props;
    this.props.form.validateFields((errs, values) => {
      if (!errs) {
        const token = getAuthToken();
        Object.keys(user).forEach((key) => {
          if (Object.keys(values).includes(key)) {
            user[key] = values[key];
          }
        });
        if (!Array.isArray(user.courses)) {
          user.courses = [user.courses];
        }
        axios.put(`${process.env.API_URL}/user/${user._id}`, user, { headers : { "Authorization": token } }).then((response) => {
          if (response.status === 200) {
            this.props.updateUser(response.data);
            this.props.closeRow(user);
            message.success('Utilisateur modifié avec succès !');
          }
        }).catch(() => {
          message.error('Une erreur s\'est produite.');
        });
      }
    });
  }
  
  render() {
    const { getFieldDecorator } = this.props.form;
    const { userType } = this.state;
    const { user, courses } = this.props;
    let AdaptiveForm;
    switch (userType) {
      default:
      case 'student':
        AdaptiveForm = (
          <>
            <Form.Item>
              {getFieldDecorator('student_number', {
                initialValue: user.student_number,
                rules: [
                  { pattern: /^[0-9]+$/, message: 'Numéro étudiant invalide.' },
                  { required: true, message: 'Numéro étudiant obligatoire' },
                ],
              })(
                <Input
                  id="student_number"
                  prefix={<Icon type="number" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Numéro étudiant"
                  style={{ width: '100%' }}
                />,
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('study_year', {
                initialValue: user.study_year.toString() || '0',
              })(
                <Select>
                  <Option value="0">L1</Option>
                  <Option value="1">L2</Option>
                  <Option value="2">L3</Option>
                  <Option value="3">M1</Option>
                  <Option value="4">M2</Option>
                </Select>,
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('courses', {
                initialValue: user.courses[0],
              })(
                <Radio.Group>
                  {
                    courses.map((course) => (
                      // eslint-disable-next-line no-underscore-dangle
                      <Radio key={course._id} value={course._id}>
                        {course.long_name}
                      </Radio>
                    ))
                  }
                </Radio.Group>,
              )}
            </Form.Item>
          </>
        );
        break;
      case 'mep':
        AdaptiveForm = (
          <Form.Item>
            {getFieldDecorator('courses', {
              initialValue: user.courses,
            })(
              <Checkbox.Group>
                {
                  courses.map((course) => (
                    // eslint-disable-next-line no-underscore-dangle
                    <>
                    <Checkbox key={course._id} value={course._id}>
                      {course.long_name}
                    </Checkbox>
                    <br/>
                    </>
                  ))
                }
              </Checkbox.Group>,
            )}
          </Form.Item>
        );
        break;
      case 'admin':
        AdaptiveForm = (<></>);
    }
    
    return (
      <>
        <Form onSubmit={this.handleSubmit} className="update-form">
          <Form.Item>
            {getFieldDecorator('type', {
              rules: [{ required: true, message: 'Type obligatoire' }],
              initialValue: this.props.user.type,
            })(
              <Select
                onChange={this.handleUserTypeChange}
                style={{ width: '100%' }}
              >
                <Option value="student">Étudiant</Option>
                <Option value="mep">Membre de l&apos;équipe pédagogique</Option>
                <Option value="admin">Administrateur</Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('first_name', {
              rules: [{ required: true, message: 'Prénom obligatoire' }],
              initialValue: this.props.user.first_name,
            })(
              <Input
                prefix={<Icon type="form" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="Prénom"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('last_name', {
              rules: [{ required: true, message: 'Nom obligatoire' }],
              initialValue: this.props.user.last_name,
            })(
              <Input
                prefix={<Icon type="form" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="Nom"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('login', {
              rules: [{ required: true, message: 'Login obligatoire' }],
              initialValue: this.props.user.login,
            })(
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="Login"
              />,
            )}
          </Form.Item>
          {AdaptiveForm}
          <Form.Item style={{textAlign: "center"}}>
            <Button id="modifyUser" type="primary" htmlType="submit">
              Modifier
            </Button>
          </Form.Item>
        </Form>
      </>
    );
  }
}

UpdateUserFormFields.propTypes = {
  user: PropTypes.instanceOf(Object).isRequired,
  courses: PropTypes.arrayOf(PropTypes.object).isRequired,
  form: PropTypes.instanceOf(Object).isRequired,
  updateUser: PropTypes.instanceOf(Function).isRequired,
  closeRow: PropTypes.instanceOf(Function).isRequired,
};

UpdateUserFormFields.defaultProps = {
  user: {},
  courses: [],
  updateUser: () => console.error('Default updateUser called'),
  closeRow: () => console.error('Default closeRow called'),
};

const UpdateUserForm = Form.create({ name: 'UpdateUserForm' })(UpdateUserFormFields);

export default UpdateUserForm;
