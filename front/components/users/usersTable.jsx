/* eslint-disable no-underscore-dangle */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import PropTypes from 'prop-types';
import UpdateUserForm from './updateUserForm';
import { getAuthUser } from '../../utils/auth';
import { Select, Input, Table, Divider, Tag, Button, Modal, Icon } from 'antd';

const { Option } = Select;

class UsersTable extends React.Component {
  constructor(props) {
    super(props);
    this.closeDeletionModal = this.closeDeletionModal.bind(this);
    this.closeRow = this.closeRow.bind(this);
    this.handleTypeFilter = this.handleTypeFilter.bind(this);
    this.handleYearFilter = this.handleYearFilter.bind(this);
    this.handleResearch = this.handleResearch.bind(this);
    this.handleCmiFilter = this.handleCmiFilter.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.incrementStudyYear = this.incrementStudyYear.bind(this)
    this.deleteMultipleStudents = this.deleteMultipleStudents.bind(this)
    this.updateUser = this.props.updateUser;
    this.state = {
      user: null,
      userToDelete: null,
      showDeletionModal: false,
      usersDisplayed: this.props.users,
      typeFilter: [],
      searchFilter: '',
      cmiFilter: [],
      yearFilter: [],
      expandedKeys: [],
      selectedRowKeys: [],
      loadingAddYear: false,
      columns: [
        {
          title: 'Prénom',
          dataIndex: 'first_name',
          key: 'first_name',
          sorter: (a, b) => a.first_name.localeCompare(b.first_name),
        },
        {
          title: 'Nom',
          dataIndex: 'last_name',
          key: 'last_name',
          sorter: (a, b) => a.last_name.localeCompare(b.last_name),
        },
        {
          title: 'Type',
          dataIndex: 'type',
          key: 'type',
          sorter: (a, b) => a.type.localeCompare(b.type),
        }, {
          title: 'Cursus',
          dataIndex: 'courses',
          key: 'courses',
          render: (tags) => (
            <span>
              {this.props.courses.filter(
                (course) => tags.includes(course._id),
              ).map((course) => course.short_name).map((tag) => (
                <Tag color="blue" key={tag}>
                  {tag}
                </Tag>
              ))}
            </span>
          ),
        },
        {
          title: 'Actions',
          dataIndex: 'actions',
          key: 'actions',
          render: (text, record) => {
            if (this.state.user && this.state.user.type === 'mep' && (record.type === 'admin' || record.type === 'mep'))
              return (
                <span>
                  <Button
                    style={{ background: 'rgb(252, 207, 3)', color: 'white' }}
                    onClick={(e) => {
                      this.setState((prevState) => ({
                        expandedKeys: prevState.expandedKeys.includes(record._id)
                          ? prevState.expandedKeys.filter((key) => key !== record._id)
                          : prevState.expandedKeys.concat(record._id),
                      }));
                      e.stopPropagation();
                    }}
                  >
                    <Icon type="edit" />
                  </Button>
                </span>);
            return (
              <span>
                <Button
                  style={{ background: 'rgb(252, 207, 3)', color: 'white' }}
                  onClick={(e) => {
                    this.setState((prevState) => ({
                      expandedKeys: prevState.expandedKeys.includes(record._id)
                        ? prevState.expandedKeys.filter((key) => key !== record._id)
                        : prevState.expandedKeys.concat(record._id),
                    }));
                    e.stopPropagation();
                  }}
                >
                  <Icon type="edit" />
                </Button>

                <Divider type="vertical" />

                <Button
                  name="deleteUserBtn"
                  type="danger"
                  onClick={(e) => {
                    e.stopPropagation();
                    this.setState({
                      showDeletionModal: true,
                      userToDelete: record,
                    });
                  }}
                >
                  <Icon type="delete" />
                </Button>
              </span>
            );
          },
        },
      ],
    };
  }

  componentDidMount() {
    const userString = getAuthUser();
    const user = userString ? JSON.parse(userString) : null;
    this.setState({
      user,
    })
  }

  componentDidUpdate(prevProps) {
    if (this.props.users.length !== prevProps.users.length) {
      this.filterUsers();
    }
  }

  deleteUser() {
    this.props.deleteUser(this.state.userToDelete);
    this.closeDeletionModal();
  }

  handleTypeFilter(values) {
    this.setState({
      typeFilter: values,
    }, () => {
      this.filterUsers();
    });
  }

  handleCmiFilter(values) {
    this.setState({
      cmiFilter: values,
    }, () => {
      this.filterUsers();
    });
  }

  handleResearch(value) {
    this.setState({
      searchFilter: value,
    }, () => {
      this.filterUsers();
    });
  }

  handleYearFilter(value) {
    this.setState({
      yearFilter: value,
    }, () => {
      this.filterUsers();
    });
  }

  filterUsers() {
    const {
      typeFilter, searchFilter, cmiFilter, yearFilter
    } = this.state;

    let usersDisplayed = this.props.users;

    // type filter
    if (typeFilter.length > 0) {
      usersDisplayed = usersDisplayed.filter((user) => typeFilter.includes(user.type));
    }

    // research filter
    usersDisplayed = usersDisplayed.filter((user) => (user.last_name.toLowerCase()
      .includes(searchFilter.toLowerCase())
      || (user.first_name.toLowerCase().includes(searchFilter.toLowerCase()))));

    // cmi filter
    if (cmiFilter.length > 0) {
      usersDisplayed = usersDisplayed.filter((user) => (user.courses.some((course) => cmiFilter.includes(course))) || user.type === 'admin');
    }

    // year filter
    if (yearFilter.length > 0) {
      usersDisplayed = usersDisplayed.filter((user) => user.type === "student" && yearFilter.some(y => user.study_year === parseInt(y) - 1));
    }

    this.setState({
      usersDisplayed,
      selectedRowKeys: []
    });
  }

  closeDeletionModal() {
    this.setState({
      showDeletionModal: false,
      userToDelete: null,
    });
  }

  closeRow(user) {
    this.setState((prevState) => ({
      expandedKeys: prevState.expandedKeys.filter((row) => row !== user._id),
    }));
  }
  onSelectedRowChange = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  async incrementStudyYear() {
    this.props.incrementStudyYear(this.state.selectedRowKeys)
  }

  async deleteMultipleStudents() {
    this.props.deleteMultipleStudents(this.state.selectedRowKeys)
  }

  render() {
    const { columns, showDeletionModal, usersDisplayed, expandedKeys, selectedRowKeys } = this.state;
    const { courses } = this.props;
    const hasSelected = selectedRowKeys.length > 0;
    let rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectedRowChange,
      getCheckboxProps: record => ({
        disabled: record.type !== 'student'
      })
    };
    const studyYear = [...Array(5).keys()].map((key) => key += 1);
    const studyOptions = studyYear.map((year) => (
      <Option key={year} value={year.toString(16)}>
        {year <= 3 ? 'L' + year : 'M' + (year - 3)}
      </Option>
    ));

    return (
      <>
        <div className={"inline"} style={{ justifyContent: 'flex-start' }}>
          <Select
            onChange={this.handleTypeFilter}
            mode="tags"
            style={{ width: 200, margin: 5 }}
            placeholder="Type"
          >
            <Option value="admin">Admins</Option>
            <Option value="mep">MEPs</Option>
            <Option value="student">Étudiants</Option>
          </Select>
          <Select
            key={this.cmiFilter}
            onChange={this.handleCmiFilter}
            mode="tags"
            style={{ width: 200, margin: 5 }}
            placeholder="CMI"
          >
            {
              courses.map((course) => (
                <Option key={course._id} value={course._id.toString(16)}>
                  {course.short_name}
                </Option>
              ))
            }
          </Select>

          <Select
            style={{ width: 200, margin: 5 }}
            placeholder="Année d'étude"
            mode="tags"
            onChange={this.handleYearFilter}
          >
            {studyOptions}
          </Select>

          <Input.Search onSearch={this.handleResearch} placeholder="Recherche" style={{ width: 300, margin: 5 }} />
        </div>

        <div>
          <Button style={{ marginTop: "2%", marginBottom: "2%", marginRight: "2%" }} type="primary" onClick={this.incrementStudyYear} disabled={!hasSelected} >
            Selection : Incrémenter l'année d'études
          </Button>

          <Button style={{ marginTop: "2%", marginBottom: "2%" }} type="danger" onClick={this.deleteMultipleStudents} disabled={!hasSelected} >
            Selection : Supprimer les élèves
          </Button>
        </div>

        <Table
          rowKey="_id"
          rowSelection={rowSelection}
          dataSource={usersDisplayed}
          expandedRowRender={(record) => (
            <UpdateUserForm
              user={record}
              courses={courses}
              updateUser={this.updateUser}
              closeRow={this.closeRow}
            />
          )}
          columns={columns}
          expandRowByClick="true"
          expandIcon={() => (<></>)}
          expandedRowKeys={expandedKeys}
        />
        <Modal
          title="Supression d'un utilisateur"
          visible={showDeletionModal}
          onCancel={this.closeDeletionModal}
          footer={[
            <Button key="delete" type="primary" onClick={this.deleteUser}>
              Supprimer
            </Button>,
            <Button key="close" type="danger" onClick={this.closeDeletionModal}>
              Annuler
            </Button>,
          ]}
        >
          <p>Êtes-vous sûr de vouloir supprimer cet utilisateur ?</p>
        </Modal>
      </>
    );
  }
}

UsersTable.propTypes = {
  courses: PropTypes.arrayOf(PropTypes.object).isRequired,
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  deleteUser: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  incrementStudyYear: PropTypes.func.isRequired,
  deleteMultipleStudents: PropTypes.func.isRequired,
};

UsersTable.defaultProps = {
  courses: [],
  users: [],
  deleteUser: () => console.error('Default deleteUser called'),
  updateUser: () => console.error('Default updateUser called'),
  incrementStudyYear: () => console.error('Default incrementStudyYear called'),
  deleteMultipleStudents: () => console.error('Default deleteMultipleStudents called'),
};

export default UsersTable;
