import React from 'react';
import axios from 'axios';
import CKEditor from 'ckeditor4-react';
import { getAuthToken } from '../../utils/auth';
import { Button, message, Tabs } from 'antd';
const { TabPane } = Tabs;
import PropTypes from "prop-types";

class DocEditor extends React.Component {
  constructor(props) {
    super(props);
    let docSelected = null;
    let coursesSelected = null;
    if(props.courses.length > 0) {
      if (props.courses.findIndex(c => c._id === 'global') !== -1) {
        docSelected = props.docs.find(d => d.type === 'global');
        coursesSelected = props.courses.find(c => c._id === 'global');
      } else {
        coursesSelected = props.courses[0];
        docSelected = props.docs.find(d => d.type === 'cmi' && d.course === coursesSelected._id);
      }
    }
    this.state = {
      selectedDoc: docSelected ? {...docSelected} : null,
      selectedCourse: {...coursesSelected},
      data: docSelected ? docSelected.content : '',
    };
    
    this.onEditorChange = this.onEditorChange.bind(this);
    this.submitNewDoc = this.submitNewDoc.bind(this);
    this.submitUpdate = this.submitUpdate.bind(this);
    this.submitDocumentation = this.submitDocumentation.bind(this);
    this.changeDocumentation = this.changeDocumentation.bind(this);
  }
  
  onEditorChange(evt) {
    this.setState({
      data: evt.editor.getData(),
    });
  }
  
  submitNewDoc() {
    const { data, selectedCourse } = this.state;
    const token = getAuthToken();
    
    axios.post(`${process.env.API_URL}/documentation`, {
      type: selectedCourse._id === 'global' ? 'global' : 'cmi',
      content: data,
      course: selectedCourse._id === 'global' ? null : selectedCourse._id,
    }, { headers : { "Authorization": token } })
      .then((docRes) => {
        message.success('Documentation publiée.');
        this.props.addDocumentation(docRes.data);
        const courseId = docRes.data.type === 'global' ? 'global' : docRes.data.course;
        this.changeDocumentation(courseId)
      }).catch((e) => {
      console.error(e);
      message.error('Une erreur est survenue.');
    });
  }
  
  submitUpdate() {
    const { data, selectedDoc } = this.state;
    const token = getAuthToken();
    axios.put(`${process.env.API_URL}/documentation/${selectedDoc._id}`, {
      content: data,
    }, { headers : { "Authorization": token } })
      .then((docRes) => {
        message.success('Documentation mise à jour.');
        this.props.updateDocumentation(docRes.data);
      }).catch((error) => {
      console.error(error);
      message.error('Une erreur est survenue.');
    });
  }
  
  async submitDocumentation() {
    const { data, selectedDoc } = this.state;
    if (data) {
      if (selectedDoc) {
        await this.submitUpdate();
      } else {
        await this.submitNewDoc();
      }
    } else {
      message.warning('Veuillez entrer une documentation non vide');
    }
  }
  
  changeDocumentation(courseId) {
    const { docs, courses } = this.props;
    let docSelected = null;
    let coursesSelected = null;
    if (courseId === 'global') {
      docSelected = docs.find(d => d.type === 'global');
      coursesSelected = courses.find(c => c._id === 'global');
    } else {
      docSelected = docs.find(d => d.type === 'cmi' && d.course === courseId);
      coursesSelected = courses.find(c => c._id === courseId);
    }
    this.setState({
      selectedDoc: docSelected ? {...docSelected} : null,
      selectedCourse: {...coursesSelected},
      data: docSelected ? docSelected.content : ''
    });
  }
  
  render() {
    const { data } = this.state;
    const { courses } = this.props;
    if(courses.length > 0) {
      return (
        <>
          <Tabs onChange={this.changeDocumentation} type="card">
            {courses.map((course) => (
              <TabPane tab={course.long_name} key={course._id} />
            ))}
          </Tabs>
          <CKEditor data={data} onChange={this.onEditorChange} />
          <Button type="primary" style={{ float: 'right', marginTop: 10 }} onClick={this.submitDocumentation}>Publier</Button>
        </>
      );
    } else {
      return (<p>Aucun cursus disponible pour éditer une documentation.</p>)
    }
  }
}

DocEditor.propTypes = {
  addDocumentation: PropTypes.func,
  updateDocumentation: PropTypes.func,
  courses: PropTypes.array,
  docs: PropTypes.array,
};

DocEditor.defaultProps = {
  addDocumentation: () => { console.error('Default addDocumentation called') },
  updateDocumentation: () => { console.error('Default updateDocumentation called') },
  courses: [],
  docs: [],
};

export default DocEditor;
