import React from 'react'
import axios from 'axios';
import Router from 'next/router'
import { Form, Icon, Input, Button, Alert, Modal } from 'antd';
import { login } from '../utils/auth'
import PropTypes from "prop-types";

class LoginForm extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: false
    }
  }
  
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        await this.setState({
          loading: true,
          error: false
        })
        axios.post(`${process.env.API_URL}/login`, {
          username: values.username,
          password: values.password,
        }).then(async response => {
          this.setState({
            loading: false,
            error: false
          })
          let {user, token} = await response.data
          const redirectionURL = user.type === "student" ? "/etu-dashboard" : "/pedagogic-dashboard"
          user = JSON.stringify(user);
          await login({user, token})
          this.props.form.resetFields();
          this.props.onOk();
          await Router.push(redirectionURL);
        }).catch(error => {
          console.error(error);
          this.setState({
            loading: false,
            error: true
          })
        })
      }
    });
  };
  
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form" style={{paddingLeft: 50, paddingRight: 50}}>
        <Form.Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Veuillez entrer votre nom d\'utilisateur !' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Nom d'utilisateur"
              maxLength={50}
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Veuillez entrer votre mot de passe !' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Mot de passe"
              maxLength={50}
            />,
          )}
        </Form.Item>
        {this.state.error &&
        <Alert message="Mauvaise combinaison : Nom d'utilisateur/Mot de passe" type="error" showIcon />
        }
        <div className={"inline"}>
          <Button type="primary" htmlType="submit">
            Connexion
          </Button>
          <Button type="danger" htmlType="reset"
                  onClick={ e => {
                    this.props.form.resetFields();
                    this.props.onCancel();
                  }}>
            Annuler
          </Button>
        </div>
      </Form>
    );
  }
}

LoginForm.propTypes = {
  onOk: PropTypes.func,
  onCancel: PropTypes.func,
};

LoginForm.defaultProps = {
  onOk: () => { console.error('Default onOk called') },
  onCancel: () => { console.error('Default onCancel called') },
};

const WrappedLoginForm = Form.create({ name: 'login' })(LoginForm);

export default WrappedLoginForm;
