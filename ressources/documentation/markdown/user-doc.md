# Documentation utilisateur

Dans cette documentation vous trouverez des informations sur les différents types d'utilisateurs, les pages qui leur sont accessibles et les actions qu'ils peuvent effectuer une fois l'application déployée. Les données présentes lors des captures d'écran ne sont que des exemples.

Pour des informations relatives au déploiement de l'application référez-vous à la [documentation administrateur](admin-doc.md).

## Utilisateur non connecté

### Page d'acceuil

Si une personne non connectée arrive sur l'application web, elle est redirigée sur la page des documentations. L'utilisateur pourra ainsi consulter la documentation globale de l'application (expliquant son principe et fonctionnement) rédigée par un administrateur de l'application.

![documentation non connectée](../images/not-co-doc.png)

## Utilisateur Étudiant

Un étudiant est enregistré dans l'application par l'administrateur. Actuellement dans une base de données locale, à terme l'inscritpion et la connexion devra être gérée au travers du CAS de l'université de Bordeaux.

### Page d'accueil et Tableau de bord

En se connectant avec son nom d'utilisateur et son mot de passe, l'étudiant arrive sur son tableau de bord.

![tableau de bord étudiant](../images/student-dashboard.png)

Sur son tableau de bord l'étudiant peut consulter ses critères, savoir s'ils sont validés ou non et voir l'avancement concernant ces critères (semaines validées) pour les critères de Stage ou de Mobilité. Les critères en cours de validation (et avancés) sont en bleu, la coloration du cercle avance en fonction de l'avancée du critère. Un critère validé apparaît en vert. Un récapitulatif est disponible en haut à drotie en cliquant sur l'icone "?".

En cliquant sur le bouton "Créer une fiche", un pop-up s'ouvre permettant à l'étudiant de compléter des informations pour soumettre une fiche pour avancer la validation d'un critère. Les champs à compléter dépendent du critère sélectionné. Pour une mobilité ou un stage des informations concernant la destination sont demandées alors que pour le niveau d'anglais seulement une description et un document prouvant cette validation (TOEIC ou TOEFL) sont demandées. Au moins une pièce justificative est demandée pour une fiche.

![tableau de bord étudiant](../images/form-english.png)

![tableau de bord étudiant](../images/form-internship.png)

En validant sa soumission en cliquant sur "Confirmer", la fiche apparaît dans la section "Fiches en attente". Cette fiche pourra ensuite être validée ou refusée par un Membre de l'équipe pédagogique.

Sous le bouton de création de fiche, trois parties permettent de visualiser les fiches précédement créées. Pour chaque fiche, la date la plus à droite est la date de soumission de la fiche. La date en rouge est la date du refus (pour les fiches refusées) et celle en verte la date de validation (pour les fiches validées).
* Les "Fiches en attente de validation" sont les fiches créées par l'étudiant et en attente de révision par un membre de l'équipe pédagogique afin qu'il la valide ou la refuse (avec un motif écrit par ce dernier).
* Les "Fiches refusées" sont les fiches qui ont été revues par un membre de l'équipe pédagogique et qui a jugé non recevable la fiche pour la ou les critères demandés. La raison du refus est signalée en  passant la souris sur la fiche refusée.
* Les "Fiches validées" sont les fiches qui ont été acceptées par un membre de l'équipe pédagogique. De ce fait l'étudiant ne peut plus modifier ces fiches.

En cliquant sur une fiche un pop-up s'ouvre permettant de visualiser les informations complétées pour une fiche. C'est par ce moyen qu'une fiche refusée ou en attente de validation peut également être modifiée, avant revue par un membre de l'équipe pédagogique. Une fiche refusée qui est modifée repasse en "Attente de validation".

![fiche avec PJ](../images/form-justifs-student.png)

Lors de la modification d'une fiche, si une pièce jointe est ajoutée alors les anciennes seront écrasées. Pour ajouter une pièce-jointe et garder les anciennes il faut donc remettre les anciennes avec la nouvelle. Si aucune pièce-joitne n'est ajouté lors d'une modification, les anciennes sont gardées.

### Documentation

En cliquant sur l'onglet "Documentation" l'étudiant est redirigé sur la page des documentations. Il peut sur cette page consulter la documentation de l'application (cette documentation), la documentation globale (rédigée par un administrateur) et la documentation relative à son cursus (rédigée par les membre de l'équipe pédagogique de ce cursus).

![tableau de bord mep](../images/doc-student.png)

## Utilisateur Membre de l'équipe pédagogique

### Page d'accueil et Tableau de bord

En se connectant avec son nom d'utilisateur et son mot de passe, le membre de l'équipe pédagogique arrive sur son tableau de bord.

![tableau de bord mep](../images/mep-dashboard.png)

Le tableau de bord contient deux tableaux. L'un pour visualiser les étudiants et l'autre pour voir les fiches en attente de ces étudiants. En arrivant sur la page le tableau des étudiants et donc des fiches est filtré sur 1 cursus, ici IMSAT (c'est un exemple). En cliquant sur le selecteur de cursus, une liste déroulante apparaît permettant de changer le cursus selectionné, parmis les cursus où l'utilisateur est un membre de l'équipe pédagogique. Lorsque le cursus change, le tableau des étudiants et des fiches en attentes sont rechargés. Le fonctionnement est le même pour filtrer d'après le semestre des étudiants. Le tableau des étudiants affiche également l'avancement des critères pour les étudiants. Chaque critère est représenté par une bulle, en mettant la souris sur une bulle sur la ligne d'un étudiant le nom du critère est affiché. Un critère qui n'est pas validé est en gris, s'il est validé alors il apparaît en vert.

En cliquant sur l'une des fiches en attente d'un étudiant un pop-up s'ouvre avec le récapitulatif de la fiche.

![fiche avec PJ](../images/form-justifs-pedagogic.png)

Le membre de l'équipe pédagogique peut ensuite "Valider" la fiche ou la "Refuser", ce qui ouvre un nouveau pop-up demandant le motif du refus de la fiche. Ce motif sera affiché à l'étudiant sur son Tableau de bord.

### Tableau de bord d'un étudiant

En cliquant sur un étudiant (une ligne du tableau des étudiants) le membre de l'équipe pédagogique est redirigé sur le tableau de bord de l'étudiant.

![tableau de bord student mep](../images/student-dashboard-pedagogic.png)

Sur cette page, le membre de l'équipe pédagogique peut : 
* Valider un critère pour l'étudiant en cliquant sur "Valider" au dessus d'un des critères. 
* Créer une fiche pour un étudiant en y complétant les informations et le ou les justificatifs.
* Consulter les fiches refusées, en attente ou validées de l'étudiant, en cliquant dessus.
* Modifier et/ou Valider une fiche refusée.
* Modifier et/ou Valider une fiche en attente.
* Modifier et/ou Refuser une fiche en attente.
* Refuser une fiche validée (si elle a été validé par erreur par exemple).

### Gestion des utilisateurs

En cliquant sur l'onglet "Gestion des utilisateurs" l'utilisateur accède à cette page.

![gestion utilisateur](../images/user-management.png)

Sur la gauche de la page un formulaire permet de créer un utilisateur. L'utilisateur à créer est soit un étudiant soit un membre de l'équipe pédagogique. L'utilisateur créé ne peut être ajouté qu'à un cursus dont fait partie le membre de l'équipe pédagogique connecté.

Des étudiants peuvent être créés en grand nombre en cliquant sur "Import CSV" dans la formualire de création. Il faut ensuite glisser-déposer le fichier au format .csv dans la zone prévu à cet effet, ou cliquer sur cetet zone pour sélectionner le fichier à importer. Le format du fichier .csv (les colonnes à créer et remplir pour chaque étudiant) sont accessible en cliquant sur le lien "Informations sur le format du fichier" ouvrant un pop-up d'explication du format.

![gestion utilisateur](../images/import-csv.png)

![gestion utilisateur](../images/info-csv.png)

Sur la droite de la page un tableau récapitulatif des utilisateurs déjà créés, qui sont des le ou les cursus du membre de l'équipe pédagogique connecté. En cliquant sur "Modifier" il est possible de changer les informations d'un utilisateur. En cliquant sur "Supprimer" un pop-up s'ouvre demandant confirmation de suppression.

![gestion utilisateur modifier](../images/user-management-modify.png)

### Gestion de la documentation

En cliquant sur l'onglet "Gestion Documentation" l'utilisateur est redirigé sur cette page.

![gestion doc](../images/doc-management.png)

C'est ici que le membre de l'équipe pédagogique peut écrire la documentation de son ou ses cursus. Pour chacun de ses cursus un onglet est affiché, en cliquant dessus, l'utilisateur peut écrire la documentation de ce cursus grâce à un éditeur de texte WYSIWYG. Pour valider la documentation, il après l'avoir rédigée cliquer sur "Publier". Cette documentation est visualisable par les étudiants du cursus concerné.

### Documentation

Sur cette page le membre de l'équipe pédagogique peut consulter les documentations de son ou ses cursus afin d'en vérifier le contenu. Mais également avoir un rappel du fonctionnement des pages auxquels l'utilisateur a accès (cette documentation) et la documentation globale du portail CMI, rédigée par un administrateur.

![gestion doc](../images/doc-mep.png)

## Utilisateur Administrateur

### Pages similaires aux membres de l'équipe pédagogique

Un administrateur est membre de l'équipe pédagogique ayant accès à tous les cursus. De ce fait, il a accès aux mêmes pages (plus d'autres détaillées plus bas) avec l'accès à tous les cursus :
* Sur la page de gestion utilisateur il voit tous les utilisateurs et peut créer des administrateurs.
* Sur son tableau de bord il peut trier son tableau sur tous les cursus et donc accéder à tous les étudiants.
* Sur la page de gestion de documentation il peut rédiger en plus la documentation globale.

### Gestion des cursus

En cliquant sur "Gestion cursus" l'administrateur arrive sur cette page.

![gestion cursus](../images/course-management.png)

Depuis cette page, accessible qu'aux administrateurs, il est possible de créer un cursus en spécifiant son nom court (acronyme) et long. Il apparaît ensuite dans la liste des cursus et peut être modifier ou supprimer. L'administrateur pourra ensuite depuis la gestion utilisateur créer des étudiants / membre de l'quipe pédagogique pour ce cursus ou ajouter des membre de l'équipe pédagogique à ce cursus en modifiant les membres de l'équipe pédagogique, et leur ajoutant ce cursus.
