# Documentation administrateur

Dans cette documentation vous trouverez des informations sur les différentes technologies utilisées dans ce projet, les informations de déploiement et les diverses possibilités de modification, les routes exposées de base après déploiement et l'exécution des tests.

Pour des informations relatives à l'utilisation de l'application après déploiement référez-vous à la [documentation utilisateur](user-doc.md).

## Technologies

**Front :** [React][2] / [Next.js][3]  
**Backend :** [NodeJS][4] ([ExpressJS][5]) avec ORM [Mongoose][1]  
**Base de données :** [MongoDB][6]  

## Déploiement

### Avec Docker

Dans le dossier racine (cmitaker) exécutez la commande : 
    
    $ docker-compose up -d
    
Les ports du front, du back et de la base de données sont modifiables dans le fichier `.env`.
Par défaut, l'application est disponible à l'adresse [localhost:3000](localhost:3000), et le back-end est lancée sur [localhost:4000](localhost:4000).
    
### Sans Docker

Si jamais, vous modifiez la variable `PORT` dans le fichier `back/.env`, pensez à modifier le port en conséquence dans le fichier `front/.env`
On suppose que vous possédez sur votre machine un SGBD MongoDB actif.  

Ensuite, lancez le back end à l'adresse [localhost:4000](localhost:4000) :

    $ cd back
    $ npm install
    $ npm start 
    
Enfin, lancez le front end à l'adresse [localhost:3000](localhost:3000) :

    $ cd front
    $ npm install
    $ npm run build 
    $ npm start 

## Routes de l'API (back)

Une fois l'application déployée, l'API écoute les requêtes à [localhost:4000](localhost:4000). Les routes et les requêtes possibles sont détaillées dans le fichier [OpenAPI](../../../back/openapi.yaml).

## Exécution des tests

Pour exécuter les tests il faut tout d'abord lancer le back sur la base de données tests. Pour se faire, si vous n'utilisez pas docker exécutez les commandes : 

    $ cd back
    $ npm run test

Si vous utilisez docker, modifier le [Dockerfile du back](../../../back/Dockerfile) et remplacez la ligne 7 (ou commentez la avec '#') par `CMD ["npm", "run" ,"docker_test"]` .

Il faut ensuite peupler la base de données de Test avec les seeders créés dans le dossier /back. Il faut lancer les commandes : 

    $ cd back
    $ npm run seed


### Front

Les tests du front sont créés avec [Cypress][7]. Les tests sont soit exécutables en mode intéractif, possibilité de selectionner le fichier à lancer et voir les actions faites par automatisation avec les commandes :  

    $ cd front
    $ npm run cypress:open

Puis en selectionnant le fichier à exécuter.

Soit en lançant les tests sans le mode interactif : 

    $ cd front
    $ npm run cypress:run

Ce qui aura pour effet de lancer tous les tests du front et afficher dans la console le resultat de tous les tests.

### Back

Les tests du back sont créés avec [Chai][8] et [Mocha][9].

Pour exécuter tous les tests du back, il faut lancer la commande : 

    $ cd back
    $ npm run mocha

Ce qui aura pour effet de lancer tous les tests et d'afficher dans la console le résultat de ces derniers.

[1]: https://mongoosejs.com/
[2]: https://fr.reactjs.org/
[3]: https://nextjs.org/
[4]: https://nodejs.org/en/
[5]: https://expressjs.com/fr/
[6]: https://www.mongodb.com/fr
[7]: https://www.cypress.io/
[8]: https://www.chaijs.com/
[9]: https://mochajs.org/
