const mongoose = require('mongoose');

const { Schema } = mongoose;
const FileSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  uniqueName: {
    type: String,
    required: true,
  },
  pathFile: {
    type: String,
    required: true,
  },
});
const FormSchema = new Schema({
  student: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'student field is required.'],
  },
  state: {
    type: String,
    enum: ['toValidate', 'validated', 'refused'],
    required: [true, 'state field is required.'],
  },
  criteria: {
    type: [{ type: Schema.Types.ObjectId, ref: 'criterion' }],
    default: [],
    required: [true, 'criteria field is required.'],
  },
  submit_date: {
    type: Date,
    required: [true, 'submit_date field is required.'],
  },
  review_date: {
    type: Date,
    required: false,
  },
  description: {
    type: String,
    required: [true, 'description field is required.'],
  },
  files_links: {
    type: [FileSchema],
    default: [],
    required: false,
  },
  rejection_reason: {
    type: String,
    required: [
      () => this.state === 'refused',
      'rejection_reason field is required for "refused" type.',
    ],
  },
  company: {
    type: String,
    required: false,
  },
  address: {
    type: String,
    required: false,
  },
  address_c: {
    type: String,
    required: false,
  },
  postal_code: {
    type: String,
    required: false,
  },
  city: {
    type: String,
    required: false,
  },
  country: {
    type: String,
    required: false,
  },
  job_title: {
    type: String,
    required: false,
  },
  start_date: {
    type: Date,
    required: false,
  },
  end_date: {
    type: Date,
    required: false,
  },
});

const Form = mongoose.model('form', FormSchema);

module.exports = Form;
