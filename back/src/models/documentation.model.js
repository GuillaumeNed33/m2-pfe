const mongoose = require('mongoose');

const { Schema } = mongoose;

const DocumentationSchema = new Schema({
  type: {
    type: String,
    enum: ['global', 'cmi'],
    required: [true, 'type field is required.'],
  },
  content: {
    type: String,
    required: [true, 'content field is required.'],
  },
  course: {
    type: Schema.Types.ObjectId,
    ref: 'course',
    unique: true,
    required: [() => this.type === 'cmi', 'content field is required.'],
  },
});

const Documentation = mongoose.model('documentation', DocumentationSchema);

module.exports = Documentation;
