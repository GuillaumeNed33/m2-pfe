const mongoose = require('mongoose');

const { Schema } = mongoose;

const CriterionSchema = new Schema({
  name: {
    type: String,
    required: [true, 'name field is required.'],
  },
  description: {
    type: String,
  },
  tag: {
    type: String,
    unique: true,
    trim: true,
    lowercase: true,
    required: [true, 'tag field is required.'],
  },
  count_total_weeks: {
    type: Boolean,
    default: false,
    required: [true, 'count_total_weeks field is required.'],
  },
  minimum_weeks_to_validate: {
    type: Number,
    default: false,
    required: [() => this.count_total_weeks, 'minimum_weeks_to_validate field is required.'],
  },
});

const Criterion = mongoose.model('criterion', CriterionSchema);

module.exports = Criterion;
