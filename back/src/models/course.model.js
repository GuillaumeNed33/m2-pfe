const mongoose = require('mongoose');

const CourseSchema = new mongoose.Schema(
  {
    short_name: {
      type: String,
      required: [true, 'short_name field is required.'],
      maxlength: 30,
      unique: true,
    },
    long_name: {
      type: String,
      required: [true, 'long_name field is required.'],
      maxlength: 100,
    },
  },
);
const Course = mongoose.model('course', CourseSchema);


module.exports = Course;
