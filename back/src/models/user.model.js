const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const arrayUniquePlugin = require('mongoose-unique-array');

const { Schema } = mongoose;

function despcriptionRequirement() {
  return (this.type==='student' && !(this.history.includes(1)));
}

function studentTypeRequirement() {
  return this.type === 'student'
}

function courseMinSize(val) {
  if (this.type === 'student') {
    return val.length === 1;
  }
}

const UserSchema = new Schema({
  type: {
    type: String,
    enum: ['admin', 'mep', 'student'],
    required: [true, 'type field is required.'],
  },
  first_name: {
    type: String,
    required: [true, 'first_name field is required.'],
  },
  last_name: {
    type: String,
    required: [true, 'last_name field is required.'],
  },
  courses: {
    type: [{ type: Schema.Types.ObjectId, ref: 'course' }],
    validate: [courseMinSize, 'At least one course is needed'],
    default: [],
    required: [studentTypeRequirement, 'courses field is required for "student" type users.'],
  },
  history: {
    type: [Number],
    default: [1],
    required: [studentTypeRequirement, 'history field is required for "student" type users.'],
  },
  description: {
    type: String,
    required: [despcriptionRequirement, 'description field is required for "student" type users whose have not intregred the CMI at first semester.'],
  },
  student_number: {
    type: String,
    required: [studentTypeRequirement, 'student_number field is required for "student" type users.'],
  },
  forms_list: {
    type: [{ type: Schema.Types.ObjectId, ref: 'form' }],
    default: [],
    required: [studentTypeRequirement, 'forms_list field is required for "student" type users.'],
  },
  criteria: {
    type: [{ type: Schema.Types.ObjectId, ref: 'criterion' }],
    default: [],
    required: [studentTypeRequirement, 'criteria field is required for "student" type users.'],
  },
  study_year: {
    type: Number,
    required: [studentTypeRequirement, 'study_year field is required for "student" type users.'],
  },
  login: {
    type: String,
    lowercase: true,
    trim: true,
    unique: true,
    required: [true, 'login field is required'],
  },
  password: {
    type: String,
    minlength: 3,
    required: [true, 'password field is required'],
  },
});

UserSchema.plugin(arrayUniquePlugin);

// custom method to generate authToken
UserSchema.methods.generateAuthToken = function generateAuthToken() {
  return jwt.sign({ _id: this._id },
    process.env.JWT_PRIVATE, { expiresIn: process.env.EXPIRY_TIME });
};

const User = mongoose.model('user', UserSchema);

module.exports = User;
