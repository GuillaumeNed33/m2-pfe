const express = require('express');
const router = express.Router();
const courseController = require('../controllers/course.controller');
const {checkAuthUser, checkAuthAdmin} = require('../utils/auth')

router.get('/all', checkAuthUser, courseController.getAllCourses);
router.post('/', checkAuthAdmin, courseController.createCourse);
router.get('/:id', checkAuthUser, courseController.getCoursebyId);
router.put('/:id', checkAuthAdmin, courseController.updateCourse);
router.delete('/:id', checkAuthAdmin, courseController.deleteCourse);

module.exports = router;
