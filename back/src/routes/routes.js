const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');
const {checkAuthUser} = require('../utils/auth')

router.post('/login', userController.login);
router.post('/auth', checkAuthUser, userController.isLoggedIn);

module.exports = router;
