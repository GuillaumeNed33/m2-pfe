const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const router = express.Router();
const User = require('../models/user.model');
const formController = require('../controllers/form.controller');
const {checkAuthUser, checkAuthAdminMEP, checkAccessForm} = require('../utils/auth')

const storageOpt = multer.diskStorage({
  destination(req, files, cb) {
    User.findById(req.body.student).then((user) => {
      fs.mkdirSync(`./fiches/${user.student_number}`, { recursive: true });
      cb(null, `./fiches/${user.student_number}`);
    }).catch(cb);
  },
  filename(req, file, cb) {
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1E9)}`;
    file.uniqueName = uniqueSuffix;
    // eslint-disable-next-line no-param-reassign
    cb(null, `${uniqueSuffix}${path.extname(file.originalname)}`);
  },
});
const upload = multer({ storage: storageOpt });

router.post('/', upload.any(), checkAuthUser, formController.createForm);
router.get('/:id', checkAuthUser, formController.getFormById);
router.put('/:id', upload.any(), checkAuthUser, formController.updateForm);
router.delete('/:id', checkAuthUser, formController.deleteForm);
router.get('/course/:id', checkAuthAdminMEP, formController.getFormsByCourse);
router.get('/:idForm/:student/file/:idFile', checkAccessForm, formController.getFileFormById);
router.get('/student/:id', checkAuthUser, formController.getFormsByStudent);

module.exports = router;
