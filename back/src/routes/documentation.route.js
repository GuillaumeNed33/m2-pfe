const express = require('express');
const router = express.Router();
const documentationController = require('../controllers/documentation.controller');
const {addAuthToReq, checkAuthUser, checkAuthAdminMEP} = require('../utils/auth')

router.get('/all', addAuthToReq, documentationController.getAllDocumentations);
router.get('/type/:type', checkAuthUser, documentationController.getDocumentationsbyType);
router.get('/type/cmi/:cmi', checkAuthUser, documentationController.getDocumentationbyCourse);
router.post('/', checkAuthAdminMEP, documentationController.createDocumentation);
router.get('/:id', checkAuthUser, documentationController.getDocumentationbyId);
router.put('/:id', checkAuthAdminMEP, documentationController.updateDocumentation);
router.delete('/:id', checkAuthAdminMEP, documentationController.deleteDocumentation);

module.exports = router;
