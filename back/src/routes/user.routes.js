const express = require('express');
const multer = require('multer');
const fs = require('fs');
const router = express.Router();
const userController = require('../controllers/user.controller');
const { checkAuthAdminMEP, checkAuthUser } = require('../utils/auth')
const storageOpt = multer.diskStorage({
  destination(req, files, cb) {
    fs.mkdirSync('./csv/', { recursive: true });
    cb(null, './csv/');
  },
  filename(req, file, cb) {
    // eslint-disable-next-line no-param-reassign
    cb(null, file.originalname);
  },
});
const upload = multer({ storage: storageOpt });

router.get('/all', checkAuthAdminMEP, userController.getAllUsers);
router.get('/studyYear/:year', checkAuthAdminMEP, userController.getUsersByStudyYear);
router.get('/type/:type', checkAuthAdminMEP, userController.getUsersByType);
router.get('/course/:courseId', checkAuthAdminMEP, userController.getUsersByCourseId);
router.get('/type/:type/course/:courseId', checkAuthAdminMEP, userController.getUsersByTypeAndCourseId);
router.post('/', checkAuthAdminMEP, userController.createUser);
router.post('/csv', upload.any(), checkAuthAdminMEP, userController.createMultipleUsers);
router.put('/incrementStudyYear', checkAuthAdminMEP, userController.incrementStudyYear);
router.put('/deleteMultipleStudents', checkAuthAdminMEP, userController.deleteMultipleStudents);
router.get('/:id', checkAuthAdminMEP, userController.getUserById);
router.put('/:id', checkAuthAdminMEP, userController.updateUser);
router.delete('/:id', checkAuthAdminMEP, userController.deleteUserById);
router.get('/', checkAuthUser, userController.getUserByToken);

module.exports = router;
