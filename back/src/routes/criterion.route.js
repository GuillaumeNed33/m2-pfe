const express = require('express');
const router = express.Router();
const criterionController = require('../controllers/criterion.controller');
const {checkAuthUser} = require('../utils/auth')

router.get('/all', checkAuthUser, criterionController.getAllCriteria);

module.exports = router;
