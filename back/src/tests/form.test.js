var assert = require("assert");
var expect = require("chai").expect;
var axios = require("axios");

async function addUserToDb(token, username, type, studentNumber, courses, year) {
    return (
      await axios.post("http://localhost:4000/user", {
        type: type,
        first_name: username,
        last_name: username,
        courses: courses,
        student_number: studentNumber,
        forms_list: [],
        criteria: [],
        study_year: year,
        login: username,
        password: username
      }, { headers : { "Authorization": token } })
    ).data._id;
}

async function addCourseToDb(token, short_name, long_name) {
    return (
      await axios.post("http://localhost:4000/course", {
        short_name: short_name,
        long_name: long_name
      }, { headers : { "Authorization": token } })
    ).data._id;
}

async function addFormToDb(token, student, state, criteria, submit_date, description, company, address, address_c, postal_code, city, country, job_title, start_date, end_date, files_links) {
    return (
      await axios.post("http://localhost:4000/form", {
        student: student,
        state: state,
        criteria: criteria,
        submit_date: submit_date,
        description: description,
        company: company,
        address: address,
        address_c: address_c,
        postal_code: postal_code,
        city: city,
        country: country,
        job_title: job_title,
        start_date: start_date,
        end_date: end_date
      }, { headers : { "Authorization": token } })
    ).data._id;
}

async function addFormToDbWithoutSubmitDate(token, student, state, criteria, description) {
    return (
      await axios.post("http://localhost:4000/form", {
        student: student,
        state: state,
        criteria: criteria,
        description: description
      }, { headers : { "Authorization": token } })
    ).data._id;
}

async function addFormToDbWithoutCriteria(token, student, state, submit_date, description) {
    return (
      await axios.post("http://localhost:4000/form", {
        student: student,
        state: state,
        submit_date: submit_date,
        description: description
      }, { headers : { "Authorization": token } })
    ).data._id;
}

async function addFormToDbWithoutState(token, student, criteria, submit_date, description) {
    return (
      await axios.post("http://localhost:4000/form", {
        student: student,
        criteria: criteria,
        submit_date: submit_date,
        description: description
      }, { headers : { "Authorization": token } })
    ).data._id;
}

async function addFormToDbWithoutStudent(token, state, criteria, submit_date, description) {
    return (
      await axios.post("http://localhost:4000/form", {
        state: state,
        criteria: criteria,
        submit_date: submit_date,
        description: description
      }, { headers : { "Authorization": token } })
    ).data._id;
}

async function addRejectedFormToDb(token, student, state, criteria, submit_date, description, rejection_reason, company, address, address_c, postal_code, city, country, job_title, start_date, end_date, files_links) {
    return (
      await axios.post("http://localhost:4000/form", {
        student: student,
        state: state,
        criteria: criteria,
        submit_date: submit_date,
        description: description,
        rejection_reason: rejection_reason,
        company: company,
        address: address,
        address_c: address_c,
        postal_code: postal_code,
        city: city,
        country: country,
        job_title: job_title,
        start_date: start_date,
        end_date: end_date
      }, { headers : { "Authorization": token } })
    ).data._id;
}

async function updateFormToDb(token, id, student, state, criteria, submit_date, description, company, address, address_c, postal_code, city, country, job_title, start_date, end_date, files_links) {
    return (
      await axios.put("http://localhost:4000/form/"+id, {
        student: student,
        state: state,
        criteria: criteria,
        submit_date: submit_date,
        description: description,
        company: company,
        address: address,
        address_c: address_c,
        postal_code: postal_code,
        city: city,
        country: country,
        job_title: job_title,
        start_date: start_date,
        end_date: end_date
      }, { headers : { "Authorization": token } })
    ).data._id;
}

async function updateRejectedFormToDb(token, id, student, state, criteria, submit_date, description, rejection_reason, company, address, address_c, postal_code, city, country, job_title, start_date, end_date, files_links) {
    return (
      await axios.put("http://localhost:4000/form/"+id, {
        student: student,
        state: state,
        criteria: criteria,
        submit_date: submit_date,
        description: description,
        rejection_reason: rejection_reason,
        company: company,
        address: address,
        address_c: address_c,
        postal_code: postal_code,
        city: city,
        country: country,
        job_title: job_title,
        start_date: start_date,
        end_date: end_date
      }, { headers : { "Authorization": token } })
    ).data._id;
}

async function deleteUsersByIds(token, ids) {
    for (let i = 0; i < ids.length; i++) {
      await axios.delete("http://localhost:4000/user/" + ids[i], { headers : { "Authorization": token } });
    }
}

async function deleteCourseByIds(token, ids) {
    for (let i = 0; i < ids.length; i++) {
      await axios.delete("http://localhost:4000/course/" + ids[i], { headers : { "Authorization": token } });
    }
}

async function deleteFormsByIds(token, ids) {
    for (let i = 0; i < ids.length; i++) {
      await axios.delete("http://localhost:4000/form/" + ids[i], { headers : { "Authorization": token } });
    }
}

describe("Adding forms", function() {
    let userIdsToRemoveAtTheEnd = [];
    let formIdsToRemoveAtTheEnd = [];
    let userId = undefined;
    let token = undefined;
    let d = Date.now();

    before(async function() {
        token = (await axios.post("http://localhost:4000/login", {username: "bpinaud", password: "bpinaud"})).data.token;
        let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
        assert(allCourses.length >= 1, "Not enough courses to test that feature");

        userId = await addUserToDb(token, "formTest", "student", 52679, [allCourses[0]._id], 1);
        userIdsToRemoveAtTheEnd.push(userId);
    });
    
    after(async function() {
        await deleteFormsByIds(token, formIdsToRemoveAtTheEnd);
        await deleteUsersByIds(token, userIdsToRemoveAtTheEnd);
    });
    
    it("Add form", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");

        let formId;

        formIdsToRemoveAtTheEnd.push(
            formId = await addFormToDb(token, userId, 'toValidate', [allCriterion[0]._id], d, "description", "company", "address", "add", "33000", "city", "country", "job_title", d, d)
        );
      
        let afterInsert = (
            await axios.get("http://localhost:4000/form/"+formId, { headers : { "Authorization": token } })
        ).data;
      
        expect(afterInsert).to.not.be.equal('');
        expect(afterInsert.student).to.be.equal(userId);
        expect(afterInsert.state).to.be.equal('toValidate');
        expect(afterInsert.criteria[0]).to.be.equal(allCriterion[0]._id);
        expect(Date.parse(afterInsert.submit_date)).to.be.equal(d);
        expect(afterInsert.description).to.be.equal("description");
        expect(afterInsert.company).to.be.equal("company");
        expect(afterInsert.address).to.be.equal("address");
        expect(afterInsert.address_c).to.be.equal("add");
        expect(afterInsert.postal_code).to.be.equal("33000");
        expect(afterInsert.city).to.be.equal("city");
        expect(afterInsert.country).to.be.equal("country");
        expect(afterInsert.job_title).to.be.equal('job_title');
        expect(Date.parse(afterInsert.start_date)).to.be.equal(d);
        expect(Date.parse(afterInsert.end_date)).to.be.equal(d);
    });

    it("Add form without some unrequired fields", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");

        let formId;

        formIdsToRemoveAtTheEnd.push(
            formId = await addFormToDb(token, userId, 'validated', [allCriterion[0]._id], d, "description", "company", "address", "add", "33000", "city", "country")
        );
      
        let afterInsert = (
            await axios.get("http://localhost:4000/form/"+formId, { headers : { "Authorization": token } })
        ).data;

        expect(afterInsert).to.not.be.equal('');
        expect(afterInsert.student).to.be.equal(userId);
        expect(afterInsert.state).to.be.equal('validated');
        expect(afterInsert.criteria[0]).to.be.equal(allCriterion[0]._id);
        expect(Date.parse(afterInsert.submit_date)).to.be.equal(d);
        expect(afterInsert.description).to.be.equal("description");
        expect(afterInsert.company).to.be.equal("company");
        expect(afterInsert.address).to.be.equal("address");
        expect(afterInsert.address_c).to.be.equal("add");
        expect(afterInsert.postal_code).to.be.equal("33000");
        expect(afterInsert.city).to.be.equal("city");
        expect(afterInsert.country).to.be.equal("country");
        expect(afterInsert.job_title).to.be.equal(undefined);
        expect(afterInsert.start_date).to.be.equal(undefined);
        expect(afterInsert.end_date).to.be.equal(undefined);
    });

    it("Try to add a form without description", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");

        let numberOfFormsBeforeInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        try {
            formIdsToRemoveAtTheEnd.push(
                await addFormToDb(token, userId, 'validated', [allCriterion[0]._id], d)
            );
        } catch (e) {}
    
        let numberOfFormsAfterInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        expect(numberOfFormsAfterInsert).to.not.equal(
            numberOfFormsBeforeInsert + 1
        );
        assert.equal(
            numberOfFormsAfterInsert,
            numberOfFormsBeforeInsert,
            "The form should not be added without description"
        );
    });

    it("Try to add a form without submit_date", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");

        let numberOfFormsBeforeInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        try {
            formIdsToRemoveAtTheEnd.push(
                await addFormToDbWithoutSubmitDate(token, userId, 'toValidate', [allCriterion[0]._id], "description")
            );
        } catch (e) {}
    
        let numberOfFormsAfterInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        expect(numberOfFormsAfterInsert).to.not.equal(
            numberOfFormsBeforeInsert + 1
        );
        assert.equal(
            numberOfFormsAfterInsert,
            numberOfFormsBeforeInsert,
            "The form should not be added without submit_date"
        );
    });

    it("Try to add a form without criteria", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");

        let numberOfFormsBeforeInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        try {
            formIdsToRemoveAtTheEnd.push(
                await addFormToDbWithoutCriteria(token, userId, 'toValidate', d, "description")
            );
        } catch (e) {}
    
        let numberOfFormsAfterInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        assert.equal(
            numberOfFormsAfterInsert,
            numberOfFormsBeforeInsert + 1,
            "The form should be added with default value for criteria"
        );
    });

    it("Try to add a form without state", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");

        let numberOfFormsBeforeInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        try {
            formIdsToRemoveAtTheEnd.push(
                await addFormToDbWithoutState(token, userId, [allCriterion[0]._id], d, "description")
            );
        } catch (e) {}
    
        let numberOfFormsAfterInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        expect(numberOfFormsAfterInsert).to.not.equal(
            numberOfFormsBeforeInsert + 1
        );
        assert.equal(
            numberOfFormsAfterInsert,
            numberOfFormsBeforeInsert,
            "The form should not be added without state"
        );
    });

    it("Try to add a form without student", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");

        let numberOfFormsBeforeInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        try {
            formIdsToRemoveAtTheEnd.push(
                await addFormToDbWithoutStudent(token, 'toValidate', [allCriterion[0]._id], d, "description")
            );
        } catch (e) {}
    
        let numberOfFormsAfterInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        expect(numberOfFormsAfterInsert).to.not.equal(
            numberOfFormsBeforeInsert + 1
        );
        assert.equal(
            numberOfFormsAfterInsert,
            numberOfFormsBeforeInsert,
            "The form should not be added without student"
        );
    });

    it("Try to add a form with wrong student", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");
        let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
        assert(allCourses.length >= 1, "Not enough courses to test that feature");

        wrongUserId = await addUserToDb(token, "worngFormTest", "student", 52681, [allCourses[0]._id], 1);
        await deleteUsersByIds(token, [wrongUserId]);
        try {
            formIdsToRemoveAtTheEnd.push(
                await addFormToDb(token, wrongUserId, 'toValidate', [allCriterion[0]._id], d, "description", "company", "address", "add", "33000", "city", "country", "job_title", d, d)
            );
        } catch (e) {}
    
        let numberOfFormsAfterInsert = (
            await axios.get("http://localhost:4000/form/student/"+wrongUserId, { headers : { "Authorization": token } })
        ).data.length;
    
        expect(numberOfFormsAfterInsert).to.be.equal(0);
    });

    it("Add rejected form", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");

        let formId;

        formIdsToRemoveAtTheEnd.push(
            formId = await addRejectedFormToDb(token, userId, 'refused', [allCriterion[0]._id], d, "description", "not conform", "company", "address", "add", "33000", "city", "country", "job_title", d, d)
        );
      
        let afterInsert = (
            await axios.get("http://localhost:4000/form/"+formId, { headers : { "Authorization": token } })
        ).data;

        expect(afterInsert).to.not.be.equal('');
        expect(afterInsert.student).to.be.equal(userId);
        expect(afterInsert.state).to.be.equal('refused');
        expect(afterInsert.criteria[0]).to.be.equal(allCriterion[0]._id);
        expect(Date.parse(afterInsert.submit_date)).to.be.equal(d);
        expect(afterInsert.description).to.be.equal("description");
        expect(afterInsert.rejection_reason).to.be.equal("not conform");
        expect(afterInsert.company).to.be.equal("company");
        expect(afterInsert.address).to.be.equal("address");
        expect(afterInsert.address_c).to.be.equal("add");
        expect(afterInsert.postal_code).to.be.equal("33000");
        expect(afterInsert.city).to.be.equal("city");
        expect(afterInsert.country).to.be.equal("country");
        expect(afterInsert.job_title).to.be.equal('job_title');
        expect(Date.parse(afterInsert.start_date)).to.be.equal(d);
        expect(Date.parse(afterInsert.end_date)).to.be.equal(d);
    });

    it("Try to add a rejected form without rejection_reason", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");

        let numberOfFormsBeforeInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        try {
            formIdsToRemoveAtTheEnd.push(
                await addRejectedFormToDb(token, userId, 'refused', [allCriterion[0]._id], d, "description")
            );
        } catch (e) {}
    
        let numberOfFormsAfterInsert = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data.length;
    
        expect(numberOfFormsAfterInsert).to.not.equal(
            numberOfFormsBeforeInsert + 1
        );
        assert.equal(
            numberOfFormsAfterInsert,
            numberOfFormsBeforeInsert,
            "The rejected form should not be added without rejection_reason"
        );
    });
});

describe("Updating forms", function() {
    let userIdsToRemoveAtTheEnd = [];
    let formIdsToRemoveAtTheEnd = [];
    let userId = undefined;
    let formId = undefined;
    let token = undefined;

    before(async function() {
        token = (await axios.post("http://localhost:4000/login", {username: "bpinaud", password: "bpinaud"})).data.token;
        let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
        assert(allCourses.length >= 1, "Not enough courses to test that feature");
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 2, "Not enough criterions to test that feature");
        userId = await addUserToDb(token, "formTest", "student", 52679, [allCourses[0]._id], 1);
        formId = await addFormToDb(token, userId, 'toValidate', [allCriterion[1]._id], Date.now(), "description", "company", "address", "add", "33000", "city", "country", "job_title", Date.now(), Date.now())
        userIdsToRemoveAtTheEnd.push(userId);
        formIdsToRemoveAtTheEnd.push(formId);
    });
    
    after(async function() {
        await deleteFormsByIds(token, formIdsToRemoveAtTheEnd);
        await deleteUsersByIds(token, userIdsToRemoveAtTheEnd);
    });
    
    it("Update form", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 2, "Not enough criterions to test that feature");

            let d = Date.now();

            await updateFormToDb(token, formId, userId, 'toValidate', [allCriterion[0]._id], d, "desc", "co", "add", "addre", "34000", "City", "Country", "jtitle", d, d);

            let formAfterUpdate = (
                await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
            ).data
            
            expect(formAfterUpdate[0].student).to.be.equal(userId);
            expect(formAfterUpdate[0].state).to.be.equal('toValidate');
            expect(formAfterUpdate[0].criteria[0]).to.be.equal(allCriterion[0]._id);
            expect(Date.parse(formAfterUpdate[0].submit_date)).to.be.equal(d);
            expect(formAfterUpdate[0].description).to.be.equal("desc");
            expect(formAfterUpdate[0].company).to.be.equal("co");
            expect(formAfterUpdate[0].address).to.be.equal("add");
            expect(formAfterUpdate[0].address_c).to.be.equal("addre");
            expect(formAfterUpdate[0].postal_code).to.be.equal("34000");
            expect(formAfterUpdate[0].city).to.be.equal("City");
            expect(formAfterUpdate[0].country).to.be.equal("Country");
            expect(formAfterUpdate[0].job_title).to.be.equal('jtitle');
            expect(Date.parse(formAfterUpdate[0].start_date)).to.be.equal(d);
            expect(Date.parse(formAfterUpdate[0].end_date)).to.be.equal(d);
    });

    it("Update form without unrequired fields", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 2, "Not enough criterions to test that feature");

            let d = Date.now();

            let formBeforeUpdate = (
                await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
            ).data

            await updateFormToDb(token, formId, userId, 'validated', [allCriterion[1]._id], d, "description");

            let formAfterUpdate = (
                await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
            ).data
            
            expect(formAfterUpdate[0].student).to.be.equal(userId);
            expect(formAfterUpdate[0].state).to.be.equal('validated');
            expect(formAfterUpdate[0].criteria[0]).to.be.equal(allCriterion[1]._id);
            expect(Date.parse(formAfterUpdate[0].submit_date)).to.be.equal(d);
            expect(formAfterUpdate[0].description).to.be.equal("description");
            expect(formAfterUpdate[0].company).to.be.equal(formBeforeUpdate[0].company);
            expect(formAfterUpdate[0].address).to.be.equal(formBeforeUpdate[0].address);
            expect(formAfterUpdate[0].address_c).to.be.equal(formAfterUpdate[0].address_c);
            expect(formAfterUpdate[0].postal_code).to.be.equal(formAfterUpdate[0].postal_code);
            expect(formAfterUpdate[0].city).to.be.equal(formAfterUpdate[0].city);
            expect(formAfterUpdate[0].country).to.be.equal(formBeforeUpdate[0].country);
            expect(formAfterUpdate[0].job_title).to.be.equal(formBeforeUpdate[0].job_title);
            expect(formAfterUpdate[0].start_date).to.be.equal(formBeforeUpdate[0].start_date);
            expect(formAfterUpdate[0].end_date).to.be.equal(formBeforeUpdate[0].end_date);
    });

    it("Update form without some required fields", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 2, "Not enough criterions to test that feature");

            let d = Date.now();

            let formBeforeUpdate = (
                await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
            ).data

            await updateFormToDb(token, formId, userId, 'validated', [allCriterion[1]._id]);

            let formAfterUpdate = (
                await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
            ).data
            
            expect(formAfterUpdate[0].student).to.be.equal(userId);
            expect(formAfterUpdate[0].state).to.be.equal('validated');
            expect(formAfterUpdate[0].criteria[0]).to.be.equal(allCriterion[1]._id);
            expect(formAfterUpdate[0].submit_date).to.be.equal(formBeforeUpdate[0].submit_date);
            expect(formAfterUpdate[0].description).to.be.equal(formBeforeUpdate[0].description);
            expect(formAfterUpdate[0].company).to.be.equal(formBeforeUpdate[0].company);
            expect(formAfterUpdate[0].address).to.be.equal(formBeforeUpdate[0].address);
            expect(formAfterUpdate[0].address_c).to.be.equal(formAfterUpdate[0].address_c);
            expect(formAfterUpdate[0].postal_code).to.be.equal(formAfterUpdate[0].postal_code);
            expect(formAfterUpdate[0].city).to.be.equal(formAfterUpdate[0].city);
            expect(formAfterUpdate[0].country).to.be.equal(formBeforeUpdate[0].country);
            expect(formAfterUpdate[0].job_title).to.be.equal(formBeforeUpdate[0].job_title);
            expect(formAfterUpdate[0].start_date).to.be.equal(formBeforeUpdate[0].start_date);
            expect(formAfterUpdate[0].end_date).to.be.equal(formBeforeUpdate[0].end_date);
    });

    it("Update form without all riquired fields", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 2, "Not enough criterions to test that feature");

            let d = Date.now();

            let formBeforeUpdate = (
                await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
            ).data

            await updateFormToDb(token, formId);

            let formAfterUpdate = (
                await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
            ).data
            
            expect(formAfterUpdate[0].student).to.be.equal(formBeforeUpdate[0].student);
            expect(formAfterUpdate[0].state).to.be.equal(formBeforeUpdate[0].state);
            expect(formAfterUpdate[0].criteria[0]).to.be.equal(formBeforeUpdate[0].criteria[0]);
            expect(formAfterUpdate[0].submit_date).to.be.equal(formBeforeUpdate[0].submit_date);
            expect(formAfterUpdate[0].description).to.be.equal(formBeforeUpdate[0].description);
            expect(formAfterUpdate[0].company).to.be.equal(formBeforeUpdate[0].company);
            expect(formAfterUpdate[0].address).to.be.equal(formBeforeUpdate[0].address);
            expect(formAfterUpdate[0].address_c).to.be.equal(formAfterUpdate[0].address_c);
            expect(formAfterUpdate[0].postal_code).to.be.equal(formAfterUpdate[0].postal_code);
            expect(formAfterUpdate[0].city).to.be.equal(formAfterUpdate[0].city);
            expect(formAfterUpdate[0].country).to.be.equal(formBeforeUpdate[0].country);
            expect(formAfterUpdate[0].job_title).to.be.equal(formBeforeUpdate[0].job_title);
            expect(formAfterUpdate[0].start_date).to.be.equal(formBeforeUpdate[0].start_date);
            expect(formAfterUpdate[0].end_date).to.be.equal(formBeforeUpdate[0].end_date);
    });

    it("Update rejected form", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 2, "Not enough criterions to test that feature");

            let d = Date.now();

            let formBeforeUpdate = (
                await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
            ).data

            await updateRejectedFormToDb(token, formId, userId, 'refused', [allCriterion[0]._id], d, "desc", "not conform");

            let formAfterUpdate = (
                await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
            ).data
            
            expect(formAfterUpdate[0].student).to.be.equal(userId);
            expect(formAfterUpdate[0].state).to.be.equal('refused');
            expect(formAfterUpdate[0].criteria[0]).to.be.equal(allCriterion[0]._id);
            expect(Date.parse(formAfterUpdate[0].submit_date)).to.be.equal(d);
            expect(formAfterUpdate[0].description).to.be.equal("desc");
            expect(formAfterUpdate[0].rejection_reason).to.be.equal("not conform");
            expect(formAfterUpdate[0].company).to.be.equal(formBeforeUpdate[0].company);
            expect(formAfterUpdate[0].address).to.be.equal(formBeforeUpdate[0].address);
            expect(formAfterUpdate[0].address_c).to.be.equal(formAfterUpdate[0].address_c);
            expect(formAfterUpdate[0].postal_code).to.be.equal(formAfterUpdate[0].postal_code);
            expect(formAfterUpdate[0].city).to.be.equal(formAfterUpdate[0].city);
            expect(formAfterUpdate[0].country).to.be.equal(formBeforeUpdate[0].country);
            expect(formAfterUpdate[0].job_title).to.be.equal(formBeforeUpdate[0].job_title);
            expect(formAfterUpdate[0].start_date).to.be.equal(formBeforeUpdate[0].start_date);
            expect(formAfterUpdate[0].end_date).to.be.equal(formBeforeUpdate[0].end_date);
    });
});

describe("Deleting forms", function() {
    let userIdsToRemoveAtTheEnd = [];
    let userId = undefined;
    let userId1 = undefined;
    let token = undefined;

    before(async function() {
        token = (await axios.post("http://localhost:4000/login", {username: "bpinaud", password: "bpinaud"})).data.token;
        let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
        assert(allCourses.length >= 1, "Not enough courses to test that feature");
        userId = await addUserToDb(token, "formTest", "student", 52679, [allCourses[0]._id], 1);
        userId1 = await addUserToDb(token, "formTest1", "student", 52680, [allCourses[0]._id], 1);
        userIdsToRemoveAtTheEnd.push(userId);
    });
    
    after(async function() {
        await deleteUsersByIds(token, userIdsToRemoveAtTheEnd);
    });
    
    it("Delete forms", async () => {
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 2, "Not enough criterions to test that feature");

            let d = Date.now();
            let formIdsToRemove = [];

            let id1 = await addFormToDb(token, userId, 'toValidate', [allCriterion[0]._id], d, "description", "company", "address", "add", "33000", "city", "country", "job_title", d, d);
            let id2 = await addRejectedFormToDb(token, userId, 'validated', [allCriterion[1]._id], d, "description", "not conform");
            let id3 = await addFormToDb(token, userId1, 'toValidate', [allCriterion[0]._id], d, "desc", "co", "add");

            formIdsToRemove.push(id1);
            formIdsToRemove.push(id2);

            let nbFormBeforeDelete = 0

            if((await axios.get("http://localhost:4000/form/"+id1, { headers : { "Authorization": token } })).data != ''){
                ++nbFormBeforeDelete;
            }
            if((await axios.get("http://localhost:4000/form/"+id2, { headers : { "Authorization": token } })).data != ''){
                ++nbFormBeforeDelete;
            }
            if((await axios.get("http://localhost:4000/form/"+id3, { headers : { "Authorization": token } })).data != ''){
                ++nbFormBeforeDelete;
            }

            await deleteUsersByIds(token, [userId1]);
            await deleteFormsByIds(token, formIdsToRemove);

            let nbFormAfterDelete = nbFormBeforeDelete;

            if((await axios.get("http://localhost:4000/form/"+id1, { headers : { "Authorization": token } })).data === ''){
                --nbFormAfterDelete;
            }
            if((await axios.get("http://localhost:4000/form/"+id2, { headers : { "Authorization": token } })).data === ''){
                --nbFormAfterDelete;
            }
            if((await axios.get("http://localhost:4000/form/"+id3, { headers : { "Authorization": token } })).data === ''){
                --nbFormAfterDelete;
            }
            
            expect(nbFormBeforeDelete).to.be.equal(3);
            expect(nbFormAfterDelete).to.be.equal(0);
    });
});

describe("Getting forms", function() {
    let userIdsToRemoveAtTheEnd = [];
    let formIdsToRemoveAtTheEnd = [];
    let userId = undefined;
    let userId1 = undefined;
    let formId = undefined;
    let formId1 = undefined;
    let d = Date.now();
    let criteria = undefined;
    let course = undefined;
    let token = undefined;

    before(async function() {
        token = (await axios.post("http://localhost:4000/login", {username: "bpinaud", password: "bpinaud"})).data.token;
        let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
        assert(allCourses.length >= 1, "Not enough courses to test that feature");
        let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
        assert(allCriterion.length >= 1, "Not enough criterions to test that feature");
        criteria = allCriterion[0]._id;
        course = allCourses[0]._id
        userId = await addUserToDb(token, "formTest", "student", 52679, [course], 1);
        userId1 = await addUserToDb(token, "formTest1", "student", 52680, [course], 1);
        formId = await addFormToDb(token, userId, 'toValidate', [criteria], d, "description", "company", "address", "add", "33000", "city", "country", "job_title", d, d)
        formId1 = await addFormToDb(token, userId1, 'toValidate', [criteria], d, "desc", "co", "add", "addr", "34000", "City", "Country", "jtitle", d, d)
        userIdsToRemoveAtTheEnd.push(userId);
        userIdsToRemoveAtTheEnd.push(userId1);
        formIdsToRemoveAtTheEnd.push(formId);
        formIdsToRemoveAtTheEnd.push(formId1);
    });
    
    after(async function() {
        await deleteFormsByIds(token, formIdsToRemoveAtTheEnd);
        await deleteUsersByIds(token, userIdsToRemoveAtTheEnd);
    });
    
    it("Get form by id", async () => {

        let formGet = (
            await axios.get("http://localhost:4000/form/"+formId, { headers : { "Authorization": token } })
        ).data

        let formGet1 = (
            await axios.get("http://localhost:4000/form/"+formId1, { headers : { "Authorization": token } })
        ).data

        expect(formGet._id).to.be.equal(formId);
        expect(formGet.student).to.be.equal(userId);
        expect(formGet.state).to.be.equal('toValidate');
        expect(formGet.criteria[0]).to.be.equal(criteria);
        expect(Date.parse(formGet.submit_date)).to.be.equal(d);
        expect(formGet.description).to.be.equal("description");
        expect(formGet.company).to.be.equal("company");
        expect(formGet.address).to.be.equal("address");
        expect(formGet.address_c).to.be.equal("add");
        expect(formGet.postal_code).to.be.equal("33000");
        expect(formGet.city).to.be.equal("city");
        expect(formGet.country).to.be.equal("country");
        expect(formGet.job_title).to.be.equal('job_title');
        expect(Date.parse(formGet.start_date)).to.be.equal(d);
        expect(Date.parse(formGet.end_date)).to.be.equal(d);
        
        expect(formGet1._id).to.be.equal(formId1);
        expect(formGet1.student).to.be.equal(userId1);
        expect(formGet1.state).to.be.equal('toValidate');
        expect(formGet1.criteria[0]).to.be.equal(criteria);
        expect(Date.parse(formGet1.submit_date)).to.be.equal(d);
        expect(formGet1.description).to.be.equal("desc");
        expect(formGet1.company).to.be.equal("co");
        expect(formGet1.address).to.be.equal("add");
        expect(formGet1.address_c).to.be.equal("addr");
        expect(formGet1.postal_code).to.be.equal("34000");
        expect(formGet1.city).to.be.equal("City");
        expect(formGet1.country).to.be.equal("Country");
        expect(formGet1.job_title).to.be.equal('jtitle');
        expect(Date.parse(formGet1.start_date)).to.be.equal(d);
        expect(Date.parse(formGet1.end_date)).to.be.equal(d);
    });

    it("Try to get form with wrong id", async () => {

        wrongFormId = await addFormToDb(token, userId, 'toValidate', [criteria], d, d, "d", "c", "a", "country", "j", d, d)
        await deleteFormsByIds(token, [wrongFormId]);

        let formGet = (
            await axios.get("http://localhost:4000/form/"+wrongFormId, { headers : { "Authorization": token } })
        ).data

        expect(formGet).to.be.equal('');

    });

    it("Get form by student_id", async () => {

        let formGet = (
            await axios.get("http://localhost:4000/form/student/"+userId, { headers : { "Authorization": token } })
        ).data

        let formGet1 = (
            await axios.get("http://localhost:4000/form/student/"+userId1, { headers : { "Authorization": token } })
        ).data

        expect(formGet[0]._id).to.be.equal(formId);
        expect(formGet[0].student).to.be.equal(userId);
        expect(formGet[0].state).to.be.equal('toValidate');
        expect(formGet[0].criteria[0]).to.be.equal(criteria);
        expect(Date.parse(formGet[0].submit_date)).to.be.equal(d);
        expect(formGet[0].description).to.be.equal("description");
        expect(formGet[0].company).to.be.equal("company");
        expect(formGet[0].address).to.be.equal("address");
        expect(formGet[0].address_c).to.be.equal("add");
        expect(formGet[0].postal_code).to.be.equal("33000");
        expect(formGet[0].city).to.be.equal("city");
        expect(formGet[0].country).to.be.equal("country");
        expect(formGet[0].job_title).to.be.equal('job_title');
        expect(Date.parse(formGet[0].start_date)).to.be.equal(d);
        expect(Date.parse(formGet[0].end_date)).to.be.equal(d);
        
        expect(formGet1[0]._id).to.be.equal(formId1);
        expect(formGet1[0].student).to.be.equal(userId1);
        expect(formGet1[0].state).to.be.equal('toValidate');
        expect(formGet1[0].criteria[0]).to.be.equal(criteria);
        expect(Date.parse(formGet1[0].submit_date)).to.be.equal(d);
        expect(formGet1[0].description).to.be.equal("desc");
        expect(formGet1[0].company).to.be.equal("co");
        expect(formGet1[0].address).to.be.equal("add");
        expect(formGet1[0].address_c).to.be.equal("addr");
        expect(formGet1[0].postal_code).to.be.equal("34000");
        expect(formGet1[0].city).to.be.equal("City");
        expect(formGet1[0].country).to.be.equal("Country");
        expect(formGet1[0].job_title).to.be.equal('jtitle');
        expect(Date.parse(formGet1[0].start_date)).to.be.equal(d);
        expect(Date.parse(formGet1[0].end_date)).to.be.equal(d);
    });

     it("Get form by course_id", async () => {

        let formGet = (
            await axios.get("http://localhost:4000/form/course/"+course, { headers : { "Authorization": token } })
        ).data

        expect(formGet[0]._id).to.be.equal(formId);
        expect(formGet[0].student).to.be.equal(userId);
        expect(formGet[0].state).to.be.equal('toValidate');
        expect(formGet[0].criteria[0]).to.be.equal(criteria);
        expect(Date.parse(formGet[0].submit_date)).to.be.equal(d);
        expect(formGet[0].description).to.be.equal("description");
        expect(formGet[0].company).to.be.equal("company");
        expect(formGet[0].address).to.be.equal("address");
        expect(formGet[0].address_c).to.be.equal("add");
        expect(formGet[0].postal_code).to.be.equal("33000");
        expect(formGet[0].city).to.be.equal("city");
        expect(formGet[0].country).to.be.equal("country");
        expect(formGet[0].job_title).to.be.equal('job_title');
        expect(Date.parse(formGet[0].start_date)).to.be.equal(d);
        expect(Date.parse(formGet[0].end_date)).to.be.equal(d);
        
        expect(formGet[1]._id).to.be.equal(formId1);
        expect(formGet[1].student).to.be.equal(userId1);
        expect(formGet[1].state).to.be.equal('toValidate');
        expect(formGet[1].criteria[0]).to.be.equal(criteria);
        expect(Date.parse(formGet[1].submit_date)).to.be.equal(d);
        expect(formGet[1].description).to.be.equal("desc");
        expect(formGet[1].company).to.be.equal("co");
        expect(formGet[1].address).to.be.equal("add");
        expect(formGet[1].address_c).to.be.equal("addr");
        expect(formGet[1].postal_code).to.be.equal("34000");
        expect(formGet[1].city).to.be.equal("City");
        expect(formGet[1].country).to.be.equal("Country");
        expect(formGet[1].job_title).to.be.equal('jtitle');
        expect(Date.parse(formGet[1].start_date)).to.be.equal(d);
        expect(Date.parse(formGet[1].end_date)).to.be.equal(d);
    });

    it("Try to get form with wrong course_id", async () => {

        let wrongCourseId = await addCourseToDb(token, "CMI1", "Cursus Master IntÃÂ©grÃÂ© 1");
        await deleteCourseByIds(token, [wrongCourseId]);

        let formGet = (
            await axios.get("http://localhost:4000/form/course/"+wrongCourseId, { headers : { "Authorization": token } })
        ).data

        expect(formGet.length).to.be.equal(0);
    });
});