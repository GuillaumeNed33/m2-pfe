var assert = require("assert");
var expect = require("chai").expect;
var axios = require("axios");

async function addUserToDb(token, username, type, studentNumber, courses, year) {
  return (
    await axios.post("http://localhost:4000/user", {
      type: type,
      first_name: username,
      last_name: username,
      courses: courses,
      student_number: studentNumber,
      forms_list: [],
      criteria: [],
      study_year: year,
      login: username,
      password: username
    }, { headers : { "Authorization": token } })
  ).data._id;
}

async function addFormToDb(token, student, state, criteria, submit_date, validation_date, description, company, address, country, job_title, start_date, end_date, files_links) {
  return (
    await axios.post("http://localhost:4000/form", {
      student: student,
      state: state,
      criteria: criteria,
      submit_date: submit_date,
      validation_date: validation_date,
      description: description,
      company: company,
      address: address,
      country: country,
      country: job_title,
      start_date: start_date,
      end_date: end_date
    }, { headers : { "Authorization": token } })
  ).data;
}

async function addCourseToDb(token, long_name, short_name) {
  return (
    await axios.post("http://localhost:4000/course", {
      long_name: long_name,
      short_name: short_name
    }, { headers : { "Authorization": token } })
  ).data._id;
}

async function addUserToDbWithDescription(token, username, type, studentNumber, courses, year, history, description) {
  return (
    await axios.post("http://localhost:4000/user", {
      type: type,
      first_name: username,
      last_name: username,
      courses: courses,
      student_number: studentNumber,
      forms_list: [],
      criteria: [],
      history: history,
      description: description,
      study_year: year,
      login: username,
      password: username
    }, { headers : { "Authorization": token } })
  ).data._id;
}

async function addUserToDbWithoutDescription(token, username, type, studentNumber, courses, year, history) {
  return (
    await axios.post("http://localhost:4000/user", {
      type: type,
      first_name: username,
      last_name: username,
      courses: courses,
      student_number: studentNumber,
      forms_list: [],
      criteria: [],
      history: history,
      study_year: year,
      login: username,
      password: username
    }, { headers : { "Authorization": token } })
  ).data._id;
}

async function deleteUsersByIds(token, ids) {
  for (let i = 0; i < ids.length; i++) {
    await axios.delete("http://localhost:4000/user/" + ids[i], { headers : { "Authorization": token } });
  }
}

async function deleteCoursesByIds(token, ids) {
  for (let i = 0; i < ids.length; i++) {
    await axios.delete("http://localhost:4000/course/" + ids[i], { headers : { "Authorization": token } });
  }
}

describe("Adding users", function() {
  let idsToRemoveAtTheEnd = [];
  let coursesIdsToRemoveAtTheEnd = [];
  let token = undefined;

  before(async function() {
    token = (await axios.post("http://localhost:4000/login", {username: "bpinaud", password: "bpinaud"})).data.token;
    let id1 = await addCourseToDb(token, "Cursus1", "CMI1");
    let id2 = await addCourseToDb(token, "Cursus2", "CMI2");
    coursesIdsToRemoveAtTheEnd.push(id1);
    coursesIdsToRemoveAtTheEnd.push(id2);
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    let id3 = await addUserToDb(token, "test", "student", 11111, [allCourses[0]._id], 1);
    idsToRemoveAtTheEnd.push(id3);
  });

  after(async function() {
    await deleteUsersByIds(token,idsToRemoveAtTheEnd);
    await deleteCoursesByIds(token,coursesIdsToRemoveAtTheEnd);
  });

  it("Add user", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 1, "Not enough courses to test that feature");

    let userId;

    idsToRemoveAtTheEnd.push(
      userId = await addUserToDb(token,"addeduser", "student", 22222, [allCourses[0]._id], 1)
    );

    let afterInsert = (
      await axios.get("http://localhost:4000/user/"+userId, { headers : { "Authorization": token } })
    ).data;

    expect(afterInsert.login).to.be.equal("addeduser");
  });

  it("Add user in middle of year", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 1, "Not enough courses to test that feature");

    let userId;

    idsToRemoveAtTheEnd.push(
      userId = await addUserToDbWithDescription(token,"addeduserinsemester3", "student", 22223, [allCourses[0]._id], 1, [3], "description")
    );

    let afterInsert = (
      await axios.get("http://localhost:4000/user/"+userId, { headers : { "Authorization": token } })
    ).data;

    expect(afterInsert.login).to.be.equal("addeduserinsemester3")
  });

  it("Add user in middle of year without description", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 1, "Not enough courses to test that feature");

    let numberOfUsersBeforeInsert = (
      await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })
    ).data.length;

    try {
      let id = await addUserToDbWithoutDescription(token,"addedUserWithoutDescription", "student", 22225, [allCourses[0]._id], 1, [3])
      idsToRemoveAtTheEnd.push(id);
    } catch (e) {}

    let numberOfUsersAfterInsert = (
      await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })
    ).data.length;

    expect(numberOfUsersAfterInsert).to.not.equal(
      numberOfUsersBeforeInsert + 1
    );
    assert.equal(
      numberOfUsersAfterInsert,
      numberOfUsersBeforeInsert,
      "The user should not be added twice"
    );
  });


  it("Add already existant user", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 1, "Not enough courses to test that feature");

    let numberOfUsersBeforeInsert = (
      await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })
    ).data.length;

    try {
      let id = await addUserToDb(token,"test", "student", 11111, [allCourses[0]._id], 1);
      idsToRemoveAtTheEnd.push(id);
    } catch (e) {}

    let numberOfUsersAfterInsert = (
      await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })
    ).data.length;

    expect(numberOfUsersAfterInsert).to.not.equal(
      numberOfUsersBeforeInsert + 1
    );
    assert.equal(
      numberOfUsersAfterInsert,
      numberOfUsersBeforeInsert,
      "The user should not be added twice"
    );
  });

  it("Add mep to multiple course", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 2, "Not enough courses to test that feature");

    let mepId = await addUserToDb(token,"mepmember", "mep", 00000, [
      allCourses[0]._id,
      allCourses[1]._id
    ]);
    idsToRemoveAtTheEnd.push(mepId);

    let mepUser = (await axios.get("http://localhost:4000/user/" + mepId, { headers : { "Authorization": token } })).data;
    await axios.put("http://localhost:4000/user/" + mepId, {
      courses: []
    }, { headers : { "Authorization": token } });

    expect(mepUser.courses.length).to.be.equal(2);
  });

  it("Add mep to inexistant course", async () => {
    let userLenghtBefore = (await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } }))
      .data.length;
    try {
      let id = await addUserToDb(token,"mepmember", "mep", 88888, [000000000]);
      idsToRemoveAtTheEnd.push(id);
    } catch (e) {}
    let userLenghtAfter = (await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } }))
      .data.length;

    expect(userLenghtBefore).to.be.equal(userLenghtAfter);
  });
});

describe("Getting users", function() {
  let idsToRemoveAtTheEnd = [];
  let adminMepIdsToRemoveAtTheEnd = [];
  let token = undefined;

  before(async function() {
    token = (await axios.post("http://localhost:4000/login", {username: "bpinaud", password: "bpinaud"})).data.token;
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;

    idsToRemoveAtTheEnd.push(
      await addUserToDb(token,"studentmember1", "student", 11111, [allCourses[0]._id], 1)
    );
    idsToRemoveAtTheEnd.push(
      await addUserToDb(token,"studentmember2", "student", 22222, [allCourses[0]._id], 1)
    );
    idsToRemoveAtTheEnd.push(
      await addUserToDb(token,"studentmember3", "student", 33333, [allCourses[1]._id], 1)
    );
    
    let mep1 = await addUserToDb(token,"mepmember1", "mep", 44444, [allCourses[0]._id])
    idsToRemoveAtTheEnd.push(mep1);
    adminMepIdsToRemoveAtTheEnd.push(mep1);
    let mep2 = await addUserToDb(token,"mepmember2", "mep", 55555, [allCourses[1]._id])
    idsToRemoveAtTheEnd.push(mep2);
    adminMepIdsToRemoveAtTheEnd.push(mep2);
    let admin1 = await addUserToDb(token,"adminmember", "admin", 66666, [allCourses[1]._id])
    idsToRemoveAtTheEnd.push(admin1);
    adminMepIdsToRemoveAtTheEnd.push(admin1);
  });

  after(async function() {
    for (let i = 0; i < adminMepIdsToRemoveAtTheEnd.length; i++) {
      await axios.put("http://localhost:4000/user/" + adminMepIdsToRemoveAtTheEnd[i], {
        courses: []
      }, { headers : { "Authorization": token } });
    }
    await deleteUsersByIds(token,idsToRemoveAtTheEnd);

  });

  it("Get all users from db", async () => {
    let users = (await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })).data;
    expect(users).not.to.be.undefined;
  });

  it("Get user by id", async () => {
    let id = await addUserToDb(token,"toretreive", "mep", 77777, []);
    idsToRemoveAtTheEnd.push(id);
    adminMepIdsToRemoveAtTheEnd.push(id);
    let userToRetreive = (await axios.get("http://localhost:4000/user/" + id, { headers : { "Authorization": token } }))
      .data;
    expect(userToRetreive.login).to.equal("toretreive");
  });

  it("Get user by type", async () => {
    let students = (await axios.get("http://localhost:4000/user/type/student", { headers : { "Authorization": token } }))
      .data;
    let meps = (await axios.get("http://localhost:4000/user/type/mep", { headers : { "Authorization": token } })).data;
    let admins = (await axios.get("http://localhost:4000/user/type/admin", { headers : { "Authorization": token } }))
      .data;

    let studentsFound = [false, false];
    let mepsFound = [false, false];
    let adminFound = [false];

    for (let i = 0; i < students.length; i++) {
      switch (students[i].login) {
        case "studentmember1":
          studentsFound[0] = true;
          break;
        case "studentmember2":
          studentsFound[1] = true;
          break;
        default:
          break;
      }
    }

    for (let i = 0; i < meps.length; i++) {
      switch (meps[i].login) {
        case "mepmember1":
          mepsFound[0] = true;
          break;
        case "mepmember2":
          mepsFound[1] = true;
          break;
        default:
          break;
      }
    }

    for (let i = 0; i < admins.length; i++) {
      if (admins[i].login === "adminmember") {
        adminFound[0] = true;
      }
    }

    for (let i = 0; i < studentsFound.length; i++) {
      expect(studentsFound[i]).to.be.true;
    }

    for (let i = 0; i < mepsFound.length; i++) {
      expect(mepsFound[i]).to.be.true;
    }

    for (let i = 0; i < adminFound.length; i++) {
      expect(adminFound[i]).to.be.true;
    }
  });

  /* ************************************** Course *************************************** */
  it("Get user by type and courseId", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(
      allCourses.length >= 2,
      "Not enough courses in db to test the feature"
    );

    let studentsOfCourse0 = (
      await axios.get(
        "http://localhost:4000/user/type/student/course/" + allCourses[0]._id, { headers : { "Authorization": token } }
      )
    ).data;

    let mepsOfCourse1 = (
      await axios.get(
        "http://localhost:4000/user/type/mep/course/" + allCourses[1]._id, { headers : { "Authorization": token } }
      )
    ).data;

    let studentsOfCourse0Found = [false, false];
    let mepsOfCourse1Found = [false];

    for (let i = 0; i < studentsOfCourse0.length; i++) {
      switch (studentsOfCourse0[i].login) {
        case "studentmember1":
          studentsOfCourse0Found[0] = true;
          break;
        case "studentmember2":
          studentsOfCourse0Found[1] = true;
          break;
        default:
          break;
      }
    }

    for (let i = 0; i < mepsOfCourse1.length; i++) {
      switch (mepsOfCourse1[i].login) {
        case "mepmember2":
          mepsOfCourse1Found[0] = true;
          break;
        default:
          break;
      }
    }

    for (let i = 0; i < studentsOfCourse0Found.length; i++) {
      expect(studentsOfCourse0Found[i]).to.be.true;
    }

    for (let i = 0; i < mepsOfCourse1Found.length; i++) {
      expect(mepsOfCourse1Found[i]).to.be.true;
    }
  });

  it("Get user by courseId", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(
      allCourses.length >= 2,
      "Not enough courses in db to test the feature"
    );

    let firstCourseUsers = (
      await axios.get("http://localhost:4000/user/course/" + allCourses[0]._id, { headers : { "Authorization": token } })
    ).data;

    let secondCourseUsers = (
      await axios.get("http://localhost:4000/user/course/" + allCourses[1]._id, { headers : { "Authorization": token } })
    ).data;

    let firstCourseUsersFound = [false, false, false];
    let secondCourseUsersFound = [false, false];

    for (let i = 0; i < firstCourseUsers.length; i++) {
      switch (firstCourseUsers[i].login) {
        case "studentmember1":
          firstCourseUsersFound[0] = true;
          break;
        case "studentmember2":
          firstCourseUsersFound[1] = true;
          break;
        case "mepmember1":
          firstCourseUsersFound[2] = true;
          break;
        default:
          break;
      }
    }

    for (let i = 0; i < secondCourseUsers.length; i++) {
      switch (secondCourseUsers[i].login) {
        case "studentmember3":
          secondCourseUsersFound[0] = true;
          break;
        case "mepmember2":
          secondCourseUsersFound[1] = true;
          break;
        default:
          break;
      }
    }

    for (let i = 0; i < firstCourseUsersFound.length; i++) {
      expect(firstCourseUsersFound[i]).to.be.true;
    }

    for (let i = 0; i < secondCourseUsersFound.length; i++) {
      expect(secondCourseUsersFound[i]).to.be.true;
    }
  });

  it("Get users by study year", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 1, "Not enough courses to test that feature");

    idsToRemoveAtTheEnd.push(
      await addUserToDb(token,"firstYear1", "student", 99999, [allCourses[0]._id], 1)
    );
    idsToRemoveAtTheEnd.push(
      await addUserToDb(token,"firstYear2", "student", 101010, [allCourses[0]._id], 1)
    );
    idsToRemoveAtTheEnd.push(
      await addUserToDb(token,"secondYear1", "student", 111111, [allCourses[0]._id], 2)
    );
    idsToRemoveAtTheEnd.push(
      await addUserToDb(token,"thirdYear1", "student", 121212, [allCourses[0]._id], 3)
    );

    let firstYearFound = [false, false];
    let secondYearFound = [false];
    let thirdYearFound = [false];

    let firstYearStudents = (
      await axios.get("http://localhost:4000/user/studyYear/1", { headers : { "Authorization": token } })
    ).data;
    let secondYearStudents = (
      await axios.get("http://localhost:4000/user/studyYear/2", { headers : { "Authorization": token } })
    ).data;
    let thirdYearStudents = (
      await axios.get("http://localhost:4000/user/studyYear/3", { headers : { "Authorization": token } })
    ).data;

    for (let i = 0; i < firstYearStudents.length; i++) {
      switch (firstYearStudents[i].login) {
        case "firstyear1":
          firstYearFound[0] = true;
          break;
        case "firstyear2":
          firstYearFound[1] = true;
          break;
        default:
          break;
      }
    }

    for (let i = 0; i < secondYearStudents.length; i++) {
      switch (secondYearStudents[i].login) {
        case "secondyear1":
          secondYearFound[0] = true;
          break;
        default:
          break;
      }
    }

    for (let i = 0; i < thirdYearStudents.length; i++) {
      switch (thirdYearStudents[i].login) {
        case "thirdyear1":
          thirdYearFound[0] = true;
          break;
        default:
          break;
      }
    }

    for (let i = 0; i < firstYearFound.length; i++) {
      expect(firstYearFound[i]).to.be.true;
    }

    for (let i = 0; i < secondYearFound.length; i++) {
      expect(secondYearFound[i]).to.be.true;
    }

    for (let i = 0; i < thirdYearFound.length; i++) {
      expect(thirdYearFound[i]).to.be.true;
    }
  });
});

describe("Delete users", function() {
  let idsToRemoveAtTheEnd = [];
  let token = undefined;

  before(async function() {
    token = (await axios.post("http://localhost:4000/login", {username: "bpinaud", password: "bpinaud"})).data.token;
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 1, "Not enough courses to test that feature");
    let id1 = await addUserToDb(token,"test", "student", 123456, [allCourses[0]._id], 1);
    idsToRemoveAtTheEnd.push(id1);
  });

  after(async function() {
    await deleteUsersByIds(token,idsToRemoveAtTheEnd);
  });

  it("Delete user by id", async () => {
    let allCriterion = (await axios.get("http://localhost:4000/criterion/all", { headers : { "Authorization": token } })).data;
    assert(allCriterion.length >= 1, "Not enough criterions to test that feature");
   
    let numberOfUsersBeforeDelete = (
      await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })
    ).data.length;
    
    
    let form = await addFormToDb(token, idsToRemoveAtTheEnd[0], 'validated', [allCriterion[0]._id], Date.now(), Date.now(), "description");
    await axios.put("http://localhost:4000/user/"+idsToRemoveAtTheEnd[0], {forms_list: [form._id]}, { headers : { "Authorization": token } });
   
    let numberOfFormsBeforeDelete = (
      await axios.get("http://localhost:4000/form/student/"+idsToRemoveAtTheEnd[0], { headers : { "Authorization": token } })
    ).data.length;
   
    let nbIdsToDelete = idsToRemoveAtTheEnd.length;
    await deleteUsersByIds(token,idsToRemoveAtTheEnd);

    let numberOfUsersAfterDelete = (
      await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })
    ).data.length;

    let numberOfFormsAfterDelete = (
      await axios.get("http://localhost:4000/form/student/"+idsToRemoveAtTheEnd[0], { headers : { "Authorization": token } })
    ).data.length;

    idsToRemoveAtTheEnd = [];

    expect(numberOfFormsBeforeDelete).to.be.equal(1);
    expect(numberOfFormsAfterDelete).to.be.equal(0);

    assert.equal(
      numberOfUsersBeforeDelete - nbIdsToDelete,
      numberOfUsersAfterDelete,
      "The user or his forms was not deleted"
    );
  });

  it("Delete mep/admin who still is has course assigned", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 2, "Not enough courses to test that feature");

    let mepToDeleteId = await addUserToDb(token,"mepmember", "mep", 00000, [
      allCourses[0]._id,
      allCourses[1]._id
    ]);
    let adminToDeleteId = await addUserToDb(token,"adminmember", "admin", 11111, [
      allCourses[0]._id,
      allCourses[1]._id
    ]);

    idsToRemoveAtTheEnd.push(mepToDeleteId);
    idsToRemoveAtTheEnd.push(adminToDeleteId);

    let numberBeforeTryingToDelete = (
      await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })
    ).data.length;

    try {
      await axios.delete("http://localhost:4000/user/" + adminToDeleteId, { headers : { "Authorization": token } });
    } catch (e) {}

    try {
      await axios.delete("http://localhost:4000/user/" + mepToDeleteId, { headers : { "Authorization": token } });
    } catch (e) {}

    numberAfterTryingToDelete = (
      await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })
    ).data.length;

    await axios.put("http://localhost:4000/user/" + adminToDeleteId, {
      courses: []
    }, { headers : { "Authorization": token } });
    await axios.put("http://localhost:4000/user/" + mepToDeleteId, {
      courses: []
    }, { headers : { "Authorization": token } });

    expect(numberBeforeTryingToDelete).to.be.equal(numberAfterTryingToDelete);
  });
});

describe("Update users", function() {
  let idsToRemoveAtTheEnd = [];
  let token = undefined;

  before(async function() {
    token = (await axios.post("http://localhost:4000/login", {username: "bpinaud", password: "bpinaud"})).data.token;
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 1, "Not enough courses to test that feature");
    let id1 = await addUserToDb(token,"update", "student", 123456, [allCourses[1]._id], 1);
    idsToRemoveAtTheEnd.push(id1);
  });

  after(async function() {
    await deleteUsersByIds(token,idsToRemoveAtTheEnd);
  });

  it("Update user by id", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 1, "Not enough courses to test that feature");
    let idToUpdate = idsToRemoveAtTheEnd[0];
    await axios.put("http://localhost:4000/user/" + idToUpdate, {
      login: "newlogin",
      student_number: 654321,
      courses: [allCourses[0]._id]
    }, { headers : { "Authorization": token } });
    let updatedUser = (
      await axios.get("http://localhost:4000/user/" + idToUpdate, { headers : { "Authorization": token } })
    ).data;

    expect(updatedUser.login).to.equal("newlogin");
    expect(updatedUser.student_number).to.equal('654321');
    expect(updatedUser.courses).to.be.contain(allCourses[0]._id);
    expect(updatedUser.courses.length).to.equal(1);
  });

  it("Prevent a student from having multiple courses", async () => {
    let allCourses = (await axios.get("http://localhost:4000/course/all", { headers : { "Authorization": token } })).data;
    assert(allCourses.length >= 2, "Not enough courses to test that feature");

    let nbUsers = (await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })).data
      .length;

    try {
      let studentId = await addUserToDb(token,"update", "student", 54875, [
        [allCourses[0]._id],
        [allCourses[1]._id]
      ]);
      idsToRemoveAtTheEnd.push(studentId);
    } catch (e) {}

    let newNbUsers = (await axios.get("http://localhost:4000/user/all", { headers : { "Authorization": token } })).data
      .length;

    expect(nbUsers).to.equal(newNbUsers);

    let idToUpdate = idsToRemoveAtTheEnd[idsToRemoveAtTheEnd.length - 1];
    try {
      await axios.put("http://localhost:4000/user/" + idToUpdate, {
        courses: [allCourses[0]._id, allCourses[1]._id]
      }, { headers : { "Authorization": token } });
    } catch (e) {}

    let updatedUser = (
      await axios.get("http://localhost:4000/user/" + idToUpdate, { headers : { "Authorization": token } })
    ).data;

    expect(updatedUser.courses.length).to.equal(1);
  });
});
