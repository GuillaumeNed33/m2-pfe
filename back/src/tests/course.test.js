const Course = require('../models/course.model');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

// get env variables from .env file
dotenv.config({ path: './.env.test' });
const PORT = process.env.PORT || 4000;
const DB_URL = process.env.DB_URL;
const SERVER = "http://localhost";


// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const chaiAsPromised = require('chai-as-promised');


chai.use(chaiHttp).use(chaiAsPromised);
const should = chai.should();
let idCourse = '';

describe('Test Course ', () => {
  before(async () => { // Before each test we empty the database
    Course.remove({}, () => {
    });
  });
  console.log(`${SERVER}:${PORT}`);
  console.log(DB_URL);

  // Test GET Courses /course/all
  describe('getAllCourse', () => {
    it('should GET all  the courses', (done) => {
      chai.request(`${SERVER}:${PORT}`)
        .get('/course/all')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe('create new Course Post /course', () => {
    it('it should not POST a course without short_name  field', (done) => {
      const course = {
        long_name: 'The Lord of the Rings',
      };
      chai.request(`${SERVER}:${PORT}`)
        .post('/course')
        .send(course)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('short_name');
          res.body.errors.short_name.should.have.property('kind').eql('required');
          done();
        });
    });

    it('it should create a course ', (done) => {
      const course = {
        short_name: 'CMI1',
        long_name: 'Long title CMI 1',
      };
      chai.request(`${SERVER}:${PORT}`)
        .post('/course')
        .send(course)
        .end((err, res) => {
          idCourse = res.body._id;
          res.should.have.status(201);
          res.body.should.be.a('object');
          res.body.should.have.property('short_name');
          res.body.should.have.property('long_name');
          res.body.short_name.should.eql('CMI1');
          done();
        });
    });

    it('it should not create a course because short_name already exists ', (done) => {
      const course = {
        short_name: 'CMI1',
        long_name: 'Long title CMI 1',
      };
      chai.request(`${SERVER}:${PORT}`)
        .post('/course')
        .send(course)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be.a('object');
          res.body.should.have.property('keyPattern');
          res.body.code.should.eql(11000);
          res.body.keyPattern.short_name.should.eql(1);
          done();
        });
    });
  });

  describe(' get one Course /course/:id ', () => {
    it('it should GET a course by the given id', (done) => {
      chai.request(`${SERVER}:${PORT}`)
        .get(`/course/${idCourse}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('short_name');
          res.body.should.have.property('long_name');
          res.body.should.have.property('_id').eql(idCourse);
          done();
        });
    });

    it('it should GET a course by given id that does not exist', (done) => {
      chai.request(`${SERVER}:${PORT}`)
        .get(`/course/${mongoose.Types.ObjectId()}`)
        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.be.a('object');
          done();
        });
    });
  });

  describe('update course /course/:id ', () => {
    it('it should UPDATE a course given the id', (done) => {
      chai.request(`${SERVER}:${PORT}`)
        .put(`/course/${idCourse}`)
        .send({
          short_name: 'CMI2',
          long_name: 'Long title CMI 1',
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('short_name');
          res.body.should.have.property('long_name');
          res.body.short_name.should.eql('CMI2');
          done();
        });
    });

    it('it should UPDATE a course given id that does not exist', (done) => {
      chai.request(`${SERVER}:${PORT}`)
        .put(`/course/${mongoose.Types.ObjectId()}`)
        .send({
          short_name: 'CMI2',
          long_name: 'Long title CMI 1',
        })
        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.be.a('object');
          done();
        });
    });
  });

  describe('update course /course/:id ', () => {
    it('it should UPDATE a course given the id', (done) => {
      chai.request(`${SERVER}:${PORT}`)
        .put(`/course/${idCourse}`)
        .send({
          short_name: 'CMI2',
          long_name: 'Long title CMI 1',
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('short_name');
          res.body.should.have.property('long_name');
          res.body.short_name.should.eql('CMI2');
          done();
        });
    });

    it('it should not UPDATE a course given id that does not exist', (done) => {
      chai.request(`${SERVER}:${PORT}`)
        .put(`/course/${mongoose.Types.ObjectId()}`)
        .send({
          short_name: 'CMI2',
          long_name: 'Long title CMI 1',
        })
        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.be.a('object');
          done();
        });
    });
  });

  describe(' delete a course /course/:id ', () => {
    it('it should DELETE a course given the id', (done) => {
      chai.request(`${SERVER}:${PORT}`)
        .delete(`/course/${idCourse}`)
        .end((err, res) => {
          res.should.have.status(202);
          res.body.should.be.a('object');
          res.body.should.have.property('ok').eql(1);
          res.body.should.have.property('n').eql(1);
          done();
        });
    });

    it('it should not DELETE a course given the id that does not exist', (done) => {
      chai.request(`${SERVER}:${PORT}`)
        .delete(`/course/${idCourse}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });
});
