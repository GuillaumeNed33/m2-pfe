const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const Form = require('../models/form.model');

module.exports = {
  addAuthToReq: (req, res, next) => {
    // get the token from the header if present
    const token = req.headers['x-access-token'] || req.headers.authorization;
    // if no token found, return response (without going to the next middelware)
    if (token) {
      try {
        req.user = jwt.verify(token, process.env.JWT_PRIVATE);
      } catch (ex) {
      }
    }
    next();
  },
  
  checkAuthUser: (req, res, next) => {
    // get the token from the header if present
    const token = req.headers['x-access-token'] || req.headers.authorization;
    // if no token found, return response (without going to the next middelware)
    if (token) {
      try {
        // if can verify the token, set req.user and pass to next middleware
        req.user = jwt.verify(token, process.env.JWT_PRIVATE);
        next();
      } catch (ex) {
        // invalid token
        console.error(ex);
        res.status(401).send('Invalid token.');
      }
    } else {
      return res.status(401).send('Access denied. No token provided.');
    }
  },
  
  checkAccessForm: async (req, res, next) => {
    // get the token from the header if present
    const token = req.headers['x-access-token'] || req.headers.authorization;
    // if no token found, return response (without going to the next middelware)
    if (token) {
      try {
        // if can verify the token, set req.user and pass to next middleware
        req.user = jwt.verify(token, process.env.JWT_PRIVATE);
        const user = await User.findById(req.user._id);
        
        if (user.type === 'student' && !user._id.equals(req.params.student)) {
          return res.status(401).send('Access denied. ');
        }
        if (user.type === 'mep') {
          const student = await User.findOne({ _id: req.params.student });
          if (!user.courses.includes(student.courses[0])) {
            return res.status(401).send('Access denied. ');
          }
        }
        next();
      } catch (ex) {
        // invalid token
        console.error(ex);
        res.status(401).send('Invalid token.');
      }
    } else {
      return res.status(401).send('Access denied. No token provided.');
    }
  },
  
  checkAuthAdminMEP: async (req, res, next) => {
    // get the token from the header if present
    const token = req.headers['x-access-token'] || req.headers.authorization;
    // if no token found, return response (without going to the next middelware)
    if (token) {
      try {
        // if can verify the token, set req.user and pass to next middleware
        req.user = jwt.verify(token, process.env.JWT_PRIVATE);
        const user = await User.findById(req.user._id);
        // verify if it's an admin or mep
        if (user.type === 'admin' || user.type === 'mep') {
          next();
        } else {
          return res.status(401).send('Access denied. You have not the necessary authorization.');
        }
      } catch (ex) {
        // invalid token
        res.status(401).send('Invalid token.');
      }
    } else {
      return res.status(401).send('Access denied. No token provided.');
    }
  },
  
  checkAuthAdmin: async (req, res, next) => {
    // get the token from the header if present
    const token = req.headers['x-access-token'] || req.headers.authorization;
    // if no token found, return response (without going to the next middelware)
    if (token) {
      try {
        // if can verify the token, set req.user and pass to next middleware
        req.user = jwt.verify(token, process.env.JWT_PRIVATE);
        const user = await User.findById(req.user._id);
        // verify if it's an admin
        if (user.type === 'admin') {
          next();
        } else {
          return res.status(401).send('Access denied. You have not the necessary authorization.');
        }
      } catch (ex) {
        // invalid token
        res.status(401).send('Invalid token.');
      }
    } else {
      return res.status(401).send('Access denied. No token provided.');
    }
  },
};
