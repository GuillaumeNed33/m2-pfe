const bcrypt = require('bcryptjs');
const csv = require('csv-parser');
const fs = require('fs');
const User = require('../models/user.model');
const Course = require('../models/course.model');
const FormController = require('../controllers/form.controller');

exports.login = (req, res, next) => {
  User.findOne({ login: req.body.username }).then(async (user) => {
    if (user !== null) {
      const checkPassword = await bcrypt.compare(req.body.password, user.password);
      if (checkPassword) {
        const token = user.generateAuthToken();
        res.header('Authorization', token).send({
          user: {
            _id: user._id,
            type: user.type,
            first_name: user.first_name,
            last_name: user.last_name,
          },
          token,
        });
      } else {
        res.status(401).send('Invalid credentials');
      }
    } else {
      res.status(401).send('Invalid credentials');
    }
  }).catch(next);
};

exports.isLoggedIn = (req, res, next) => {
  if (typeof req.body.roles !== 'undefined') {
    User.findById(req.user._id).then((user) => {
      if (user !== null && req.body.roles.includes(user.type)) {
        res.send('You\'re logged in with right role');
      } else {
        res.status(400).send('Wrong role');
      }
    }).catch(next);
  } else {
    res.send('You\'re logged in');
  }
};

exports.updateUser = (req, res, next) => {
  User.findById(req.user._id)
    .then((authUser) => {
      User.findById(req.params.id)
        .then(async (user) => {
          if (authUser.type === 'admin' || user.courses.some((c) => authUser.courses.includes(c))) {
            if ((user.type === 'student' && (req.body.type === 'mep' || req.body.type === 'admin'))
              || ((user.type === 'admin' || user.type === 'mep') && req.body.type === 'student')) {
              res.status(401).send("Bad body parameters type for the user corresponding to id, can't change admin or mep in student or student in admin or mep");
            } else {
              const updatedUser = await User.findOneAndUpdate({ _id: user._id }, { ...req.body }, { new: true });
              res.send(updatedUser);
            }
          } else {
            res.status(401).send("Can't update user of a course you are not in.");
          }
        }).catch(next);
    }).catch(next);
};

exports.getAllUsers = (request, response, next) => {
  User.find({}).then((users) => {
    User.findById(request.user._id)
      .then((authUser) => {
        if (authUser.type === 'admin') {
          response.status(200).send(users);
        } else {
          const cpUsers = [...users];
          const filteredUsers = cpUsers.filter((user) => user.courses.some((c) => authUser.courses.includes(c)));
          response.status(200).send(filteredUsers);
        }
      }).catch(next);
  }).catch(next);
};

exports.getUsersByStudyYear = (request, response, next) => {
  User.findById(request.user._id)
    .then((authUser) => {
      User.find({ study_year: request.params.year })
        .then((users) => {
          if (authUser.type === 'admin') {
            response.status(200).send(users);
          } else {
            const cpUsers = [...users];
            const filteredUsers = cpUsers.filter((user) => user.courses.some((c) => authUser.courses.includes(c)));
            response.status(200).send(filteredUsers);
          }
        })
        .catch(next);
    }).catch(next);
};

exports.getUsersByType = (request, response, next) => {
  User.findById(request.user._id)
    .then((authUser) => {
      User.find({ type: request.params.type })
        .then((users) => {
          if (authUser.type === 'admin') {
            response.status(200).send(users);
          } else {
            const cpUsers = [...users];
            const filteredUsers = cpUsers.filter((user) => user.courses.some((c) => authUser.courses.includes(c)));
            response.status(200).send(filteredUsers);
          }
        }).catch(next);
    }).catch(next);
};

exports.getUsersByCourseId = (request, response, next) => {
  User.findById(request.user._id)
    .then((authUser) => {
      User.find({ courses: request.params.courseId })
        .then((users) => {
          if (authUser.type === 'admin' || authUser.courses.includes(request.params.courseId)) {
            response.status(200).send(users);
          } else {
            response.status(401).send("Can't get users from a course you are not in.");
          }
        }).catch(next);
    }).catch(next);
};

exports.getUsersByTypeAndCourseId = (request, response, next) => {
  User.findById(request.user._id)
    .then((authUser) => {
      User.find({ type: request.params.type, courses: request.params.courseId }).populate('forms_list')
        .then((users) => {
          if (authUser.type === 'admin' || authUser.courses.includes(request.params.courseId)) {
            response.status(200).send(users);
          } else {
            response.status(401).send("Can't get users from a course you are not in.");
          }
        }).catch(next);
    }).catch(next);
};

exports.createUser = (request, response, next) => {
  if (
    request.body.type === 'student'
    && request.body.courses
    && request.body.courses.length > 1
  ) {
    response.status(403).send('A student should only be part of one course');
    return;
  }
  User.findById(request.user._id)
    .then(async (authUser) => {
      request.body.password = await bcrypt.hash(request.body.password, 10);
      if (authUser.type === 'admin' || request.body.courses.some((c) => authUser.courses.includes(c))) {
        User.create(request.body)
          .then((user) => {
            response.status(201).send(user);
          })
          .catch(next);
      } else {
        response.status(401).send("Can't create user in a course you are not in.");
      }
    }).catch(next);
};

exports.getUserById = (request, response, next) => {
  User.findById(request.user._id)
    .then((authUser) => {
      User.findById(request.params.id).populate('forms_list')
        .then((user) => {
          if (authUser.type === 'admin' || user.courses.some((c) => authUser.courses.includes(c))) {
            response.status(200).send(user);
          } else {
            response.status(401).send("Can't get user from a course you are not in.");
          }
        }).catch(next);
    }).catch(next);
};

exports.deleteUserById = (request, response, next) => {
  User.findById(request.user._id)
    .then((authUser) => {
      User.findById(request.params.id)
        .then((user) => {
          if (authUser.type === 'admin' || user.courses.some((c) => authUser.courses.includes(c))) {
            if ((user.type === 'mep' || user.type === 'admin') && user.courses.length !== 0) {
              response.status(403).send(
                "The user is still part of at least one course. Please remove all user's course before deleting it.",
              );
              return;
            }
            User.findByIdAndRemove(request.params.id).then((deleted) => {
              if (user.type === 'student') {
                deleted.forms_list.forEach((form) => {
                  FormController.deleteFormByIdForUserDelete(form._id, response, next);
                });
              }
              response.status(200).send(deleted);
            });
          } else {
            response.status(401).send("Can't delete user from a course you are not in.");
          }
        }).catch(next);
    }).catch(next);
};

const parseCSVUser = (file, courses) => {
  const users = [];
  return new Promise((resolve, reject) => {
    const filecsv = fs.createReadStream(file).pipe(csv());
    filecsv.on('data', (data) => {
      const coursesId = [];
      const mappingStudyYear = {
        L1: 0,
        L2: 1,
        L3: 2,
        M1: 3,
        M2: 4,
      };

      if (!data.type || (data.type !== 'student' && data.type !== 'mep' && data.type !== 'admin')) {
        reject('User type is required.');
      }
      if (!data.first_name) {
        reject('First name is required.');
      }
      if (!data.last_name) {
        reject('Last name is required.');
      }
      if (!data.login) {
        reject('Login is required.');
      }
      if (!data.password) {
        reject('Login is required.');
      }
      if (!data.courses && data.type !== 'admin') {
        reject('Courses is required.');
      }

      if (data.type === 'student') {
        if (!data.study_year || typeof mappingStudyYear[data.study_year] === 'undefined') {
          reject('Study year is required for students.');
        }
        if (!data.student_number) {
          reject('Student number is required for students.');
        }
        data.study_year = mappingStudyYear[data.study_year];
        data.history = [1];
      }
      if (data.courses) {
        const dataCoursesToken = data.courses.split(';');
        dataCoursesToken.forEach((c) => {
          course = courses.find((elem) => elem.short_name === c);
          if(typeof course === 'undefined'){
            reject("The course " + c + " doesn't exist")
          }
          else{
            coursesId.push(course._id);
          }
        });
      }
      data.courses = coursesId;
      data.forms_list = [];
      data.criteria = [];
      data.description = null;
      users.push(data);
    });
    filecsv.on('end', () => {
      resolve(users);
    });
  });
};


// eslint-disable-next-line consistent-return
const hashPasswordFromCSV = (users) => new Promise((resolve) => {
  const promisePasswordUsers = [];
  users.forEach((user) => {
    promisePasswordUsers.push(bcrypt.hash(user.password, 10));
  });
  try {
    Promise.all(promisePasswordUsers)
      .then((usersPassword) => {
        users.forEach((user) => {
          user.password = usersPassword.shift();
        });
        resolve(users);
      });
  } catch (e) {
    console.error(e);
  }
});

exports.createMultipleUsers = async (req, res, next) => {
  try {
    const courses = await Course.find();
    const users = await parseCSVUser(req.files[0].path, courses);
    const usersPasswordHash = await hashPasswordFromCSV(users);
    fs.unlinkSync(`./${req.files[0].path}`);
    const newUsers = await User.create(usersPasswordHash);
    res.status(201).send(newUsers);
  } catch (e) {
    console.error(e);
    next();
  }
};

exports.getUserByToken = async (req, res, next) => {
  const { user } = req;
  User.findById(user._id).populate('forms_list')
    .then((user) => {
      res.status(200).send(user);
    })
    .catch(next);
};

exports.incrementStudyYear = async (req, res, next) => {
  const students = req.body.studentIds;
  User.updateMany(
    { _id: { $in: students }, study_year: { $lt: 4 } },
    { $inc: { study_year: 1 } },
  )
    .then((users) => {
      res.status(200).send(users);
    })
    .catch(next);
};

exports.deleteMultipleStudents = async (req, res, next) => {
  const students = req.body.studentIds;
  students.forEach((studentId) => {
    User.findById(req.user._id)
      .then((authUser) => {
        User.findById(studentId)
          .then((user) => {
            if (authUser.type === 'admin' || user.courses.some((c) => authUser.courses.includes(c))) {
              if ((user.type === 'mep' || user.type === 'admin') && user.courses.length !== 0) {
                res.status(403).send(
                  "The user is still part of at least one course. Please remove all user's course before deleting it.",
                );
                return;
              }
              User.findByIdAndRemove(studentId).then((deleted) => {
                if (user.type === 'student') {
                  deleted.forms_list.forEach((form) => {
                    FormController.deleteFormByIdForUserDelete(form._id, res, next);
                  });
                }
                res.status(200).send(deleted);
              });
            } else {
              res.status(401).send("Can't delete user from a course you are not in.");
            }
          }).catch(next);
      }).catch(next);
  });
};
