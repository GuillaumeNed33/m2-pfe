const Documentation = require('../models/documentation.model');
const User = require('../models/user.model');

exports.getAllDocumentations = (req, res, next) => {
  if(req.user) {
    if(req.query.withoutPopulateCourse && req.query.withoutPopulateCourse === 'true') {
      Documentation.find({}).then((docs) => {
        User.findById(req.user._id)
          .then(authUser => {
            let copyDocs = [...docs];
            if(authUser.type !== 'admin' && copyDocs.length > 0) {
              copyDocs  = copyDocs.filter(d => d.type === "global" || authUser.courses.includes(d.course._id));
            }
            res.send(copyDocs)
          }).catch(next);
      }).catch(next);
    } else {
      Documentation.find({}).populate('course').then((docs) => {
        User.findById(req.user._id)
          .then(authUser => {
            let copyDocs = [...docs];
            if(authUser.type !== 'admin' && copyDocs.length > 0) {
              copyDocs  = copyDocs.filter(d => d.type === "global" || authUser.courses.includes(d.course._id));
            }
            //sort to have global doc first
            const globalIndex = copyDocs.findIndex(d => d.type === "global");
            if(globalIndex !== -1) {
              copyDocs.unshift(copyDocs[globalIndex]);
              copyDocs.splice(globalIndex+1, 1)
            }
            res.send(copyDocs)
          }).catch(next);
      }).catch(next);
    }
  } else {
    Documentation.find({ type: 'global'})
      .then(doc => {
        res.send(doc);
      }).catch(next);
  }
};

exports.getDocumentationbyId = (req, res, next) => {
  Documentation.findById(req.params.id).then((doc) => {
    User.findById(req.user._id)
      .then(authUser => {
        if (doc.type === "global" || authUser.type === 'admin' || authUser.courses.includes(doc.course)) {
          res.send(doc);
        } else {
          res.status(401).send("Can't get a doc for a course you are not in.")
        }
      }).catch(next);
  }).catch(next);
};

exports.deleteDocumentation = (req, res, next) => {
  Documentation.findById(req.params.id).then((doc) => {
    User.findById(req.user._id)
      .then(authUser => {
        if (authUser.type === 'admin' || authUser.courses.includes(doc.course)) {
          Documentation.findByIdAndRemove({ _id: req.params.id }).then((doc) => {
            res.send(doc);
          }).catch(next);
        } else {
          res.status(401).send("Can't delete a doc for a course you are not in.")
        }
      }).catch(next);
  }).catch(next);
};

exports.updateDocumentation = (req, res, next) => {
  Documentation.findById(req.params.id).then((doc) => {
    User.findById(req.user._id)
      .then(authUser => {
        if (authUser.type === 'admin' || authUser.courses.includes(doc.course)) {
          Documentation.findByIdAndUpdate(
            { _id: req.params.id },
            { ...req.body },
            { new: true, useFindAndModify: false })
            .then((doc) => {
              res.send(doc);
            }).catch(next);
        } else {
          res.status(401).send("Can't update a doc for a course you are not in.")
        }
      }).catch(next);
  }).catch(next);
};

exports.createDocumentation = (req, res, next) => {
  User.findById(req.user._id)
    .then(authUser => {
      if (authUser.type === 'admin' || authUser.courses.includes(req.body.course)) {
        Documentation.create({ ...req.body }).then((doc) => {
          res.status(201).send(doc);
        }).catch(next);
      } else {
        res.status(401).send("Can't create a doc for a course you are not in.")
      }
    }).catch(next);
};

exports.getDocumentationsbyType = (req, res, next) => {
  Documentation.find({ type: req.params.type }).then((docs) => {
    if(req.params.type === "global") {
      res.send(docs)
    } else {
      User.findById(req.user._id)
        .then(authUser => {
          let copyDocs = [...docs];
          if(authUser.type !== 'admin') {
            copyDocs  = copyDocs.filter(d => authUser.courses.includes(d.course));
          }
          res.send(copyDocs)
        }).catch(next);
    }
  }).catch(next);
};

exports.getDocumentationbyCourse = (req, res, next) => {
  User.findById(req.user._id)
    .then(authUser => {
      if (authUser.type === 'admin' || authUser.courses.includes(req.params.cmi)) {
        Documentation.findOne({ course: req.params.cmi }).then((doc) => {
          res.send(doc);
        }).catch(next);
      } else {
        res.status(401).send("Can't get a doc for a course you are not in.");
      }
    }).catch(next);
};
