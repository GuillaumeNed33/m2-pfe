const fs = require('fs');
const Form = require('../models/form.model');
const User = require('../models/user.model');

exports.createForm = (req, res, next) => {
  User.findById(req.user._id)
    .then(authUser => {
      User.findById(req.body.student)
        .then(user => {
          if (authUser.type === 'admin' || authUser._id === user || authUser.courses.includes(user.courses[0])) {
            if(req.files){
                req.body.files_links = req.files.map((file) => ({ name: file.originalname, pathFile: file.path, uniqueName: file.uniqueName }));
            }
            Form.create({ ...req.body }).then((form) => {
              const formList = user.forms_list;
              formList.push(form._id);
              user.forms_list = formList;
              user.save();
              res.status(201).send(form);
            }).catch(next);
          } else {
            res.status(401).send("Can't create a form for a student in a course you are not in.");
          }
        })
        .catch(next);
    })
    .catch(next);
};

exports.getFormById = (req, res, next) => {
  User.findById(req.user._id)
    .then(authUser => {
      Form.findById(req.params.id)
        .populate('student')
        .then((form) => {
          if (authUser.type === 'admin' || authUser.courses.includes(form.student.courses[0])) {
            res.send(form);
          } else {
            res.status(401).send("Can't get a form of a student in a course you are not in.");
          }
        }).catch(next);
    }).catch(next)
};

exports.updateForm = (req, res, next) => {
  User.findById(req.user._id)
    .then(authUser => {
      Form.findById(req.params.id)
        .populate('student')
        .then((oldForm) => {
          if (oldForm.state === 'validated'
            && authUser.type === 'student') {
            res.status(401).send("A student can't modify a validated form");
          } else {
            if (authUser.type === 'admin' || authUser.courses.includes(oldForm.student.courses[0])) {
                if (req.files && req.files.length > 0) {
                  oldForm.files_links.forEach((file) => {
                    fs.unlinkSync(`./${file.pathFile}`);
                  });
                  req.body.files_links = req.files.map((file) => ({ name: file.originalname, pathFile: file.path, uniqueName: file.uniqueName }));
                }
              Form.findByIdAndUpdate(
                { _id: req.params.id },
                { ...req.body },
                { new: true, useFindAndModify: false })
                .then((newFom) => {
                  res.status(200).send(newFom);
                }).catch(next)
            } else {
              res.status(401).send("Can't update a form of a student in a course you are not in.");
            }
          }
        }).catch(next);
    }).catch(next);
};

exports.deleteForm = (req, res, next) => {
  User.findById(req.user._id)
    .then(authUser => {
      Form.findById(req.params.id)
        .populate('student')
        .then(form => {
          if (authUser.type === 'admin' || authUser.courses.includes(form.student.courses[0])) {
            Form.remove(form)
              .then(delForm => {
                delForm.files_links.forEach((file) => {
                  fs.unlinkSync(`.\\${file}`);
                });
                User.findById(delForm.student._id).then((user) => {
                  const formList = user.forms_list;
                  const index = formList.indexOf(req.params.id);
                  if (index > -1) {
                    formList.splice(index, 1);
                    user.forms_list = formList;
                    user.save();
                  }
                  res.status(200).send(delForm);
                }).catch(next);
              })
          } else {
            res.status(401).send("Can't delete a form of a student in a course you are not in.");
          }
        }).catch(next);
    }).catch(next);
};

exports.deleteFormByIdForUserDelete = (id, res, next) => {
  Form.findByIdAndRemove({ _id: id }).then((form) => {
    form.files_links.forEach((file) => {
      fs.unlinkSync(`.\\${file.pathFile}`);
    });
  }).catch(next);
};

exports.getFormsByCourse = (req, res, next) => {
  User.findById(req.user._id)
    .then(authUser => {
      if (authUser.type === 'admin' || authUser.courses.includes(req.params.id)) {
        User.find({ type: 'student', courses: req.params.id }).populate('forms').then((usersInCourse) => {
          const forms = [];
          usersInCourse.forEach((user) => {
            forms.concat(user.forms_list);
          });
          res.send(forms);
        }).catch(next);
      } else {
        res.status(401).send("Can't get forms of a course you are not in.");
      }
    }).catch(next);
};


exports.getFileFormById = (req, res, next) => {
  Form.findById(req.params.idForm).then((form) => {
    const file = form.files_links.find((f) => f.uniqueName === req.params.idFile);
    if (!file) {
      return res.status(404).end();
    } else {
      res.download(`./${file.pathFile}`);
    }
  }).catch(next);
};

exports.getFormsByStudent = (req, res, next) => {
  User.findById(req.user._id)
    .then(authUser => {
      User.findById(req.params.id)
        .then(student => {
          if (authUser.type === 'admin' || authUser.courses.includes(student.courses[0])) {
            Form.find({student: req.params.id}).then((forms) => {
              res.send(forms);
            }).catch(next);
          } else {
            res.status(401).send("Can't get forms of a student in a course you are not in.")
          }
        }).catch(next);
    }).catch(next);
};
