const Course = require('../models/course.model');
const User = require('../models/user.model');
const Documentation = require('../models/documentation.model');

exports.getAllCourses = (req, res, next) => {
  User.findById(req.user._id).populate('courses')
    .then(authUser => {
      if (authUser.type === 'admin') {
        Course.find({})
          .then((courses) => {
            let copyCourses = [...courses];
            if(req.query.forDocumentation && req.query.forDocumentation === 'true') {
              copyCourses.unshift({
                _id: 'global',
                short_name: 'Globale',
                long_name: 'Globale'
              });
            }
            res.send(copyCourses);
          }).catch(next);
      } else {
        res.send(authUser.courses);
      }
    }).catch(next);
};

exports.getCoursebyId = (req, res, next) => {
  Course.findById(req.params.id).then((c) => {
    User.findById(req.user._id)
      .then(authUser => {
        if (authUser.type === 'admin' || authUser.courses.includes(c._id)) {
          res.send(c);
        } else {
          res.status(401).send("Can't get a course you are not in.");
        }
      }).catch(next);
  }).catch(next);
};

exports.deleteCourse = async (req, res, next) => {
  const userwithCourses = await User.find({ courses: req.params.id });
  if (userwithCourses && userwithCourses.length > 0 && userwithCourses.some((user) => user.type === 'student')) {
    return res.status(401).send({ message: "Can't delete a cursus with enrolled students" });
  }
  Course.findByIdAndRemove({ _id: req.params.id }).then((c) => {
    Documentation.findOneAndRemove({ course: req.params.id }).then((doc) => {
      res.send(c);
    }).catch(next);
  }).catch(next);
};

exports.updateCourse = (req, res, next) => {
  Course.findByIdAndUpdate(
    { _id: req.params.id },
    { ...req.body },
    { new: true, useFindAndModify: false })
    .then((course) => {
      res.send(course);
    }).catch(next);
};

exports.createCourse = (req, res, next) => {
  Course.create({ ...req.body }).then((c) => {
    res.status(201).send(c);
  }).catch(next);
};
