const Criterion = require('../models/criterion.model');

exports.getAllCriteria = (req, res, next) => {
  Criterion.find({})
    .then((criterion) => {
      res.status(200).send(criterion);
    })
    .catch(next);
};
