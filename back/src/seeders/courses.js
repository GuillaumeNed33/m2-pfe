const Course = require('../models/course.model');

exports.seed = async () => {
  const course = {
    short_name: 'c1',
    long_name: 'courseTest',
  };
  const course2 = {
    short_name: 'c2',
    long_name: 'courseTest2',
  };
    const course3 = {
    short_name: 'c3',
    long_name: 'courseTest3',
  };
  const courses = [course, course2, course3];
  try {
    await Course.create(courses);
  } catch(err) {
    console.error(err);
  }
};

exports.reset = async () => {
  try {
    const course = await Course.findOne({ short_name: 'c1'});
    const course2 = await Course.findOne({ short_name: 'c2'});
    const course3 = await Course.findOne({ short_name: 'c3'});
    if (course && course._id) {
      await Course.findByIdAndRemove(course._id);
      await Course.findByIdAndRemove(course2._id);
      await Course.findByIdAndRemove(course3._id);
    }
  } catch(err) {
    console.error(err);
  }
};