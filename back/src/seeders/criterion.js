const Criterion = require("../models/criterion.model");

exports.seed = async () => {
  const criterion = {
    name: "critTest",
    description: "critTest",
    tag: "test"
  };
  const criterion1 = {
    name: "critTest1",
    description: "critTest1",
    tag: "test1"
  };
  const criterionWithWeeks = {
    name: "critWeek",
    description: "critTest",
    tag: "stage",
    count_total_weeks: true,
    minimum_weeks_to_validate: 5
  };
  try {
    await Criterion.create(criterion);
    await Criterion.create(criterion1);
    await Criterion.create(criterionWithWeeks);
  } catch (err) {
    console.error(err);
  }
};

exports.reset = async () => {
  try {
    Criterion.remove({}, function(err) {
      console.log("collection Removed");
    });
  } catch (err) {
    console.error(err);
  }
};
