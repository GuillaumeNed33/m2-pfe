const User = require('../models/user.model');
const Form = require('../models/form.model');
const Criterion = require('../models/criterion.model');

createForm = (student, crits) => {
  return new Promise(async (resolve) => {
    let form = {
      student: student._id,
      state: 'toValidate',
      criteria: [crits[0]._id],
      submit_date: Date.now(),
      description: 'formTest'
    };
    try {
      const createdForm = await Form.create(form);
      student.forms_list.push(createdForm._id);
      await student.save();
      resolve();
    } catch(err) {
      console.error(err);
    }
  });
}

exports.seed = async () => {
  try {
    const students = await User.find({ type: 'student'});
    const crits = await Criterion.find({});
  
    const promises = [];
    students.forEach(async student => {
      promises.push(createForm(student, crits));
    });
    await Promise.all(promises);
  } catch(err) {
    console.error(err);
  }
};