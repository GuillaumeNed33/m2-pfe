const mongoose = require('mongoose');
const dotenv = require('dotenv');
const userSeed = require('./users');
const courseSeed = require('./courses');
const criterionSeed = require('./criterion');
const formSeed = require('./forms');

seederMain = async () => {
  // get env variables from .env file
  switch (process.env.NODE_ENV) {
    case 'production':
      dotenv.config({ path: './.env.prod' });
      break;
    case 'testing':
      dotenv.config({ path: './.env.test' });
      break;
    case 'docker':
      dotenv.config({ path: './.env.docker' });
      break;
    case 'docker_test':
      dotenv.config({ path: './.env.docker.test' });
      break;
    default: //dev mode
      dotenv.config({ path: './.env' });
      break;
  }

  // connect to mongodb
  mongoose.connect(process.env.DB_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
    }
  );

  await courseSeed.seed();
  await userSeed.seed();
  await criterionSeed.seed();
  await formSeed.seed();
  
  mongoose.connection.close();
}

seederMain();