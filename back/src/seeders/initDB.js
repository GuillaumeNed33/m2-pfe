const mongoose = require('mongoose');
const dotenv = require('dotenv');
const bcrypt = require("bcryptjs");
const User = require("../models/user.model");
const Criterion = require("../models/criterion.model");

initDB = async () => {
  // get env variables from .env file
  switch (process.env.NODE_ENV) {
    case 'production':
      dotenv.config({ path: './.env.prod' });
      break;
    case 'testing':
      dotenv.config({ path: './.env.test' });
      break;
    case 'docker':
      dotenv.config({ path: './.env.docker' });
      break;
    case 'docker_test':
      dotenv.config({ path: './.env.docker.test' });
      break;
    default: //dev mode
      dotenv.config({ path: './.env' });
      break;
  }

  // connect to mongodb
  mongoose.connect(process.env.DB_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
    }
  );

  const cryptedAdminPass = await bcrypt.hash("admin", 10);
  const admin = {
    type: 'admin',
    first_name: 'admin',
    last_name: 'admin',
    courses: [],
    forms_list: [],
    criteria: [],
    login: "admin",
    password: cryptedAdminPass
  };

  const stage = {
    name: "Stage",
    description: "",
    tag: "stage",
    count_total_weeks: true,
    minimum_weeks_to_validate: 14
  };
  const mobilite = {
    name: "Mobilité",
    description: "",
    tag: "mobilite",
    count_total_weeks: true
  };
  const pix = {
    name: "PIX",
    description: "",
    tag: "pix",
    count_total_weeks: false
  };
  const anglais = {
    name: "Anglais",
    description: "",
    tag: "anglais",
    count_total_weeks: false
  };

  try {
    await User.create(admin);
    await Criterion.create(stage);
    await Criterion.create(mobilite);
    await Criterion.create(pix);
    await Criterion.create(anglais);
  } catch (err) {
    console.error(err);
  }

  mongoose.connection.close();
}

initDB();