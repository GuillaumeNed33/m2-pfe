const bcrypt = require("bcryptjs");
const User = require("../models/user.model");
const Course = require("../models/course.model");
const Forms = require("../models/form.model");

exports.seed = async () => {
  const password = "admintest";
  const cryptedPass = await bcrypt.hash(password, 10);
  const courseC1 = await Course.findOne({ short_name: 'c1' });
  const courseArray1 = [courseC1._id];
  const courseC2 = await Course.findOne({ short_name: 'c2' });
  const courseArray2 = [courseC2._id];
  const admin = {
    type: 'admin',
    first_name: 'admin',
    last_name: 'admin',
    courses: [],
    forms_list: [],
    criteria: [],
    login: "admintest",
    password: cryptedPass
  };
  const studentPass = "studenttest";
  const studentCrypted = await bcrypt.hash(studentPass, 10);
  const student = {
    type: 'student',
    first_name: 'student',
    last_name: 'student',
    courses: courseArray1,
    student_number: '1111',
    forms_list: [],
    criteria: [],
    login: "studenttest",
    password: studentCrypted,
    study_year: 0,
    history: [1]
  };

  const mepPass = "meptest";
  const mepCrypted = await bcrypt.hash(mepPass, 10);
  const mep = {
    type: "mep",
    first_name: "mep",
    last_name: "mep",
    courses: courseArray1,
    forms_list: [],
    criteria: [],
    login: "meptest",
    password: mepCrypted
  };
 
  const stud2 = {
    type: 'student',
    first_name: '2ndStud',
    last_name: '2ndStud',
    courses: courseArray2,
    student_number: '2222',
    forms_list: [],
    criteria: [],
    login: '2ndStud',
    password: studentCrypted,
    study_year: 0,
    history: [1]
  };

  const users = [admin, student, stud2, mep];
  try {
    await User.create(users);
  } catch (err) {
    console.error(err);
  }
};

exports.reset = async () => {
  try {
    User.remove({}, function(err) {
      console.log("collection Removed");
    });
  } catch (err) {
    console.error(err);
  }
};
