const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');
const dotenv = require('dotenv');
const cors = require('cors');

// get env variables from .env file
switch (process.env.NODE_ENV) {
  case 'production':
    dotenv.config({ path: './.env.prod' });
    break;
  case 'testing':
    dotenv.config({ path: './.env.test' });
    break;
  case 'docker':
    dotenv.config({ path: './.env.docker' });
    break;
  case 'docker_test':
    dotenv.config({ path: './.env.docker.test' });
    break;
  default: //dev mode
    dotenv.config({ path: './.env' });
    break;
}

// set up express app
const app = express();

app.disable('x-powered-by');
// connect to mongodb
mongoose.connect(process.env.DB_URL, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
});

// use body-parser middleware
app.use(bodyParser.json());

// log http request
app.use(morgan('dev'));

// enable cors
app.use(cors());

// initialize routes
app.use('/', require('./src/routes/routes'));
app.use('/user', require('./src/routes/user.routes'));
app.use('/course', require('./src/routes/course.route'));
app.use('/criterion', require('./src/routes/criterion.route'));
app.use('/documentation', require('./src/routes/documentation.route'))
app.use('/form', require('./src/routes/form.routes'));

// error handling middleware
app.use(async (err, req, res, next) => {
  res.status(422).send(err.message);
});

// start server
const PORT = process.env.PORT || 4000
app.listen(PORT, () => {
  console.log(`now listening for requests on  http://127.0.0.1:${PORT}`);
});
