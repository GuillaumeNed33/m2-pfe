# PFE - M2 Informatique
[![pipeline status](https://gitub.u-bordeaux.fr/daauber/cmitacker/badges/master/pipeline.svg)](https://gitub.u-bordeaux.fr/daauber/cmitacker/commits/master)
[![coverage report](https://gitub.u-bordeaux.fr/daauber/cmitacker/badges/master/coverage.svg)](https://gitub.u-bordeaux.fr/daauber/cmitacker/commits/master)

## Description

Projet de fin d'étude du Master 2 Informatique. Le sujet est la création d'une application web permettant la gestion des Cursus Master Ingenierie. C'est-à-dire une plateforme permettant aux étudiants de soumettre des fiches pour valider les critères des CMI et aux membres des équipes pédagogiques de gérer les étudiants, des gérer leurs demandes de validations de fiches et de critères.

## Technologies
**Front :** [React][2] / [Next.js][3]  
**Backend :** [NodeJS][4] ([ExpressJS][5]) avec ORM [Mongoose][1]  
**Base de données :** [MongoDB][6]  

## Manuel d'installation

### Avec Docker

Executez la commande : 
    
    $ docker-compose up -d
    
Les ports du front, du back et de la base de données sont modifiables dans le fichier `.env`.  
Par défaut : 
* Le front est disponible à l'adresse [localhost:3000](localhost:3000)
* Le back-end est lancée sur [localhost:4000](localhost:4000) !
* La base de donnée est lancée sur [mongodb://localhost:27017](mongodb://localhost:27017) !
    
### Sans Docker
Lancez le back end à l'adresse [localhost:4000](localhost:4000) (par défaut) :

    $ cd back
    $ npm install
    $ npm start 
    
Lancez le front end à l'adresse [localhost:3000](localhost:3000) (par défaut) :

    $ cd front
    $ npm install
    $ npm run build 
    $ npm start 
    
### Configuration
**Back-end**  
Les fichiers de configuration sont les fichiers `.env.*` situés dans `./back/`.
* `.env` : Fichier utilisé au lancement en mode dev (`npm run dev`), **SANS** docker
* `.env.prod` : Fichier utilisé au lancement en mode prod (`npm start`), **SANS** docker
* `.env.test` : Fichier utilisé au lancement des tests (`npm run test`), **SANS** docker
* `.env.docker` : Fichier utilisé au lancement **AVEC** docker
* `.env.docker.test` : Fichier utilisé au lancement des tests **AVEC** docker

**Front-end**  
Les fichiers de configuration sont les fichiers `.env.*` situés dans `./front`.

* `.env` : Fichier utilisé au lancement **SANS** docker
* `.env.docker` : Fichier utilisé au lancement **AVEC** docker

**IMPORTANT : SI VOUS MODIFIEZ UN FICHIER DE CONFIGURATION, 
VERIFIEZ QUE LES CONFIGURATION DU BACK ET DU FRONT SONT COMPATIBLE!**  
*Exemple : Le changement de port du backend doit entrainer une modification de l'URL côté front*

## Première utilisation de l'application

Afin que le site soit fonctionnel dès sa première utilisation, il faut insérer 
dans la base de données, un premier utilisateur (supprimable par la suite).
De plus l'application ne fournit pas d'interface pour créer ou modifier les critères de validation des CMI.

Pour remédier à cela, vous pouvez utiliser la commande suivante (A N'UTILISER QU'UNE FOIS, AU PREMIER LANCEMENT DE L'APPLICATION) : 

    $ cd back
    $ npm run initDB
    
Cette commande permet de créer un administrateur : (login: admin / mot de passe : admin), 
ainsi que les 4 critères suivants : Stage, Mobilité, Anglais et PIX.

Vous pouvez modifier ces informations ou les détails de celles-ci via des outils de gestion de base de données MongoDB 
comme [MongoDB Compass][7] ou [Robo3T][8].

Vous pouvez ensuite vous connecter avec l'admin nouvellement créé afin de créer votre propre compte dans l'onglet *Gestion Utilisateur*. 
Dans la boite de dialogue *Ajouter un utilisateur*, ajoutez un nouvel Administrateur avec les informations voulues. Une fois cela fait, supprimez le compte admin depuis le Tableau des utilisateurs. 

Vous pouvez maintenant commencer à utiliser l'application.

## L'Équipe 

* Cyril CAULONQUE
* Nicolas DUROC
* Rayan Ryad KHELOUFI
* Florian MOREL
* Guillaume NEDELEC
* Florian ROULET
* Alfred Aboubacar SYLLA

[1]: https://mongoosejs.com/
[2]: https://fr.reactjs.org/
[3]: https://nextjs.org/
[4]: https://nodejs.org/en/
[5]: https://expressjs.com/fr/
[6]: https://www.mongodb.com/fr
[7]: https://www.mongodb.com/products/compass
[8]: https://robomongo.org/
